# Copyright (C) 2003 Arnaud DESMONS (adesmons@mandrakesoft.com),
#                    Damien KROTKINE (damien@tuxfamily.org),
#                    Charles LONGEAU (chl@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# Libconf Template for pf.conf, OpenBSD Packet Filter configuration file.
# $Author: logarno $
# $Id: pf_template.pm,v 1.1.1.1 2004/02/04 12:11:59 logarno Exp $

package Libconf;

$templates{pf} = {
		  rules => [q(foreach my $regex ('pass|block|antispoof|scrub', 'in|out', 'log', 'quick', 'keep\s+state',
						 'all') {
			   if ($in =~ s/(?<!\S)($regex)(?!\S)//) {
			       $atom->{type} = 'VALUES_LIST';
			       $matched = 1;
	                       push @{$atom->{list}}, $1;
			   }
		       }
		       foreach my $regex ('proto', 'on', 'flags') {
			   if ($in =~ s/(?<!\S)$regex\s+(\S*)//) {
			       push @{$atom->{list}}, {$regex => $1};
			   }
		       }
		       foreach my $regex ('from', 'to') {
			   my $peer;
                         if ($in =~ /(?<!\S)$regex(?!\S)/) {
                           if ($in =~ /$regex\s+{([^{}]*)}/) {
 			     $peer->{host} = [split(/\s*,\s*/, $1)];
                           }
	                   elsif ($in =~ /$regex\s+(\S+)/) {
			     $peer->{host} = $1;
                           }
			   if ($in =~ s/$regex\s+(\S*|{.*})\s+port\s+{(.*)}//) {
			      $peer->{port}{value} = [split(/\s*,\s*/, $2)];
			      $peer->{port}{type} = "LIST";
		           }
		           elsif ($in =~ s/$&\s+port\s+(\S+)\s+([<>]+)\s+(\S+)//) {
			      $peer->{port}{value} = [$1, $2, $3];
		           }
			   $in =~ s/$&//;
                           push @{$atom->{list}}, { $regex => $peer};
                         }
                        }
		       ),
		     q( if ($in =~ s/^\s*(\w+)\s*=\s*("([^"]*)"|'([^']*)'|[^'"\s]+)\s*$//) {
                           $atom->{type} = 'KEY_VALUE';
			   $atom->{type2} = 'PF_EQ';
                           $atom->{key} = $1;
                           $atom->{value} = defined $3 ? $3 : $2;
                           $matched = 1;
                           }
                       ),
		     q( if ($in =~ s/^\s*set\s+(\S+)\s+//) {
                          $atom->{type} = 'KEY_VALUE';
                          $atom->{type2} = 'PF_SET';
                          $atom->{key} = $1;
                          if ($in =~ s/^(\S+)\s*$//) {
                            $atom->{value} = $1;
                          }
                          elsif ($in =~ s/{?([^{}]+)}?//) {
			    foreach (split(/\s*,\s*/, $1)) {
                               my $h;
                               ($h->{key}, $h->{value}) = split;
                               push @{$atom->{value}}, $h;
                            }
                           }
                         }
                        )
		     ],
	   comments => [ ['#'] ],
	   comment_output => q(/^(\s*)$/ ? "$_\n" : "#$_\n"),
	   output => {
                      KEY_VALUE => q(
				     if ($atom->{type2} eq 'PF_SET') {
				       if (!ref $atom->{value}) {
					 "set ". $atom->{key} ." ". $atom->{value} . "\n"
				       }
				       elsif (ref $atom->{value} eq "ARRAY") {
					 my $out;
					 foreach (@{$atom->{value}}) {
					   $out.= "$_->{key} $_->{value},";
					 }
					 @{$atom->{value}} and chop $out;
					 if (@{$atom->{value}} > 1) {
					   return "set $atom->{key} {$out}\n";
					 }
					 "set $atom->{key} $out\n";
				       }
				     }
				     elsif ($atom->{type2} eq 'PF_EQ') {
				       $atom->{key} ."=". $atom->{value} ."\n";
				     }
				    ),
		      VALUES_LIST => q(
					 my $out;
					 foreach (@{$atom->{list}}) {
					   if (!ref) {
					     $out .= "$_ ";
					   }
					   elsif (ref eq "HASH") {
					     my $h = $_;
					     foreach my $keys (keys %{$_}){
					       $out .= $keys . " ";
					       if (!ref $h->{$keys}) {
						 $out .= $h->{$keys} ." ";
					       }
					       elsif (ref $h->{$keys} eq "HASH") {
						 if (!ref $h->{$keys}{host}) {
						   $out .= $h->{$keys}{host} . " ";
						 }
						 elsif (ref $h->{$keys}{host} eq "ARRAY") {
						   $out .= "{" . join(", ", @{$h->{$keys}{host}}) . "} ";
						 }
						 if (defined $h->{$keys}{port}) {
						   $out .= "port ";
						   if ($h->{$keys}{port}{type} eq "LIST") {
						     $out .= "{" . join(", ", @{$h->{$keys}{port}{value}}) . "} ";
						   }
						   else {
						     $out .= join(" ", @{$h->{$keys}{port}{value}}) . " ";
						   }
						 }
					       }
					     }
					   }
					 }
				       chop $out;
				       "$out\n";;
				      )
		     },
	   edit_atom => q(
	                   if ($index == -1) {
	                       my $i = 0;
	                       foreach (@{$out->{atoms}}) {
	                   	   $_->{key} eq $args{key} and $index = $i;
	                   	   $i++;
	                       }
	                       $index == -1 and return -2;
	                   }
	                   @{@{$out->{atoms}}[$index]}{keys(%args)} = values(%args);
	                   return $index;
	                 ),
	   find_atom_pos => q(
	                     my $i = 0;
	                     my @res;
	                     foreach my $atom (@{$out->{atoms}}) {
	                         my $flag = 1;
	                         foreach my $key (keys(%args)) {
	                             if ($key eq 'values') {
	                                 foreach (keys(%{$args{values}})) {
	                                     $atom->{values}{$_} eq $args{values}->{$_} or $flag = 0, last;
	                                 }
	                             } else {
	                                 $atom->{$key} eq $args{$key} or $flag = 0, last;
	                             }
	                         }
	                         $flag and push(@res, $i);
	                         $i++;
	                     }
	                     wantarray ? @res : $res[0];
	                   ),
	  };
1
