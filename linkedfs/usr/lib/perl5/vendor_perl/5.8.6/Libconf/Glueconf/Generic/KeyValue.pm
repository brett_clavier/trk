#!/usr/bin/perl

# Author : Brian J. Murrell (brian@interlinx.bc.ca)
# Based heavily on Shell.pm (in fact I ripped it off completely!)
#
# Contributors : 
#
# Copyright (C) 2003 Brian J. Murrell (brian@interlinx.bc.ca)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Generic::KeyValue;
use strict;
use vars qw(@ISA);
use Libconf;
use Libconf::Glueconf;
use Libconf::Templates::Generic::KeyValue;
@ISA = qw(Libconf::Glueconf);

sub new {
    my ($class, $args, $data_description, $data_mapping) = @_;
    my $libconf = new Libconf::Templates::Generic::KeyValue($args);
    $libconf->set_uniq(qw(key));
    $libconf->read_conf();
    tie my %wrapper, 'Libconf::Glueconf::Generic::KeyValue::Wrapper', $libconf, $data_description, $data_mapping;
    bless \%wrapper, $class;
}

sub freeze {
    my ($self) = @_;
    tied(%$self)->{_frozen} = 1;
}

sub thaw {
    my ($self) = @_;
    my $tied = tied(%$self);
    # we delete from end to start to respect indexes.
    $tied->{libconf}->delete_atom($_) foreach (sort {$b <=> $a} @{$tied->{_to_delete}});
    $tied->synchronize();
    $tied->{_frozen} = 0;
}

package Libconf::Glueconf::Generic::KeyValue::Wrapper;

sub debug { Libconf::debug(@_) }

sub synchronize {
    my $self = shift;
    $self->{hash} = { map { $self->{libconf}->get_atom($_)->{key}, $_ } (0..$self->{libconf}->get_size()-1) };
}

sub TIEHASH {
    my ($pkg, $libconf, $data_description, $data_mapping) = @_;
    debug();
    my $self = bless {
                      libconf => $libconf,
                      _data_description => $data_description,
                      _data_mapping => $data_mapping,
                      _frozen => 0,
                      _to_delete => [],
                      hash => {},
                     }, $pkg;
    $self->synchronize();
    $self
}

sub CLEAR { my ($obj) = @_; debug(); $obj->{hash} = {}; $obj->{libconf}->clear }

sub DELETE {
    my ($obj, $key) = @_; debug("key: $key");
    my $ret;
    defined $obj->{hash}{$_} or return undef;
    if ($obj->{_frozen}) {
        push @{$obj->{_to_delete}}, $obj->{hash}{$_};
    } else {
#        foreach ($obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE', key => $key })) {
            $ret = $obj->{libconf}->delete_atom($obj->{hash}{$key});#$_);
#        }
        $obj->synchronize();
    }
    $ret;
}

sub FIRSTKEY { my ($obj) = @_; debug(); keys %{$obj->{hash}}; scalar(each %{$obj->{hash}}) }

sub EXISTS { my ($obj, $key) = @_; debug("key : $key"); exists $obj->{hash}{$key} }

sub NEXTKEY { my ($obj, $lastkey) = @_; debug("lastkey : $lastkey"); scalar(each %{$obj->{hash}}) }

sub STORE {
    my ($obj, $key, $value) = @_; debug("key : $key - value : $value");
    $obj->{_frozen} and warn("I can't store a value in the hash while it is frozen (use thaw to unfreeze)"), return undef;
    ref $value eq '' or die 'trying to store anything else than a value';
    my $index;
    if (exists $obj->{hash}{$key}) {
        $index = $obj->{hash}{$key};
        $obj->{libconf}->edit_atom($index, { type => 'KEY_VALUE', key => $key, value => $value });
    } else {
        $index = $obj->{libconf}->append_atom({ type => 'KEY_VALUE', key => $key, value => $value });
        $obj->{hash}{$key} = $index;
    }
    $index;
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
#    substr($key, 0, 1) eq '_' and return $obj->{$key};
    $obj->{libconf}->get_atom($obj->{hash}{$key})->{value};
}

1;

