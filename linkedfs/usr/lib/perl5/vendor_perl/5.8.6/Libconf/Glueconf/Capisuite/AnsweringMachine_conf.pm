#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors :
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::Capisuite::AnsweringMachine_conf;
use strict;
use Libconf;

our @ISA = qw(Libconf);
our $data_synopsis;

# $data_synopsis_version is optionnal
sub new {
    my ($class, $filename, $data_synopsis_version, $data_description, $data_mapping) = @_;
    if (defined $data_synopsis) {
        $data_synopsis_version ||= 'default_version';
        $data_description = $data_synopsis->{$data_synopsis_version}{description};
        $data_mapping = $data_synopsis->{$data_synopsis_version}{mapping};
    }
    my $libconf = Libconf->new($filename, 'capi_answering', '');
    $libconf->setUniq();
    tie my %wrapper, 'Libconf::Glueconf::Capisuite::AnsweringMachine_conf::Wrapper', $libconf, $data_description, $data_mapping ;
    bless \%wrapper, $class;
}

sub readConf {
    my ($obj) = @_;
    $obj->{libconf}->readConf;
}

sub writeConf {
    my ($obj) = @_;
    $obj->{libconf}->writeConf;
}


package Libconf::Glueconf::Capisuite::AnsweringMachine_conf::Wrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $data_description, $data_mapping) = @_;
    debug();
    bless {
           libconf => $libconf,
           _data_description => $data_description,
           _data_mapping => $data_mapping,
          }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{libconf}->clear;
}

#sub DESTROY {
#}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key: $key");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'SECTION', sections => [], section_name => $key });
    foreach (@pos) {
        $obj->{libconf}->deleteSection($_, 1);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    my $atom = $obj->{libconf}->getAtom(0);
    if (defined $atom){
        $atom->{type} eq 'SECTION' or die "atom n� 0 is not a section";
    }
    $atom->{section_name};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->findAtomPos( { type => 'SECTION', sections => [], section_name => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("lastkey : $lastkey");
    my @pos = $obj->{libconf}->findAtomPos( { type => 'SECTION', sections => [] });
    my $i = 0;
    my $index;
    foreach (@pos) {
        $obj->{libconf}->getAtom($_)->{section_name} eq $lastkey and $index = $i;
        $i++;
    }
    defined $index or return undef;
    $index == $#pos and return undef;
    print " $lastkey NXTK : " . $obj->{libconf}->getAtom($pos[$index+1])->{section_name} . "\n";
    $obj->{libconf}->getAtom($pos[$index+1])->{section_name};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    ref $value eq 'HASH' or die 'trying to store something else than hash ref';
    my $index;
    my $pos = $obj->{libconf}->findAtomPos({ type => 'SECTION', section_name => $key, sections => [] });
    defined $pos or $pos = $obj->{libconf}->appendAtom({ section_name => $key, type => 'SECTION' });
    my %hash;
    tie %hash, 'Libconf::Glueconf::Capisuite::AnsweringMachine_conf::SectionWrapper', $obj->{libconf}, $pos;
    %hash = %$value;
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    substr($key, 0, 1) eq '_' and return $obj->{$key};
    my $section_position = $obj->{libconf}->findAtomPos( { type => 'SECTION', section_name => $key, sections => [ ] });
    defined $section_position or $section_position = $obj->{libconf}->appendAtom({ section_name => $key, type => 'SECTION', sections => [ ] });
    my %ret;
    tie %ret, 'Libconf::Glueconf::Capisuite::AnsweringMachine_conf::SectionWrapper', $obj->{libconf}, $section_position;
    \%ret;
}

package Libconf::Glueconf::Capisuite::AnsweringMachine_conf::SectionWrapper;

sub debug { Libconf::debug(@_) }

sub TIEHASH {
    my ($pkg, $libconf, $beginning) = @_;
    debug("beginning : $beginning");
    my $atom = $libconf->getAtom($beginning);
    my @depth = @{$atom->{sections}};
    scalar(keys %{$depth[0]}) == 0 and shift @depth;
    push @depth, { name => $atom->{section_name} };

    my $key = $beginning+1;
    foreach ($beginning+1..$libconf->size-1) {
        my $atom = $libconf->getAtom($_);
        @{$atom->{sections}} > @depth and next;
        Libconf::compare_depth($atom->{sections}, \@depth) or last;
    } continue {
        $key++;
    }
    my %hash = (
                libconf => $libconf,
                firstatom => $beginning,
                sections => \@depth,
                lastatom => $key,
               );
    debug("firstatom : $hash{firstatom} - lastatom : $hash{lastatom}");
    return bless \%hash, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug();
    $obj->{lastatom} -= $obj->{libconf}->clearSection($obj->{firstatom}, 1);
    $obj->{lastatom} == $obj->{firstatom} or die "removed atoms number is wrong";
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("key : $key");
    my @pos = $obj->{libconf}->findAtomPos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    foreach (@pos) {
        $obj->{libconf}->deleteAtom($_);
        $obj->{lastatom}--;
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug();
    $obj->{firstatom} + 1 == $obj->{lastatom} and return undef;
    my $atom = $obj->{libconf}->getAtom($obj->{firstatom}+1);
    $atom->{type} eq 'KEY_VALUE' and return $atom->{key};
    die "houston, we have a problem";
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("key : $key");
    my $pos = $obj->{libconf}->findAtomPos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    defined $pos ? 1 : 0;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("SectionWrapper - NEXTKEY - lastkey : $lastkey");
    debug "coin : " . ($obj->{firstatom}+1) . " --- " . ($obj->{lastatom}-1);
    my $pos = $obj->{libconf}->findAtomPos( {type => 'KEY_VALUE', key => $lastkey },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    !defined $pos || $pos+1 == $obj->{lastatom} and return undef;
    return $obj->{libconf}->getAtom($pos+1)->{key};
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("key : $key - value : $value");
    #ref $value eq 'HASH' or die 'trying to store anything else than hash ref';
    ref $value eq '' or die "try to store a hash in a kery_value atom";
    my $pos = $obj->{libconf}->findAtomPos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    if (!defined $pos) {
        $pos = $obj->{libconf}->insertAtom($obj->{lastatom}, { type => 'KEY_VALUE', key => $key,
                                                               sections => [ @{$obj->{sections}} ],
                                                             }
                                          );
        $obj->{lastatom}++;
    }
    $obj->{libconf}->editAtom($pos, { value => $value });
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("key : $key\n");
    my $pos = $obj->{libconf}->findAtomPos( {type => 'KEY_VALUE', key => $key },
                                            $obj->{firstatom}+1, $obj->{lastatom}-1
                                          );
    defined $pos or return undef;
    return $obj->{libconf}->getAtom($pos)->{value};
}

$data_synopsis ||= {};
$data_synopsis->{default_version} =
  {
   description => {

                   #GLOBAL section
                   GLOBAL => {
                              audio_dir => { type => 'FILENAME', default => '/usr/share/capisuite/', mandatory => 1, description =>
                                             qq(audio_dir (MANDATORY)\n).
                                             qq(\n).
                                             qq(Directory where audio snippets used in the answering machine script are\n).
                                             qq(located. If user_audio_files is enabled (see below), each user can also\n).
                                             qq(provide his own audio snippets in his user_dir (see below).)
                                           },

                              voice_user_dir => { type => 'DIRECTORY', default => '/var/spool/capisuite/users/', mandatory => 1, description =>
                                                  qq(voice_user_dir (MANDATORY)\n).
                                                  qq(\n).
                                                  qq(Directory for all user-specific data. Contains one subdirectory\n).
                                                  qq(for each user (named like his userid). The following directory tree is used:\n).
                                                  qq(\n).
                                                  qq(user_dir/username/          -  here the user may provide his own audio_files\n).
                                                  qq(                               (see user_audio_files option). The user defined announcement\n).
                                                  qq(                               is also saved here.\n).
                                                  qq(user_dir/username/received  -  all received calls (voice and fax) will be saved here\n)
                                                },

                              user_audio_files => { type => 'BOOLEAN', default => 0, description =>
                                                    qq(user_audio_files (optional, defaults to 0)\n).
                                                    qq(\n).
                                                    qq(Controls wether the user_dir (see below) will also be searched for audio\n).
                                                    qq(files. If set to "1", the script will look in the user_dir and then in\n).
                                                    qq(audio_dir for a needed audio file. If "0", only audio_dir is used.\n).
                                                    qq(This doesn't affect the announcement, which can and should be different\n).
                                                    qq(for each user in any case.\n)
                                                  },

                              voice_delay => { type => 'INTEGER', default => 15, mandatory => 1, description =>
                                               qq(voice_delay (MANDATORY)\n).
                                               qq(\n).
                                               qq(Global setting for the time in seconds before incoming voice calls are\n).
                                               qq(accepted.\n)
                                             },

                              announcement => { type => 'FILENAME', default => 'announcement.la', description =>
                                                qq(announcement (optional, defaults to "announcement.la")\n).
                                                qq(\n).
                                                qq(This value gives the default name of the announcement file which is searched\n).
                                                qq(in the user_dir. Each user should provide one in his/her own dir.\n)
                                              },

                              record_length => { type => 'INTEGER', default => 60, description =>
                                                 qq(record_length (optional, defaults to 60)\n).
                                                 qq(\n).
                                                 qq(Global setting for the maximal record length of the answering machine\n).
                                                 qq(in seconds\n)
                                               },

                              record_silence_timeout => { type => 'INTEGER', default => 5, description =>
                                                          qq(record_silence_timeout (optional, defaults to 5)\n).
                                                          qq(\n).
                                                          qq(Global setting for the length of silence after which recording is\n).
                                                          qq(finished by the answering machine\n)
                                                        },
                             },

                   #users section (it's anything else GLOBAL)
                   '^(?!GLOBAL).*' => {

                           # The following sections start with the name of the users which want to use
                           # CapiSuite. The names must be exactly equal to system users.
                           #
                           # Each user section can override the following default options given above:
                           #
                           voice_delay => { type => 'INTEGER', default => 15, mandatory => 1, description =>
                                            qq(voice_delay (MANDATORY)\n).
                                            qq(\n).
                                            qq(Global setting for the time in seconds before incoming voice calls are\n).
                                            qq(accepted.\n)
                                          },

                           announcement => { type => 'FILENAME', default => 'announcement.la', description =>
                                             qq(announcement (optional, defaults to "announcement.la")\n).
                                             qq(\n).
                                             qq(This value gives the default name of the announcement file which is searched\n).
                                             qq(in the user_dir. Each user should provide one in his/her own dir.\n)
                                           },

                           record_length => { type => 'INTEGER', default => 60, description =>
                                              qq(record_length (optional, defaults to 60)\n).
                                              qq(\n).
                                              qq(Global setting for the maximal record length of the answering machine\n).
                                              qq(in seconds\n)
                                            },

                           record_silence_timeout => { type => 'INTEGER', default => 5, description =>
                                                       qq(record_silence_timeout (optional, defaults to 5)\n).
                                                       qq(\n).
                                                       qq(Global setting for the length of silence after which recording is\n).
                                                       qq(finished by the answering machine\n)
                                                     },

                           #
                           # Additionally, the following options are possible:
                           #
                           voice_numbers => { type => 'LIST', type2 => 'STRING', type3 => ',', description =>
                                              qq(This list contains the numbers on which this user wants to receive incoming\n).
                                              qq(voice calls. The values are separated by commas. You can also use the special\n).
                                              qq(entry "*" which stands for accepting ALL incoming calls (use with care!).\n).
                                              qq(The special character "-" stands for "no destination number available". That\n).
                                              qq(is necessary for example for the austrian "Global Call" where no number is\n).
                                              qq(signalled when the main MSN is called (sic).\n)
                                            },

                           voice_email => { type => 'MAIL_ADDR', description =>
                                            qq(If given, this string indicates email-addresses where the received faxes\n).
                                            qq(and voice calls will be sent to. If it is empty, the recorded calls and\n).
                                            qq(faxes will be sent to the user on the current system. If you don't want to\n).
                                            qq(get emails, see the "action" option below\n)
                                          },

                           pin => { type => 'STRING', description =>
                                    qq(pin for activating the remote inquiry. Start typing when the announcement\n).
                                    qq(is played. If you don't want remote inquiry function for your answering\n).
                                    qq(machine for security or other reasons, just set this to an empty string.\n).
                                    qq(You can use as many digits as you want. The script will wait 2 seconds after\n).
                                    qq(each typed digit for the next one.\n)
                                  },

                           voice_action => { type => 'STRING', values => [ qw(MailAndSave SaveOnly None) ], mandatory => 1, description =>
                                             qq(Here you can define what action will be taken when a call is received.\n).
                                             qq(Currently, three possible actions are supported:\n).
                                             qq(\n).
                                             qq(MailAndSave - the received call will be mailed to the given address \(see\n).
                                             qq( 	  "email" above\) and saved to the user_dir.\n).
                                             qq(SaveOnly 	- the call will be only saved to the user_dir\n).
                                             qq(None 	- only the announcement will be played - no voice file will\n).
                                             qq(              be recorded\n)
                                           },

                          }
                  },
  };

1;
