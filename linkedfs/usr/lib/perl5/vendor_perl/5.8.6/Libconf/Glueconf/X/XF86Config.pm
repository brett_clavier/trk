#!/usr/bin/perl

# Author : Damien KROTKINE (damien@tuxfamily.org)
#
# Contributors :
#
# Copyright (C) 2002 Damien KROTKINE (damien@tuxfamily.org)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Glueconf::X::XF86Config;
use strict;
use vars qw(@ISA);
use Libconf qw(:all);
use Libconf::Glueconf;
use Libconf::Templates::XF86Config;
@ISA = qw(Libconf::Glueconf);
our $data_synopsis;

my @keywords_option = ();
my @keywords_list = ();

my @META_SECTIONS = qw(InputDevice Device Monitor Modes Screen ServerLayout Vendor);
my @LOAD_SECTIONS = qw(Module);
my @DISPLAY_SECTIONS = qw(Screen);
my @INPUTDEVICE_SECTIONS = qw (ServerLayout);
my @SCREEN_SECTIONS = qw(ServerLayout);
#my @OPTIONS_SECTIONS = qw();

sub new {
    my ($class, $filename) = @_;
    my $libconf = new Libconf::Templates::XF86Config({filename => $filename});
    $libconf->read_conf();
    tie my %wrapper, 'Libconf::Glueconf::X::XF86Config::Wrapper', $libconf;
    bless \%wrapper, $class;
}

package Libconf::Glueconf::X::XF86Config::Wrapper;
use Libconf qw(:all);

sub TIEHASH {
    my ($pkg, $libconf) = @_;
    debug("Wrapper - TIEHASH");
    bless { libconf => $libconf }, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug("Wrapper - CLEAR");
    $obj->{libconf}->clear;
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("Wrapper - DELETE - key: $key");
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [], section_name => $key });
    foreach (@pos) {
        $obj->{libconf}->delete_section($_);
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug("Wrapper - FIRSTKEY");
    my $atom = $obj->{libconf}->get_atom(0);
    $atom->{type} eq 'SECTION' or die "atom n� 0 is not a section\n";
    $atom->{section_name};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("Wrapper - EXISTS - key : $key");
    my $pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [], section_name => $key });
    defined $pos;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug("Wrapper - NEXTKEY - lastkey : $lastkey");
    my @sections_pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [] });
#    print "----*****>>>>>>" .  $obj->{libconf}{atoms}[$_]{section_name} . "\n" foreach @sections_pos;
    my $i = 0;
    my %h_idxToPos = map { ($i++, $_) } @sections_pos;
    my %h_posToIdx = reverse %h_idxToPos;
    use Data::Dumper;
#    print Dumper(\%h_idxToPos) . "\n";
#    print Dumper(\%h_posToIdx) . "\n";

    #positions of sections of names $lastkeys
    my @pos = $obj->{libconf}->find_atom_pos( { type => 'SECTION', sections => [], section_name => $lastkey });

    #the following is very tricky. you better ask me
    #Shortly, it is used to handle correctly this:
    #
    # Section "Monitor"
    # Section "InputDevice"
    # Section "InputDevice"
    # Section "Monitor"
    # section "InputDevice"
    # Section "Monitor"
    # Section "foo"
    #
    # NEXTKEY('Monitor') should return 'InputDevice'
    # NEXTKEY('InputDevice') should return 'foo'
    my $newidx;
    if (@pos > 1) {
        my $i;
        foreach (0..@pos-2) {
            $i = $_;
            $h_posToIdx{$pos[$i]}+1 != $h_posToIdx{$pos[$i+1]} and last;
        }
        $newidx = $h_posToIdx{$pos[$i]}+1;
    } else {
        $newidx = $h_posToIdx{$pos[0]}+1;
    }
    my $nextpos = noduppos($obj, $newidx, %h_idxToPos);
    defined $nextpos or return undef;
    my $ret = $obj->{libconf}->get_atom($nextpos)->{section_name};
    debug("Wrapper - nextkey : $ret");
    $ret;
}

sub noduppos {
    my ($obj, $idx, %h_idxToPos) = @_;
    defined $idx or return undef;
    my $pos = $h_idxToPos{$idx};
    defined $pos or return undef;
    my $firstpos = ($obj->{libconf}->find_atom_pos({type=>'SECTION', sections=>[], section_name=>$obj->{libconf}->get_atom($pos)->{section_name}}))[0];
    $pos == $firstpos and return $pos;
    noduppos($obj, $idx+1, %h_idxToPos);
}

#    print "Wrapper - pos : $pos\n";
#    my $nextpos = $h_idxToSection{$h_sectionToIdx{$pos}+1};
#    print "Wrapper - nextpos : $nextpos\n";
#    defined $nextpos or return undef;
#    my $ret = $obj->{libconf}->get_atom($nextpos)->{section_name};
#    print "Wrapper - nextkey : $ret\n";
#    $ret;
#}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug("Wrapper - STORE - key : $key - value : $value");
    ref $value eq 'HASH' or die 'trying to store anything else than hash ref';
    my %hash;
    if (member($key, @META_SECTIONS)) {
        tie %hash, 'Libconf::Glueconf::X::XF86Config::MetaSectionWrapper', $obj->{libconf}, $key;
    } else {
        my $index;
        my @pos = $obj->{libconf}->find_atom_pos({ type => 'SECTION', section_name => $key, sections => [] });
        if (@pos == 0) {
            $index = $obj->{libconf}->append_atom({ section_name => $key, type => 'SECTION' });
        } else {
            $index = $pos[0];
        }
    tie %hash, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $index;
    }
    %hash = %$value;
}

sub FETCH {
    my ($obj, $key) = @_;
    debug("Wrapper - FETCH - key : $key");
    $key eq 'libconf' and return $obj->{libconf};
    my %ret;
    if (Libconf::member($key, @META_SECTIONS)) {
        tie %ret, 'Libconf::Glueconf::X::XF86Config::MetaSectionWrapper', $obj->{libconf}, $key;
        return \%ret;
    }
    my @section_positions = $obj->{libconf}->find_atom_pos( { type => 'SECTION', section_name => $key, sections => [ ] });
    @section_positions > 1 and die "there are multiple sections named $key, but I don't know how to handle them";
    tie %ret, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $section_positions[0];
    \%ret;
}

package Libconf::Glueconf::X::XF86Config::MetaSectionWrapper;
use Libconf qw(:all);

sub TIEHASH {
    my ($pkg, $libconf, $metasection_name) = @_;
    debug("MetaSectionWrapper - TIEHASH - meta name : $metasection_name");
    my %hash = (
                libconf => $libconf,
                metasection_name => $metasection_name
               );
    return bless \%hash, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    my $metasection_name = $obj->{metasection_name};
    my $libconf = $obj->{libconf};
    debug("MetaSectionWrapper - CLEAR $metasection_name ");
    my @metasection_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => $metasection_name, sections => [ ] });
    $obj->{libconf}->clear_section($_) foreach (@metasection_pos);
}

#sub DESTROY {
#}

sub DELETE {
    my ($obj, $key) = @_;
    my $metasection_name = $obj->{metasection_name};
    my $libconf = $obj->{libconf};
    debug("MetaSectionWrapper - DELETE - metasection_name : $metasection_name | key : $key");
    my @metasections_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => $metasection_name, sections => [ ] });
    foreach my $section_pos (@metasections_pos) {
        my %section;
        tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $section_pos;
        $section{Identifier} eq $key and undef %section;
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    my $metasection_name = $obj->{metasection_name};
    my $libconf = $obj->{libconf};
    debug("MetaSectionWrapper - FIRSTKEY - metasection_name : $metasection_name ");
    my $first_metasection_pos = ($libconf->find_atom_pos( { type => 'SECTION', section_name => $metasection_name, sections => [ ] }))[0];
    defined $first_metasection_pos or return undef;
    my %section;
    tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $first_metasection_pos;
    debug("MetaSectionWrapper - FIRSTKEY - ret : " . $section{Identifier} . "");
    $section{Identifier};
}

sub EXISTS {
    my ($obj, $key) = @_;
    my $metasection_name = $obj->{metasection_name};
    debug("MetaSectionWrapper - EXISTS - metasection_name : $metasection_name - key : $key ");
    return defined (scalar($obj->{libconf}->find_atom_pos({ type => 'KEY_VALUE',
                                                   key => 'Identifier',
                                                   value => $key,
                                                   sections => [ {name => $metasection_name} ],
                                                 }))) ? 1 : 0;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    my $metasection_name = $obj->{metasection_name};
    my $libconf = $obj->{libconf};
    debug("MetaSectionWrapper - NEXTKEY - metasection_name : $metasection_name - lastkey : $lastkey");
    my @metasections_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => $metasection_name, sections => [ ] });
    foreach my $pos (0..@metasections_pos-1) {
        my $section_pos = $metasections_pos[$pos];
        my %section;
        tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $section_pos;
        if ($section{Identifier} eq $lastkey) {
            my $next_section_pos = $metasections_pos[$pos+1];
            defined $next_section_pos or return undef;
            my %next_section;
            tie %next_section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $next_section_pos;
            return $next_section{Identifier};
        }
    }
    undef;
}

sub STORE {
    my ($obj, $key, $value) = @_;
    my $libconf = $obj->{libconf};
    my $metasection_name = $obj->{metasection_name};
    debug("MetaSectionWrapper - STORE - metasection_name : $metasection_name - key : $key - value : $value");
    if (ref $value eq 'HASH') {
        my @metasections_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => $metasection_name, sections => [ ] });
        foreach my $section_pos (@metasections_pos) {
            my %section;
            tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $section_pos;
            if ($section{Identifier} eq $key) {
                %section = %$value;
                return;
            }
        }
        my $index;
        my $last_pos = $obj->{libconf}->get_section_end_atom($metasections_pos[-1]);
        $index = $obj->{libconf}->insert_atom($last_pos+1, { section_name => $metasection_name,
                                                            type => 'SECTION',
                                                            sections => [ ],
                                                          });
        $obj->{libconf}->insert_atom($last_pos+2, { type => 'KEY_VALUE',
                                                   key => 'Identifier',
                                                   value => $key,
                                                   sections => [ {name => $metasection_name} ],
                                                 });
        $obj->{libconf}->insert_atom($last_pos+3, { type => 'ENDSECTION',
                                                   sections => [ ],
                                                 });
        my %section;
        tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $index;
        %section = %$value;
    } else {
        die "wrong type of value : value is not a ref on hash";
    }
}

sub FETCH {
    my ($obj, $key) = @_;
    my $metasection_name = $obj->{metasection_name};
    my $libconf = $obj->{libconf};
    debug("MetaSectionWrapper - FETCH - metasection_name : $metasection_name - key : $key");
    my @metasections_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => $metasection_name, sections => [ ] });
    foreach my $section_pos (@metasections_pos) {
        my %section;
        tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $section_pos;
        if ($section{Identifier} eq $key) {
            return \%section;
        }
    }
}

package Libconf::Glueconf::X::XF86Config::SectionWrapper;
use Libconf qw(:all);

#Special behaviour : we regroup Load and Option as latest keys in any case

sub TIEHASH {
    my ($pkg, $libconf, $beginning) = @_;
    my $lastatom = $libconf->get_section_end_atom($beginning);
    my $section_name = $libconf->get_atom($beginning)->{section_name};
    my $atom = $libconf->get_atom($beginning);
    my @depth = @{$atom->{sections}};
    push @depth, { name => $atom->{section_name} };
    my %hash = (
                libconf => $libconf,
                firstatom => $beginning,
                sections => \@depth,
                lastatom => $lastatom,
                sectionname => $section_name,
               );
#    print Data::Dumper->Dump([%hash],['hash']) . "\n";

    debug("SectionWrapper - TIEHASH - firstatom : $hash{firstatom} - lastatom : $hash{lastatom} ");
    return bless \%hash, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug("SectionWrapper - CLEAR");
    $obj->{lastatom} -= $obj->{libconf}->clear_section($obj->{firstatom});
    $obj->{lastatom} == $obj->{firstatom} + 1 or die "removed atoms number is wrong";
}

sub DELETE {
    my ($obj, $key) = @_;
    debug("SectionWrapper - DELETE - key : $key");
    foreach ($obj->{firstatom}+1..$obj->{lastatom}-1) {
        my $atom = $obj->{libconf}->get_atom($_);
        if ($atom->{type} eq 'SECTION' &&
            $atom->{section_name} eq $key) {
            my $removed = $obj->{libconf}->delete_section($_);
            $obj->{lastatom} -= $removed;
            last;
        } elsif ($atom->{type} eq 'KEY_VALUE' &&
                 $atom->{key} eq $key) {
            $obj->{libconf}->delete_atom($_);
            $obj->{lastatom}--;
        } elsif ($atom->{type} eq 'VALUE' &&
                 $atom->{value} eq $key) {
            $obj->{libconf}->delete_atom($_);
            $obj->{lastatom}--;
        }
    }
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug("SectionWrapper - FIRSTKEY");
    if ($obj->{firstatom} + 1 == $obj->{lastatom}) {
        Libconf::member($obj->{sectionname}, @DISPLAY_SECTIONS) and return 'Display';
        Libconf::member($obj->{sectionname}, @INPUTDEVICE_SECTIONS) and return 'InputDevice';
        Libconf::member($obj->{sectionname}, @SCREEN_SECTIONS) and return 'Screen';
        Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) and return 'Load';
        return 'Option';
    }
    my $pos = $obj->{firstatom}+1;
  FIRSTKEY_loop:
    my $atom = $obj->{libconf}->get_atom($pos);
    if (!defined $atom) {
        Libconf::member($obj->{sectionname}, @DISPLAY_SECTIONS) and return 'Display';
        Libconf::member($obj->{sectionname}, @INPUTDEVICE_SECTIONS) and return 'InputDevice';
        Libconf::member($obj->{sectionname}, @SCREEN_SECTIONS) and return 'Screen';
        Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) and return 'Load';
        return 'Option';
    }
    $atom->{type2} eq 'XF86_LOAD' and $pos++, goto FIRSTKEY_loop;
    $atom->{type2} eq 'XF86_OPTION' and $pos++, goto FIRSTKEY_loop;
    $atom->{type} eq 'SECTION' and return $atom->{section_name};
    $atom->{type} eq 'KEY_VALUE' and return $atom->{key};
    $atom->{type} eq 'VALUE' and return $atom->{value};
    use Data::Dumper;
    die "atom n�" . $obj->{firstatom}+1 . " type unknown (not SECTION, KEY_VALUE, VALUE)";
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug("SectionWrapper - EXISTS - key : $key");
    $key eq 'Display' && Libconf::member($obj->{sectionname}, @DISPLAY_SECTIONS) and return 1;
    $key eq 'InputDevice' && Libconf::member($obj->{sectionname}, @INPUTDEVICE_SECTIONS) and return 1;
    $key eq 'Screen' && Libconf::member($obj->{sectionname}, @SCREEN_SECTIONS) and return 1;
    $key eq 'Load' && Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) and return 1;
    $key eq 'Option' and return 1;
    foreach ($obj->{firstatom}+1..$obj->{lastatom}-1) {
        my $atom = $obj->{libconf}->get_atom($_);
        ($atom->{type} eq 'SECTION' && $atom->{section_name} eq $key) ||
        ($atom->{type} eq 'KEY_VALUE' && $atom->{key} eq $key) and return 1;
        ($atom->{type} eq 'VALUE' && $atom->{value} eq $key) and return 1;
    }
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    my $libconf = $obj->{libconf};
    debug("SectionWrapper - NEXTKEY - lastkey : $lastkey");
    if ($lastkey eq 'Screen') {
        Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) and return 'Load';
        return 'Option';
    }
    if ($lastkey eq 'InputDevice') {
        Libconf::member($obj->{sectionname}, @SCREEN_SECTIONS) and return 'Screen';
        Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) and return 'Load';
        return 'Option';
    }
    if ($lastkey eq 'Display') {
        Libconf::member($obj->{sectionname}, @INPUTDEVICE_SECTIONS) and return 'InputDevice';
        Libconf::member($obj->{sectionname}, @SCREEN_SECTIONS) and return 'Screen';
        Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) and return 'Load';
        return 'Option';
    }
    $lastkey eq 'Load' and return 'Option';
    $lastkey eq 'Option' and return undef;
    my $next_pos;
    foreach ($obj->{firstatom}+1..$obj->{lastatom}-1) { #BUG, don't go into subsections, better use find_atom
        my $atom = $libconf->get_atom($_);
        if (($atom->{type} eq 'SECTION' && $atom->{section_name} eq $lastkey) ||
            ($atom->{type} eq 'KEY_VALUE' && ! exists($atom->{type2}) && $atom->{key} eq $lastkey) ||
            ($atom->{type} eq 'VALUE' && ! exists($atom->{type2}) && $atom->{value} eq $lastkey)) {
            $next_pos = $_+1;
          NEXTKEY_loop:
            if ($next_pos == $obj->{lastatom}-1) {
                Libconf::member($obj->{sectionname}, @DISPLAY_SECTIONS) and return 'Display';
                Libconf::member($obj->{sectionname}, @INPUTDEVICE_SECTIONS) and return 'InputDevice';
                Libconf::member($obj->{sectionname}, @SCREEN_SECTIONS) and return 'Screen';
                Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) and return 'Load';
                return 'Option';
            }
            $atom = $libconf->get_atom($next_pos);
            if (exists $atom->{type2}) {
                $next_pos++;
                goto NEXTKEY_loop;
            }
            if ($atom->{type} eq 'SECTION' && $atom->{section_name} eq 'Display') {
                $next_pos++;
                goto NEXTKEY_loop;
            }
            if ($atom->{type} eq 'SECTION') {
                return $atom->{section_name};
            }
            if ($atom->{type} eq 'KEY_VALUE') {
                return $atom->{key};
            }
            if ($atom->{type} eq 'VALUE') {
                return $atom->{value};
            }
        }
    }
    undef;
}

sub STORE {
    my ($obj, $key, $value) = @_;
    my $libconf = $obj->{libconf};
    debug("SectionWrapper - STORE - key : $key - value : $value");
    if ($key eq 'Display') {
        Libconf::member($obj->{sectionname}, @DISPLAY_SECTIONS) or die "you cannot store a Display metasection in " . $obj->{sectionname} . "\n";
        my @display;
        tie @display, 'Libconf::Glueconf::X::XF86Config::DisplaySectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom};
        ref $value eq 'ARRAY' or die 'wrong type (not ARRAY ref)';
        @display = @$value;
        return;
    }
    if ($key eq 'InputDevice') {
        my %inputdevice;
        tie %inputdevice, 'Libconf::Glueconf::X::XF86Config::OptionSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom}, 'XF86_INPUTDEVICE';
        ref $value eq 'HASH' or die 'wrong type (not HASH ref)';
        %inputdevice = %$value;
        return;
    }
    if ($key eq 'Screen') {
        my %screen;
        tie %screen, 'Libconf::Glueconf::X::XF86Config::OptionSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom}, 'XF86_SCREEN';
        ref $value eq 'HASH' or die 'wrong type (not HASH ref)';
        %screen = %$value;
        return;
    }
    if ($key eq 'Load') {
        Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) or die "you cannot store a Load metasection in " . $obj->{sectionname} . "\n";
        my @load;
        tie @load, 'Libconf::Glueconf::X::XF86Config::LoadSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom};
        ref $value eq 'ARRAY' or die 'wrong type (not ARRAY ref)';
        @load = @$value;
        return;
    }
    if ($key eq 'Option') {
        my %option;
        tie %option, 'Libconf::Glueconf::X::XF86Config::OptionSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom}, 'XF86_OPTION';
        ref $value eq 'HASH' or die 'wrong type (not HASH ref)';
        %option = %$value;
        return;
    }
    my $index;
    if (ref $value eq 'HASH') {
        foreach ($obj->{firstatom}+1..$obj->{lastatom}-1) {
            my $atom = $libconf->get_atom($_);
            ($atom->{type} eq 'SECTION' && $atom->{section_name} eq $key) and $index = $_, last;
        }
        if (!defined $index) {
            $index = $libconf->insert_atom($obj->{lastatom}, { section_name => $key,
                                                                    type => 'SECTION',
                                                                    sections => [ @{$obj->{sections}} ],
                                                                  } );
            $libconf->insert_atom($obj->{lastatom}+1, { type => 'ENDSECTION',
                                                              sections => [ @{$obj->{sections}} ],
                                                            });
            $obj->{lastatom} += 2;
        }
        my %hash;
        tie %hash, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $index;
        %hash = %$value;
    } elsif (ref $value eq '') {
        foreach ($obj->{firstatom}+1..$obj->{lastatom}-1) {
            my $atom = $libconf->get_atom($_);
            ($atom->{type} eq 'KEY_VALUE' && $atom->{key} eq $key) and $index = $_, last;
        }
        if (!defined $index) {
            $index = $libconf->insert_atom($obj->{lastatom}, { key => $key,
                                                                    type => 'KEY_VALUE',
                                                                    sections => [ @{$obj->{sections}} ],
                                                                  } );
            $obj->{lastatom}++;
        }
        $libconf->editAtom($index, { value => $value });
    } else {
        die "not implemented yet";
    }
}

sub FETCH {
    my ($obj, $key) = @_;
    my $libconf = $obj->{libconf};
    debug("SectionWrapper - FETCH - key : $key");
    if ($key eq 'Display') {
        Libconf::member($obj->{sectionname}, @DISPLAY_SECTIONS) or return undef;
        my @display;
        tie @display, 'Libconf::Glueconf::X::XF86Config::DisplaySectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom};
        return \@display;
    }
    if ($key eq 'InputDevice') {
        my %inputdevice;
        tie %inputdevice, 'Libconf::Glueconf::X::XF86Config::OptionSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom}, 'XF86_INPUTDEVICE';
        return \%inputdevice;
    }
    if ($key eq 'Screen') {
        my %screen;
        tie %screen, 'Libconf::Glueconf::X::XF86Config::OptionSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom}, 'XF86_SCREEN';
        return \%screen;
    }
    if ($key eq 'Load') {
        Libconf::member($obj->{sectionname}, @LOAD_SECTIONS) or return undef;
        my @load;
        tie @load, 'Libconf::Glueconf::X::XF86Config::LoadSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom};
        return \@load;
    }
    if ($key eq 'Option') {
        my %option;
        tie %option, 'Libconf::Glueconf::X::XF86Config::OptionSectionWrapper', $libconf, $obj->{firstatom}, $obj->{lastatom}, 'XF86_OPTION';
        return \%option;
    }
    my $index;
    foreach ($obj->{firstatom}+1..$obj->{lastatom}-1) { #BUG, don't go into subsections, better use find_atom
        my $atom = $libconf->get_atom($_);
        ($atom->{type} eq 'SECTION' && $atom->{section_name} eq $key) ||
        ($atom->{type} eq 'KEY_VALUE' && $atom->{key} eq $key) and $index = $_, last;
        ($atom->{type} eq 'VALUE' && $atom->{value} eq $key) and $index = $_, last;
    }
    defined $index or return undef;
    my $atom = $libconf->get_atom($index);
    if ($atom->{type} eq 'SECTION') {
        my %ret;
        tie %ret, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $libconf, $index;
        return \%ret;
    }
    if ($atom->{type} eq 'KEY_VALUE') {
        return $atom->{value};
    }
    if ($atom->{type} eq 'VALUE') {
        return $atom->{value};
    }
    die "unknown type";
}

package Libconf::Glueconf::X::XF86Config::DisplaySectionWrapper;
use Libconf qw(:all);

#Special behaviour : we regroup Load and Option as latest keys in any case

sub TIEARRAY {
    my ($pkg, $libconf, $first_atom, $last_atom) = @_;
    debug("DisplaySectionWrapper - TIEARRAY - first_atom : $first_atom");
    my $atom = $libconf->get_atom($first_atom);
    my @depth = @{$atom->{sections}};
    push @depth, { name => $atom->{section_name} };
    my %hash = (
                libconf => $libconf,
                firstatom => $first_atom,
                sections => \@depth,
                lastatom => $last_atom,
               );
#    print Data::Dumper->Dump([%hash],['hash']) . "\n";

    debug("DisplaySectionWrapper - TIEHASH - firstatom : $hash{firstatom} - lastatom : $hash{lastatom} ");
    return bless \%hash, $pkg;
}

sub FETCH {
    my ($obj, $idx) = @_;
    debug("idx : $idx");
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    $idx eq 'libconf' and return $libconf;
    my @display_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom);
    my %ret;
    tie %ret, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $display_pos[$idx];
    \%ret;
}

sub STORE {
    my ($obj, $idx, $value) = @_;
    debug("idx : $idx - value : $value");
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @display_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom);
    my %section;
    if ($idx > $#display_pos) {
        $libconf->insert_atom($display_pos[-1]+1, {type => 'SECTION', section_name => 'Display', sections => [ @depth ] });
        $libconf->insert_atom($display_pos[-1]+2, {type => 'ENDSECTION', sections => [ @depth ] });
        tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $display_pos[-1]+1;
        %section = %$value;
        $obj->{lastatom} += getSectionSize($display_pos[-1]+1);
    } else {
        my $size_before = getSectionSize($display_pos[$idx]);
        tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $display_pos[$idx];
        %section = %$value;
        my $size_after = getSectionSize($display_pos[$idx]);
        $obj->{lastatom} += $size_after-$size_before;
    }
}

sub FETCHSIZE {
    my ($obj) = @_;
    debug();
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @display_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom);
    scalar(@display_pos);
}

sub STORESIZE {
    my ($obj, $count) = @_;
    debug("count : $count");
    my $size = $obj->FETCHSIZE;
    if ($count > $size) {
        foreach ($count-$size..$count) {
            $obj->STORE($_, '');
        }
    } elsif ($count < $size) {
        foreach (0..$size-$count-2) {
            $obj->POP;
        }
    }
}

sub EXTEND {}

sub EXISTS {
    my ($obj, $index) = @_;
    debug("index : $index");
    defined $obj->FETCH($index);
}

sub CLEAR {
    my $obj = shift;
    debug("Clear");
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    foreach ($libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom)) {
        my @removed_items = $libconf->delete_section($_);
        $obj->{lastatom}-=@removed_items;
    }
}

sub PUSH {
    my $obj = shift;
    my @list = @_;
    debug("list : [" . join('|', @list) . "]");
    my $last = $obj->FETCHSIZE;
    $obj->STORE($last + $_, $list[$_]) foreach (0..$#list);
    $obj->FETCHSIZE;
}

sub POP {
    my $obj = shift;
    debug();
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @display_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom);
    my %value;
    tie %value, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $display_pos[-1];
    my %ret = %value; #deep copy
    my @removed_items = $libconf->delete_section($display_pos[-1]);
    $obj->{lastatom}-=@removed_items;
    \%ret;
}

sub SHIFT {
    my $obj = shift;
    debug();
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @display_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom);
    my %section;
    tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $display_pos[0];
    my %ret = %section; #deep copy
    my @removed_items = $libconf->delete_section($display_pos[0]);
    $obj->{lastatom}-=@removed_items;
    \%ret;
}

sub UNSHIFT {
    my ($obj, @list) = @_;
    debug("list : [" . join('|', @list));
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @display_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom);
    foreach (reverse @list) {
        $libconf->insert_atom($display_pos[0], {type => 'SECTION', section_name => 'Display', sections => [ @depth ] });
        $libconf->insert_atom($display_pos[0]+1, {type => 'ENDSECTION', sections => [ @depth ] });
        my %section;
        tie %section, 'Libconf::Glueconf::X::XF86Config::SectionWrapper', $obj->{libconf}, $display_pos[-1]+1;
        %section = %$_;
        $obj->{lastatom}+=getSectionSize($display_pos[0]);
    }
}

sub SPLICE {
    my $obj = shift;
    my $offset = shift || 0;
    my $length = shift || $obj->FETCHSIZE - $offset;
    my @list = @_;
    debug("offset : $offset - length : $length - list : [" . join('|', @list) . "]");
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @display_pos = $libconf->find_atom_pos( { type => 'SECTION', section_name => 'Display', sections => [ @depth ] }, $firstatom, $lastatom);
    my @ret;
    foreach (0..$length-1) {
        push @ret, $obj->FETCH($offset+$_);
        if (exists $list[$_]) {
            $obj->STORE($offset+$_, $list[$_]);
        } else {
            my @removed_items = $libconf->delete_section($display_pos[$offset+$_]);
            $obj->{lastatom}-=@removed_items;
        }
    }
    wantarray() ? @ret : $ret[-1];
}

package Libconf::Glueconf::X::XF86Config::LoadSectionWrapper;
use Libconf qw(:all);

#Special behaviour : we regroup Load and Option as latest keys in any case

sub TIEARRAY {
    my ($pkg, $libconf, $first_atom, $last_atom) = @_;
    debug "LoadSectionWrapper - TIEARRAY - first_atom : $first_atom";
    my $atom = $libconf->get_atom($first_atom);
    my @depth = @{$atom->{sections}};
    push @depth, { name => $atom->{section_name} };
    my %hash = (
                libconf => $libconf,
                firstatom => $first_atom,
                sections => \@depth,
                lastatom => $last_atom,
               );
#    print Data::Dumper->Dump([%hash],['hash']) . "\n";

    debug "LoadSectionWrapper - TIEHASH - firstatom : $hash{firstatom} - lastatom : $hash{lastatom} ";
    return bless \%hash, $pkg;
}

sub FETCH {
    my ($obj, $idx) = @_;
    debug("idx : $idx");
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    $idx eq 'libconf' and return $libconf;
    my @load_pos = $libconf->find_atom_pos( { type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom);
    $libconf->get_atom($load_pos[$idx])->{value};
}

sub STORE {
    my ($obj, $idx, $value) = @_;
    debug("idx : $idx - value : $value");
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @load_pos = $libconf->find_atom_pos( { type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom);
    if ($idx > $#load_pos) {
        $libconf->insert_atom($load_pos[-1]+1, {type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ], value => $value });
        $obj->{lastatom}++;
    } else {
        $libconf->editAtom($load_pos[$idx], {type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ], value => $value });
    }
}

sub FETCHSIZE {
    my ($obj) = @_;
    debug();
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @load_pos = $libconf->find_atom_pos( { type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom);
    scalar(@load_pos);
}

sub STORESIZE {
    my ($obj, $count) = @_;
    debug("count : $count");
    my $size = $obj->FETCHSIZE;
    if ($count > $size) {
        foreach ($count-$size..$count) {
            $obj->STORE($_, '');
        }
    } elsif ($count < $size) {
        foreach (0..$size-$count-2) {
            $obj->POP;
        }
    }
}

sub EXTEND {}

sub EXISTS {
    my ($obj, $index) = @_;
    debug("index : $index");
    defined $obj->FETCH($index);
}

sub CLEAR {
    my $obj = shift;
    debug();
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    foreach ($libconf->find_atom_pos( { type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom)) {
        $libconf->delete_atom($_);
        $obj->{lastatom}--;
    }
}

sub PUSH {
    my $obj = shift;
    my @list = @_;
    debug("list : [" . join('|', @list) . "]");
    my $last = $obj->FETCHSIZE;
    $obj->STORE($last + $_, $list[$_]) foreach (0..$#list);
    $obj->FETCHSIZE;
}

sub POP {
    my $obj = shift;
    debug();
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @load_pos = $libconf->find_atom_pos({ type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom);
    my $value = $libconf->get_atom($load_pos[-1])->{value};
    $libconf->delete_atom($load_pos[-1]);
    $obj->{lastatom}--;
    $value;
}

sub SHIFT {
    my $obj = shift;
    debug();
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @load_pos = $libconf->find_atom_pos({ type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom);
    my $value = $libconf->get_atom($load_pos[0])->{value};
    $libconf->delete_atom($load_pos[0]);
    $obj->{lastatom}--;
    $value;
}

sub UNSHIFT {
    my ($obj, @list) = @_;
    debug("list : [" . join('|', @list));
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @load_pos = $libconf->find_atom_pos({ type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom);
    foreach (reverse @list) {
        $libconf->insert_atom($load_pos[0], { type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ], value => $_ }) ;
        $obj->{lastatom}++;
    }
}

sub SPLICE {
    my $obj = shift;
    my $offset = shift || 0;
    my $length = shift || $obj->FETCHSIZE - $offset;
    my @list = @_;
    debug("offset : $offset - length : $length - list : [" . join('|', @list) . "]");
    my ($libconf, $firstatom, $lastatom) = @{%$obj}{qw(libconf firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @load_pos = $libconf->find_atom_pos({ type => 'VALUE', type2 => 'XF86_LOAD', sections => [ @depth ] }, $firstatom, $lastatom);
    my @ret;
    foreach (0..$length-1) {
        push @ret, $obj->FETCH($offset+$_);
        if (exists $list[$_]) {
            $obj->STORE($offset+$_, $list[$_]);
        } else {
            $libconf->delete_atom($load_pos[$offset+$_]);
            $obj->{lastatom}--;
        }
    }
    wantarray() ? @ret : $ret[-1];
}

package Libconf::Glueconf::X::XF86Config::OptionSectionWrapper;
use Libconf qw(:all);

sub TIEHASH {
    my ($pkg, $libconf, $first_atom, $last_atom, $type) = @_;
    debug "OptionSectionWrapper - TIEHASH - first_atom : $first_atom - last_atom : $last_atom - type : $type";
    $type or die "you specify the type from : 'XF86_INPUTDEVICE', 'XF86_SCREEN', 'XF86_OPTION'";
    my $atom = $libconf->get_atom($first_atom);
    my @depth = @{$atom->{sections}};
    push @depth, { name => $atom->{section_name} };
    my %hash = (
                libconf => $libconf,
                firstatom => $first_atom,
                sections => \@depth,
                lastatom => $last_atom,
                type => $type,
               );
#    print Data::Dumper->Dump([%hash],['hash']) . "\n";
    debug "OptionSectionWrapper - TIEHASH - firstatom : $hash{firstatom} - lastatom : $hash{lastatom} ";
    return bless \%hash, $pkg;
}

sub CLEAR {
    my ($obj) = @_;
    debug "OptionSectionWrapper - CLEAR";
    my ($libconf, $type, $firstatom, $lastatom) = @{%$obj}{qw(libconf type firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @opt_pos = $libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ] }, $firstatom, $lastatom);
    $libconf->delete_atom($_) foreach (@opt_pos);
    $obj->{lastatom} -= scalar(@opt_pos);
}

sub DELETE {
    my ($obj, $key) = @_;
    debug "OptionSectionWrapper - DELETE - key : $key";
    my ($libconf, $type, $firstatom, $lastatom) = @{%$obj}{qw(libconf type firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @opt_pos = $libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ], key => $key }, $firstatom, $lastatom);
    $libconf->delete_atom($_) foreach (@opt_pos);
    $obj->{lastatom} -= scalar(@opt_pos);
}

sub FIRSTKEY {
    my ($obj) = @_;
    debug "OptionSectionWrapper - FIRSTKEY";
    my ($libconf, $type, $firstatom, $lastatom) = @{%$obj}{qw(libconf type firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my $first_atom = $libconf->get_atom(($libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ]}, $firstatom, $lastatom))[0]);
    debug " --- ret : " . $first_atom->{key} . "";
    $first_atom->{key};
}

sub EXISTS {
    my ($obj, $key) = @_;
    debug "OptionSectionWrapper - EXISTS - key : $key";
    my ($libconf, $type, $firstatom, $lastatom) = @{%$obj}{qw(libconf type firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    $libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ], key => $key}, $firstatom, $lastatom) > 0;
}

sub NEXTKEY {
    my ($obj, $lastkey) = @_;
    debug "OptionSectionWrapper - NEXTKEY - lastkey : $lastkey";
    my ($libconf, $type, $firstatom, $lastatom) = @{%$obj}{qw(libconf type firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my @opt_pos = $libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ]}, $firstatom, $lastatom);
    my $lastkeypos = $libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ], key => $lastkey}, $firstatom, $lastatom);
    defined $lastkeypos or return undef;
    foreach my $i (0..@opt_pos-1) {
        if ($opt_pos[$i] == $lastkeypos) {
            my $next_pos = $opt_pos[$i+1];
            defined $next_pos or return undef;
            return $libconf->get_atom($opt_pos[$i+1])->{key};
        }
    }
    undef;
}

sub STORE {
    my ($obj, $key, $value) = @_;
    debug "OptionSectionWrapper - STORE - key : $key - value : $value";
    my ($libconf, $type, $firstatom, $lastatom) = @{%$obj}{qw(libconf type firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    my $keypos = $libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ], key => $key}, $firstatom, $lastatom);
    if (!defined $keypos) {
        my $lastoptpos = $libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ]}, $firstatom, $lastatom);
        $libconf->insert_atom($lastoptpos+1, { type => 'KEY_VALUE', type2 => $type, sections => [ @depth ], key => $key, value => $value} );
        $obj->{lastatom}++;
    } else {
        $libconf->editAtom({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ]}, key => $key, value => $value );
    }
}

sub FETCH {
    my ($obj, $key) = @_;
    debug "OptionSectionWrapper - FETCH - key : $key";
    my ($libconf, $type, $firstatom, $lastatom) = @{%$obj}{qw(libconf type firstatom lastatom)};
    my @depth = @{$obj->{sections}};
    $libconf->get_atom(scalar($libconf->find_atom_pos({ type => 'KEY_VALUE', type2 => $type, sections => [ @depth ], key => $key}, $firstatom, $lastatom)))->{value};
}


#            Files          File pathnames
#            ServerFlags    Server flags
#            Module         Dynamic module loading
#            InputDevice    Input device description
#            Device         Graphics device description
#            VideoAdaptor   Xv video adaptor description
#            Monitor        Monitor description
#            Modes          Video modes descriptions
#            Screen         Screen configuration
#            ServerLayout   Overall layout
#            DRI            DRI-specific configuration
#            Vendor         Vendor-specific configuration


# 1;

# my @synopsis = (
#                 {
#                  type => 'SECTION',
#                  section_name => 'Files',
#                  content => [
#                              {
#                               key => 'FontPath',
#                               type => 'KEY_VALUE',
#                              },
#                              {
#                               key => 'RGBPath',
#                               type => 'KEY_VALUE',
#                              },
#                              {
#                               key => 'ModulePath',
#                               type => 'KEY_VALUE',
#                              },
#                             ],
#                 },
#                 {
#                  type => 'SECTION',
#                  section_name => 'ServerFlags',
#                  content => [
#                              {
#                               key => 'NoTrapSignals',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'DontZap',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'DontZoom',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'DisableVidModeExtension',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'AllowNonLocalXvidtune',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'DisableModInDev',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'AllowNonLocalModInDev',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'AllowMouseOpenFail',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'VTInit',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                              },
#                              {
#                               key => 'VTSysReq',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'BlankTime',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                              },
#                              {
#                               key => 'StandbyTime',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                              },
#                              {
#                               key => 'SuspendTime',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                              },
#                              {
#                               key => 'OffTime',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                              },
#                              {
#                               key => 'Pixmap',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                              },
#                              {
#                               key => 'PC98',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'NoPM',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                              {
#                               key => 'Xinerama',
#                               type => 'KEY_VALUE',
#                               type2 => 'XF86_OPTION',
#                               value => 'BOOLEAN',
#                              },
#                             ],
#                 },
#                );

# sub new {
#     my ($pkg, $filename) = @_;
#     my $self = $pkg->Libconf::new($filename, 'XF86Config', '');
#     my $ret = Libconf::Glueconf::generateFunctions(@synopsis);
#     print " plop : $ret\n";
#     eval Libconf::Glueconf::generateFunctions(@synopsis);
#     $@ and die $@;
#     $self;
# }

1;

