
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Resolv;
use strict;
use vars qw(@ISA);
use Libconf qw(:helpers);
use Libconf::Templates;
use Libconf::Templates::Generic::Value;
use Libconf::Templates::Generic::List;
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::Samba - Libconf low level template to handle resolv.conf style conf files

=head1 DESCRIPTION

Libconf::Templates::Samba is a template that handles resolv.conf config files.

=head1 SYNOPSIS

 my $template = new Libconf::Templates::Resolv({ filename => '/etc/resolv.conf' });
 ...
 (see L<Libconf::Templates> for transformation methods on $template)
 ...
$template->write_conf();

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

 my $template = new Libconf::Templates::Resolv({ filename => '/etc/resolv.conf' });

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my $ref_atoms = [];
    my $value_nameserver_template = new Libconf::Templates::Generic::Value({
                                                                            regexp => '^\s*nameserver\s+(\S+)\s*$',
                                                                            output_function => sub { 'nameserver ' . $_[0]->{value} }
                                                                           });
    $value_nameserver_template->{atoms} = $self->{atoms};
    my $value_domain_template = new Libconf::Templates::Generic::Value({
                                                                        regexp => '^\s*domain\s+(\S+)\s*$',
                                                                        output_function => sub { 'domain ' . $_[0]->{value} }
                                                                       });
    $value_domain_template->{atoms} = $self->{atoms};
    my $list_search_template = new Libconf::Templates::Generic::List({
                                                                      _input_function => sub {
                                                                          my ($self, $in, $atom, $parser_env) = @_;
                                                                          if ($in =~ s/^\s*search\s+(.+\S)\s*$//) {
                                                                              $atom->{type} = 'LIST';
                                                                              $atom->{type2} = 'RESOLV_SEARCH';
                                                                              $atom->{list} = [ split(' ', $1) ];
                                                                              return ($in, 'stop');
                                                                          }
                                                                          ($in, 'failed');
                                                                      },
                                                                      output_function => sub { join(' ', 'search', @{$_[0]->{list}}) }
                                                                     });
    $list_search_template->{atoms} = $self->{atoms};
    my $list_sortlist_template = new Libconf::Templates::Generic::List({
                                                                        _input_function => sub {
                                                                            my ($self, $in, $atom, $parser_env) = @_;
                                                                            if ($in =~ s/^\s*sortlist\s+(.+\S)\s*$//) {
                                                                                $atom->{type} = 'LIST';
                                                                                $atom->{type2} = 'RESOLV_SORTLIST';
                                                                                $atom->{list} = [ split(' ', $1) ];
                                                                                return ($in, 'stop');
                                                                            }
                                                                            ($in, 'failed');
                                                                        },
                                                                        output_function => sub { join(' ', 'search', @{$_[0]->{list}}) }
                                                                       });
    $list_sortlist_template->{atoms} = $self->{atoms};    
    my $list_options_template = new Libconf::Templates::Generic::List({
                                                                       _input_function => sub {
                                                                          my ($self, $in, $atom, $parser_env) = @_;
                                                                          if ($in =~ s/^\s*options\s+(.+\S)\s*$//) {
                                                                              $atom->{type} = 'LIST';
                                                                              $atom->{type2} = 'RESOLV_OPTIONS';
                                                                              $atom->{list} = [ split(' ', $1) ];
                                                                              return ($in, 'stop');
                                                                          }
                                                                          ($in, 'failed');
                                                                      },
                                                                      output_function => sub { join(' ', 'search', @{$_[0]->{list}}) }
                                                                     });
    $list_options_template->{atoms} = $self->{atoms};    

    bless put_in_hash($self, {
                              filename => $args->{filename},
                              template_name => 'Resolv',
                              value_nameserver_template => $value_nameserver_template,
                              value_domain_template => $value_domain_template,
                              list_search_template => $list_search_template,
                              list_sortlist_template => $list_sortlist_template,
                              list_options_template => $list_options_template,
                              comments_struct => [['#'], [';']],
                             }), $class;
}

# private function, called by Libconf::Template::read_conf
sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    # rule for value
    ($out, $command) = $self->{value_nameserver_template}->_parse_chunk($in, $atom, $parser_env);
    if ($command ne 'failed') {
        $atom->{type2} = 'RESOLV_NAMESERVER';
        return ($out, $command);
    }

    ($out, $command) = $self->{value_domain_template}->_parse_chunk($in, $atom, $parser_env);
    if ($command ne 'failed') {
        $atom->{type2} = 'RESOLV_DOMAIN';
        return ($out, $command);
    }

    # rule for list
    ($out, $command) = $self->{list_search_template}->_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    ($out, $command) = $self->{list_sortlist_template}->_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    ($out, $command) = $self->{list_options_template}->_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    $self->{keyvalue_template}->_parse_chunk($in, $atom, $parser_env);
}

# private function, called by Libconf::Template::write_conf
sub _output_atom {
    my ($self, $atom, $parser_env) = @_;

    # rule for value
    $atom->{type2} eq 'RESOLV_NAMESERVER' and
      return $self->{value_nameserver_template}->_output_atom($atom, $parser_env);
    $atom->{type2} eq 'RESOLV_DOMAIN' and
      return $self->{value_domain_template}->_output_atom($atom, $parser_env);
    $atom->{type2} eq 'RESOLV_SEARCH' and
      return $self->{list_search_template}->_output_atom($atom, $parser_env);
    $atom->{type2} eq 'RESOLV_SORTLIST' and
      return $self->{list_sortlist_template}->_output_atom($atom, $parser_env);

    $self->{list_options_template}->_output_atom($atom, $parser_env);
}

1
