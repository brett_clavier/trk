
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Shell;
use strict;
use vars qw(@ISA);
use Libconf::Templates::Generic::KeyValue;
@ISA = qw(Libconf::Templates::Generic::KeyValue);

=head1 NAME

Libconf::Templates::Shell - Libconf low level template for shell styles config files

=head1 DESCRIPTION

Libconf::Templates::Shell is a template that handles the files that contain 'shell like' informations.

=head1 SYNOPSIS

 my $template = new Libconf::Templates::Shell({ filename => 'some_file' });
 $template->read_conf();
 ...
 (see L<Libconf::Templates> for transformation methods on $template)
 ...
 $template_write_conf();

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Shell({
                                                filename => 'some_file',
                                           })
  $template = new Libconf::Templates::Shell({
                                                filename => 'some_file',
						shell_style => 'true_bash',
						shell_command => '/bin/bash',
                                                export_line => 1,
                                                export_only_defined => 1,
                                           })

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

shell_style [B<type> : STRING, B<default> : 'sh', B<values>: 'sh', 'csh','true_bash'] :
specifies the shell type syntax to use. If 'sh' is used, the
key=value is used, if 'csh' is used, the setenv key value is used. An example
to transform a sh style config file to a csh one :

 my $template = new Libconf::Templates::Shell({ filename => 'some_file' });
 $template->read_conf();
 $template->{shell_style} = 'csh';
 $template_write_conf();

If 'true_bash' is used, then the template will call the shell binary to interpret
the values. If 'true_bash' is set, then 'shell_command' has to be set also

shell_command [B<type> : STRING] : required if shell_style is set to true_bash.
This will be used to call the shell binary. Examples : shell_command =>
'/bin/bash'. It will be used to interpret the values. It is useful when the
values uses special bash command (like variable evaluation)

export_line [B<type> : BOOLEAN, B<default> : 0 ] : if true, and if applicable
(shell_style = sh), a line will be added at the beginning of the file to export the
defined variables in the file, like :

 export http_proxy ftp_proxy

export_only_defined [B<type> : BOOLEAN, B<default> : 1 ] : if true, and if
export_line is true, the added line will export only non void variables. If
false, the added line will export all variables, even if they are void.

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the complete list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific method

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new( {
                                    separator_char => '=',
                                    allow_space => 0,
                                    comments_struct => [['#']],
                                    handle_quote => 1,
                                   });
    my $default_args = {
                        filename => '',
			shell_style => 'sh',
                        export_line => 0,
                        export_only_defined => 1,
                        template_name => 'Shell',
                       };
    $self->init_args($args, $default_args);
    $self->{shell_style} eq 'sh' || $self->{shell_style} eq 'csh' || $self->{shell_style} eq 'true_bash'
      or die "shell_style must be one of this value : 'sh' or 'csh'";
    bless $self, $class;
}

# private function, called by Libconf::Template::read_conf
sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    if ($parser_env->{multiline_atom}) {
        $parser_env->{multiline_atom} = 0;
        my $previous_atom = $parser_env->{previous_atom};
        $parser_env->{previous_atom} = '';
        return ($previous_atom . " $in", 'switch_to_prev_atom');
    }

    my $rule1 = '\\\s*$';
    if (!$parser_env->{multiline_atom} && $in =~ s/$rule1//o) {
        $parser_env->{multiline_atom} = 1;
        $parser_env->{previous_atom} = $in;
        return ('', 'stop');
    }
    if ($self->{shell_style} eq 'sh' || $self->{shell_style} eq 'true_bash') {
        if ($self->{export_line}) {
            # remove lines like  ``export var1 var2 var3''
            $in =~ s/^\s*export ?(\w+\s+)+\s*\w*\s*//;
            # removes only the ``export'' in lines like : export foo=bar
            $in =~ s/^\s*export\s+//;
        }
        # parent rules
        my $command;
        if ($self->{shell_style} eq 'true_bash') {
            my $rule1 = '^\s*(.+?)' . $self->{separator_char} . '.*$';
            if ($in =~ s/$rule1//) {
                $atom->{type} = 'KEY_VALUE';
                $atom->{key} = $1;
                $atom->{value} = _get_shell_value($atom->{key}, $self->{filename}, $self->{shell_command});
                !$self->{simplify_quote} && $atom->{value} =~ /^("|').*\1$/ and $atom->{quote} = $1;
                return ($in, 'stop');
            }
            return ('', 'next_chunk');  #we silently ignore errors
        } else {
            return $self->SUPER::_parse_chunk($in, $atom);
        }
    } elsif ($in =~ s/^\s*setenv\s+(\w+)\s+("([^"]*)"|'([^']*)'|[^'"\s]+)\s*$//) {
        $atom->{type} = 'KEY_VALUE';
        $atom->{key} = $1;
        $atom->{value} = defined $3 ? $3 : $2;
	return ($in, 'stop');
    }
    ($in, 'failed');
}

use FileHandle;
use IPC::Open2;
my $init_shell_com = 0;
# private help function, to communicate with the shell
sub _get_shell_value {
    my ($key, $filename, $shell_command) = @_;
    $key or die "key asked to the shell empty/wrong";
    $filename or die "filename asked to the shell empty/wrong";
    $shell_command or die "shell_command to the shell empty/wrong";
    if (!$init_shell_com) {
        $init_shell_com = open2(*Reader, *Writer, $shell_command );
        print Writer "source $filename\n";
    }
    print Writer "echo \$$key\n";
    my $got = <Reader>;
    chomp $got;
    $got;
}

# private function, called by Libconf::Template::read_conf
sub _parse_eof {
    my ($self, $parser_env) = @_;
    $parser_env->{previous_atom} ne '' and warn "this chunk had no endline : " . $parser_env->{previous_atom};
    $parser_env = {
                   previous_atom => '',
                   multiline_atom => 0,
                  };
    close Reader;
    close Writer;
    $self->SUPER::_parse_eof();
}

# private function, called by Libconf::Template::write_conf
sub _output_header {
    my ($self, $parser_env) = @_;
    my ($footer, $indent) = $self->SUPER::_output_header();
    if ($self->{export_line} && $self->{shell_style} eq 'sh') {
        my @list = map { $_->{key} } @{$self->{atoms}};
        if ($self->{export_only_defined}) {
            @list = map { $_->{key} } grep { exists $_->{value} && defined $_->{value} && $_->{value} ne '' } @{$self->{atoms}};
        }
        @list > 0 and $footer .= "export " . join(' ', @list) . "\n";
    }
    return ($footer, $indent);
}

1
