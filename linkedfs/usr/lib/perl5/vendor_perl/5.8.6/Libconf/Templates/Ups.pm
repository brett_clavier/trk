
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Ups;
use strict;
use vars qw(@ISA);
use Libconf qw(:helpers);
use Libconf::Templates;
use Libconf::Templates::Generic::KeyValueSections;
@ISA = qw(Libconf::Templates::Generic::KeyValueSections);

=head1 NAME

Libconf::Templates::Ups - Libconf low level template to handle Ups ups.conf style conf files

=head1 DESCRIPTION

Libconf::Templates::Ups - Libconf low level template to handle Ups ups.conf style conf files

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

 my $template = new Libconf::Templates::Ups({ filename => '/etc/ups/ups.conf' });

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut


sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new({
                                   filename => $args->{filename},
                                   separator_char => '=',
                                   allow_space => 1,
                                   handle_quote => 0,
                                   handle_multiples_lines => 0,
                                   section_regexp => '^\s*\[([^\]]+)\]\s*$',
                                   section_output_function => sub { "[$_[0]]" },
                                   has_endsection => 0,
                                   endsection_regexp => '',
                                   endsection_output_function => '',
                                   accept_empty_value => 1,
                                  });
    bless $self, $class;
}

1
