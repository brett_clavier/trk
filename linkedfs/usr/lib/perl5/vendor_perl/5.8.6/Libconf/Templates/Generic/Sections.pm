
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::Generic::Sections;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
use Libconf qw(:helpers); # for compare_depth()
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::Generic::Sections - Libconf generic low level template for semantic association (SECTION) rules

=head1 DESCRIPTION

Libconf::Templates::Generic::Sections is a generic template that handles config files that contain semantic informations of type : (SECTION).

=head1 SYNOPSIS

 $template = new Libconf::Templates::Generic::Section({
                                               section_regexp => '^\s*\[([^\]]+)\]\s*$',
                                               section_output_function => sub { "[$_->[0]]" },
                                               has_endsection => 0,
                                               endsection_regexp => '',
                                               endsection_output_function => '',
                                               comments_struct => [['#']],
                                              });
 $template->read_conf();
 ...
 (see L<Libconf::Templates> for transformation methods on $template) ...
 ...
 $template->write_conf("output_file");

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

  $template = new Libconf::Templates::Generic::Section({
                                                         section_regexp => '^\s*\[([^\]]+)\]\s*$',
                                                         section_output_function => sub { "[$_->[0]]" },
                                                         has_endsection => 0,
                                                         endsection_regexp => '',
                                                         endsection_output_function => '',
                                                         comments_struct => [['#']],
                                                       });

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

section_regexp [B<type> : STRING, B<default> : '^\s*\[([^\]]+)\]\s*$'] : the
regexp that is applied to determine if a line is a section start. $1 has to be
the section name.

section_output_function [B<type> : FUNCTION_REF, B<default> : sub { "[$_->[0]]"
} ] : the code to be applied to a section atom, to generate the line to output.
It takes in arguments the name of the section, the atom structure, and
$parser_env, the parser environment variable. It should return the line to be
written

has_endsection [B<type> : BOOLEAN, B<default> : 1] : if true, section have both
start and end tag. If false, a section is considered to end where another
section begins, or at the end of the file

endsection_output_function [B<type> : FUNCTION_REF, B<default> : sub { '' } ] :
the code to be applied to a section atom, to generate the end section line to
output. It takes in arguments the name of the section, the atom structure, and
$parser_env, the parser environment variable. It should return the line to be
written

endsection_regexp [B<type> : STRING, B<default> : ''] : the regexp that is
applied to determine if a line is a end of a section.

comments_struct [B<type> : ARRAY_REF of ARRAY_REF of STRING,STRING,SCALAR,
B<default> : [['#']] ] : defines the type and the characters of comments. See
L<Libconf::Templates::Keyvalue> for additional details on it

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific method

=cut

# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my $default_args = {
                        section_regexp => '^\s*\[([^\]]+)\]\s*$',
                        section_output_function => sub { "[$_->[0]]" },
                        has_endsection => 0,
                        endsection_regexp => '',
                        endsection_output_function => '',
                        comments_struct => [['#']],
                        template_name => 'Generic::Section',
                       };
    bless $self->init_args($args, $default_args), $class;
}

sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom);
    $command eq 'failed' or return ($out, $command);

    # rule section start
    my $regexp = $self->{section_regexp};
    if ($in =~ s/$regexp//) {
        $atom->{type} = 'SECTION';
        $atom->{section_name} = $1;
        if (!$self->{has_endsection}) {
            # in this case, a section ends the previous section
            pop @{$parser_env->{current_sections}};
        }
        $atom->{sections} = [ @{$parser_env->{current_sections}} ];
        push @{$parser_env->{current_sections}}, { name => $1 };
        return ($in, 'stop');
    }

    if ($self->{has_endsection}) {
        my $regexp = $self->{endsection_regexp};
        if ($in =~ s/$regexp//) {
            $atom->{type} = 'ENDSECTION';
            $atom->{sections} = [ @{$parser_env->{current_sections}} ];
            pop @{$parser_env->{current_sections}};
            return ($in, 'stop');
        }
    }
    ($in, 'failed');
}

# _parse_eof()

# if we have something in $parser_env->{current_sections}} and sections have
# end tags, then we need to create the missing closing tag at the end of
# parsing.

sub _parse_eof {
    my ($self, $parser_env) = @_;
    $self->SUPER::_parse_eof();
    if ($self->{has_endsection}) {
        foreach(1..@{$parser_env->{current_sections}}) {
            $self->append_atom({
                                type => 'ENDSECTION',
                                sections => [ @{$parser_env->{current_sections}} ],
                               });
            pop @{$parser_env->{current_sections}};
        }
    }
}

sub _output_atom {
    my ($self, $atom, $parser_env) = @_;
    if ($atom->{type} eq 'SECTION') {
        return ($self->{section_output_function}->($atom->{section_name}, $atom, $parser_env), scalar(@{$atom->{sections}}));
    } elsif ($atom->{type} eq 'ENDSECTION') {
        return ($self->{endsection_output_function}->($atom->{section_name}, $atom, $parser_env), scalar(@{$atom->{sections}})-1);
    }
    $self->SUPER::_output_atom($atom);
}

1
