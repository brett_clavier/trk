
# Author : damien KROTKINE (dams@libconf.net)
#
#
# Copyright (C) 2004 damien KROTKINE (dams@libconf.net)
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

package Libconf::Templates::XF86Config;
use strict;
use vars qw(@ISA);
use Libconf::Templates;
use Libconf::Templates::Generic::KeyValue;
use Libconf::Templates::Generic::Sections;
@ISA = qw(Libconf::Templates);

=head1 NAME

Libconf::Templates::XF86Config - Libconf low level template to handle XF86config or xorg.conf style config files

=head1 DESCRIPTION

Libconf::Templates::XF86Config is a template that handles XF86Config and xorg.conf config files.

=head1 SYNOPSIS

=head1 CONSTRUCTOR

=over

=item B<new($options)>

creates the template

 my $template = new Libconf::Templates::XF86Config({ filename => '/etc/X11/xorg.conf' });

B<arguments>

$options [B<type> : HASH_REF] specifies the options to create the new template instance.

B<options>

filename [B<type> : STRING, B<default> : ''] : the filename of the config file
you want to work on. Can be read and written lately by using set_filename and
get_filename.

=back

=head1 GENERAL METHODS

See L<Libconf::Templates> for the general list of methods you can call on this
template.

=head1 SPECIFIC METHODS

There is no specific methods

=cut



# constructor
sub new {
    my ($class, $args) = @_;
    my $self = $class->SUPER::new();
    my @atoms = ();
    my $ref_atoms = \@atoms;
    my $keyvalue_template = new Libconf::Templates::Generic::KeyValue({
                                                                       separator_char => '=',
                                                                       allow_space => 1,
                                                                       handle_quote => 0,
                                                                       handle_multiples_lines => 0,
                                                                      });
    $keyvalue_template->{atoms} = $ref_atoms;
    my $sections_template = new Libconf::Templates::Generic::Sections({
                                                                       section_regexp => '^\s*(?:Subs|SubS|S)ection\s+\"([^\"]+)\"\s*$',
                                                                       section_output_function => sub {
                                                                           my ($section_name, $atom, $parser_env) = @_;
                                                                           (@{$atom->{sections}}>0 ? 'Subs' :'S').qq(ection "$section_name");
                                                                       },
                                                                       has_endsection => 1,
                                                                       endsection_regexp => '^\s*End(?:Subs|SubS|S)ection\s*$',
                                                                       endsection_output_function => sub {
                                                                           my ($section_name, $atom, $parser_env) = @_;
                                                                           'End'.(@{$atom->{sections}} > 1 ? 'Subs' : 'S') . 'ection';
                                                                       },
                                                                      });
    $sections_template->{atoms} = $ref_atoms;
    my $self = {
                atoms => $ref_atoms,
                filename => $args->{filename},
                template_name => 'XF86Config',
                keyvalue_template => $keyvalue_template,
                sections_template => $sections_template,
                comments_struct => [['#'], [';']],
               };
    bless $self, $class;
}

# private function, called by Libconf::Template::read_conf
sub _parse_chunk {
    my ($self, $in, $atom, $parser_env) = @_;

    # parent rules
    my ($out, $command) = $self->SUPER::_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    # rule for sections
    ($out, $command) = $self->{sections_template}->_parse_chunk($in, $atom, $parser_env);
    $command eq 'failed' or return ($out, $command);

    # rules
    if ($in =~ s/^\s*InputDevice\s+"([^"]+)"\s+(.*)\s*$//) {
        $atom->{type} = 'KEY_VALUE';
        $atom->{key} = $1;
        $atom->{type2} = 'XF86_INPUTDEVICE';
        $atom->{value} = $2;
        return ($in, 'stop');
    }
    if ($in =~ s/^\s*Screen\s+([^\s"])*\s*"([^"]+)"\s*(.*)\s*$//) {
        $atom->{type} = 'KEY_VALUE';
        $atom->{type2} = 'XF86_SCREEN';
        $atom->{key} = $2; #BUG : the screen-id is not kept for now
        $atom->{value} = $3;
        return ($in, 'stop');
    }
    if ($in =~ s/^\s*Option\s+(.*)\s*$//) {
        $atom->{type} = 'KEY_VALUE';
        $atom->{type2} = 'XF86_OPTION';
        my $tmp = $1;
        $tmp =~ s/^\s*"([^"]*)"// and $atom->{key} = $1;
        $tmp =~ s/^\s*"([^"]*)"// and $atom->{value} = $1;
        $in = $tmp; #to allow the engine to check if some stuff were not interpreted
        return ($in, 'stop');
    }
    if ($in =~ s/^\s*Load\s*(.*)\s*$//) {
        $atom->{type} = 'VALUE';
        $atom->{type2} = 'XF86_LOAD';
        my $tmp = $1;
        $tmp =~ s/^\s*"([^"]*)"// and $atom->{value} = $1;
        $atom->{value} = $1;
        $in = $tmp; #to allow the engine to check if some stuff were not interpreted
        return ($in, 'stop');
    }
    if ($in =~ s/^\s*(\S+)\s+(.*)\s*$//) {
        $atom->{type} = 'KEY_VALUE';
        $atom->{key} = $1;
        $atom->{value} = $2;
        return ($in, 'stop');
    }
    if ($in =~ s/^\s*(\S+)\s*$//) {
        $atom->{type} = 'VALUE';
        $atom->{value} = $1;
        return ($in, 'stop');
    }
    ($in, 'failed');
}

# private function, called by Libconf::Template::write_conf
sub _output_atom {
    my ($self, $atom, $parser_env) = @_;

    if ($atom->{type} eq 'VALUE') {
        my ($value) = $atom->{value};
        return ($atom->{type2} eq 'XF86_LOAD' ? 
                qq(Load "$value") : 
                $atom->{value}, scalar(@{$atom->{sections}})
               );
    }
    if ($atom->{type} eq 'KEY_VALUE') {
        my ($key, $value, $type2) = ($atom->{key}, $atom->{value}, $atom->{type2});
        $type2 eq 'XF86_OPTION' and return (qq(Option "$key" "$value"), scalar(@{$atom->{sections}}));
        $type2 eq 'XF86_INPUTDEVICE' and return (qq(InputDevice "$key" $value), scalar(@{$atom->{sections}}));
        $type2 eq 'XF86_SCREEN' and return (qq(Screen "$key" $value), scalar(@{$atom->{sections}}));
        return (qq($key $value), scalar(@{$atom->{sections}}));
    }
    $self->{sections_template}->_output_atom($atom, $parser_env);
}

1
