=head1 NAME

Gtk2::Gdk::Event::Setting

=for position post_hierarchy

=head1 HIERARCHY

  Gtk2::Gdk::Event
  +----Gtk2::Gdk::Event::Setting

=cut




=head1 METHODS

=head2 settingaction = $eventsetting-E<gt>B<action> ($newvalue=0)

=over

=over

=item * $newvalue (Gtk2::Gdk::SettingAction) 

=back

=back

=head2 string or undef = $eventsetting-E<gt>B<name> ($newvalue=undef)

=over

=over

=item * $newvalue (string or undef) 

=back

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

