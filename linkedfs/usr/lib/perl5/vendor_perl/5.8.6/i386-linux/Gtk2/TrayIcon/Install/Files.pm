package Gtk2::TrayIcon::Install::Files;

$self = {
          'inc' => '-DXTHREADS -D_REENTRANT -DXUSE_MTSAFE_API -I/usr/include/gtk-2.0 -I/usr/lib/gtk-2.0/include -I/usr/X11R6/include -I/usr/include/atk-1.0 -I/usr/include/pango-1.0 -I/usr/include/freetype2 -I/usr/include/freetype2/freetype2 -I/usr/include/freetype2/freetype2/config -I/usr/include/glib-2.0 -I/usr/lib/glib-2.0/include   -DEGG_COMPILATION',
          'typemaps' => [
                          'trayicon.typemap'
                        ],
          'deps' => [
                      'Glib',
                      'Gtk2'
                    ],
          'libs' => '-Wl,--export-dynamic -lgtk-x11-2.0 -lgdk-x11-2.0 -latk-1.0 -lgdk_pixbuf-2.0 -lm -lpangoxft-1.0 -lpangox-1.0 -lpango-1.0 -lgobject-2.0 -lgmodule-2.0 -ldl -lglib-2.0  '
        };


# this is for backwards compatiblity
@deps = @{ $self->{deps} };
@typemaps = @{ $self->{typemaps} };
$libs = $self->{libs};
$inc = $self->{inc};

	$CORE = undef;
	foreach (@INC) {
		if ( -f $_ . "/Gtk2/TrayIcon/Install/Files.pm") {
			$CORE = $_ . "/Gtk2/TrayIcon/Install/";
			last;
		}
	}

1;
