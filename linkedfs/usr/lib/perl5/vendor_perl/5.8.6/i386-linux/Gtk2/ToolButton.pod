=head1 NAME

Gtk2::ToolButton

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Bin
                          +----Gtk2::ToolItem
                                +----Gtk2::ToolButton

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 toolitem = Gtk2::ToolButton-E<gt>B<new> ($icon_widget, $label)

=over

=over

=item * $icon_widget (Gtk2::Widget or undef) 

=item * $label (string or undef) 

=back

=back

=head2 toolitem = Gtk2::ToolButton-E<gt>B<new_from_stock> ($stock_id)

=over

=over

=item * $stock_id (string) 

=back

=back

=head2 widget or undef = $button-E<gt>B<get_icon_widget> 

=over

=back

=head2 $button-E<gt>B<set_icon_widget> ($icon_widget)

=over

=over

=item * $icon_widget (Gtk2::Widget or undef) 

=back

=back

=head2 string or undef = $button-E<gt>B<get_label> 

=over

=back

=head2 $button-E<gt>B<set_label> ($label)

=over

=over

=item * $label (string or undef) 

=back

=back

=head2 widget or undef = $button-E<gt>B<get_label_widget> 

=over

=back

=head2 $button-E<gt>B<set_label_widget> ($label_widget)

=over

=over

=item * $label_widget (Gtk2::Widget or undef) 

=back

=back

=head2 string = $button-E<gt>B<get_stock_id> 

=over

=back

=head2 $button-E<gt>B<set_stock_id> ($stock_id)

=over

=over

=item * $stock_id (string or undef) 

=back

=back

=head2 boolean = $button-E<gt>B<get_use_underline> 

=over

=back

=head2 $button-E<gt>B<set_use_underline> ($use_underline)

=over

=over

=item * $use_underline (boolean) 

=back

=back


=head1 PROPERTIES

=over

=item 'icon-widget' (Gtk2::Widget : readable / writable)

Icon widget to display in the item

=item 'label' (string : readable / writable)

Text to show in the item.

=item 'label-widget' (Gtk2::Widget : readable / writable)

Widget to use as the item label

=item 'stock-id' (string : readable / writable)

The stock icon displayed on the item

=item 'use-underline' (boolean : readable / writable)

If set, an underline in the label property indicates that the next character should be used for the mnemonic accelerator key in the overflow menu

=back


=head1 SIGNALS

=over

=item B<clicked> (Gtk2::ToolButton)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Bin>, L<Gtk2::ToolItem>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

