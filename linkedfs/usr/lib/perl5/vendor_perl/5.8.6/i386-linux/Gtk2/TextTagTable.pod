=head1 NAME

Gtk2::TextTagTable

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::TextTagTable


=head1 METHODS

=head2 texttagtable = Gtk2::TextTagTable-E<gt>B<new> 

=over

=back

=head2 $table-E<gt>B<add> ($tag)

=over

=over

=item * $tag (Gtk2::TextTag) 

=back

=back

=head2 $table-E<gt>B<foreach> ($callback, $callback_data=undef)

=over

=over

=item * $callback (scalar) 

=item * $callback_data (scalar) 

=back

=back

=head2 texttag = $table-E<gt>B<lookup> ($name)

=over

=over

=item * $name (string) 

=back

=back

=head2 $table-E<gt>B<remove> ($tag)

=over

=over

=item * $tag (Gtk2::TextTag) 

=back

=back

=head2 integer = $table-E<gt>B<get_size> 

=over

=back


=head1 SIGNALS

=over

=item B<tag-changed> (Gtk2::TextTagTable, Gtk2::TextTag, boolean)

=item B<tag-added> (Gtk2::TextTagTable, Gtk2::TextTag)

=item B<tag-removed> (Gtk2::TextTagTable, Gtk2::TextTag)

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

