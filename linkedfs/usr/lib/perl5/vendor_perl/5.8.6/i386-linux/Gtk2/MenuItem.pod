=head1 NAME

Gtk2::MenuItem

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Bin
                          +----Gtk2::Item
                                +----Gtk2::MenuItem

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::MenuItem-E<gt>B<new> ($label=undef)

=over

=over

=item * $label (string) 

=back

=back

=head2 widget = Gtk2::MenuItem-E<gt>B<new_with_label> ($label=undef)

=over

=over

=item * $label (string) 

=back

=back

=head2 widget = Gtk2::MenuItem-E<gt>B<new_with_mnemonic> ($label=undef)

=over

=over

=item * $label (string) 

=back

=back

=head2 $menu_item-E<gt>B<set_accel_path> ($accel_path)

=over

=over

=item * $accel_path (string) 

=back

=back

=head2 $menu_item-E<gt>B<activate> 

=over

=back

=head2 $menu_item-E<gt>B<deselect> 

=over

=back

=head2 $menu_item-E<gt>B<remove_submenu> 

=over

=back

=head2 boolean = $menu_item-E<gt>B<get_right_justified> 

=over

=back

=head2 $menu_item-E<gt>B<set_right_justified> ($right_justified)

=over

=over

=item * $right_justified (boolean) 

=back

=back

=head2 $menu_item-E<gt>B<select> 

=over

=back

=head2 widget or undef = $menu_item-E<gt>B<get_submenu> 

=over

=back

=head2 $menu_item-E<gt>B<set_submenu> ($submenu)

=over

=over

=item * $submenu (Gtk2::Widget) 

=back

=back

=head2 $menu_item-E<gt>B<toggle_size_allocate> ($allocation)

=over

=over

=item * $allocation (integer) 

=back

=back

=head2 requisition = $menu_item-E<gt>B<toggle_size_request> 

=over

=back


=head1 SIGNALS

=over

=item B<activate> (Gtk2::MenuItem)

=item B<activate-item> (Gtk2::MenuItem)

=item B<toggle-size-request> (Gtk2::MenuItem, gpointer)

=item B<toggle-size-allocate> (Gtk2::MenuItem, integer)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>, L<Gtk2::Bin>, L<Gtk2::Item>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

