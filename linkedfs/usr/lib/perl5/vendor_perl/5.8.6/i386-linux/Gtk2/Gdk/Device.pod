=head1 NAME

Gtk2::Gdk::Device

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Gdk::Device


=head1 METHODS

=head2 list = $device-E<gt>B<axes> 

=over


Returns a list of hash references that resemble the I<GdkDeviceAxis> structure,
i.e. that have three keys: "use", "min", and "max".


=back

=head2 double = $device-E<gt>B<get_axis> ($use, ...)

=over

=over

=item * $use (Gtk2::Gdk::AxisUse) 

=item * ... (list) of axis values such as the one returned by L<get_state>

=back




=back

=head2 $device-E<gt>B<set_axis_use> ($index_, $use)

=over

=over

=item * $index_ (integer) 

=item * $use (Gtk2::Gdk::AxisUse) 

=back

=back

=head2 device = Gtk2::Gdk::Device-E<gt>B<get_core_pointer> 

=over

=back

=head2 boolean = $device-E<gt>B<has_cursor> 

=over

=back

=head2 list = $device-E<gt>B<get_history> ($window, $start, $stop)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=item * $start (unsigned) 

=item * $stop (unsigned) 

=back


Returns a list of hash references that resemble the I<GdkTimeCoord> structure,
i.e. that have two keys: "time" and "axes".


=back

=head2 $device-E<gt>B<set_key> ($index_, $keyval, $modifiers)

=over

=over

=item * $index_ (integer) 

=item * $keyval (integer) 

=item * $modifiers (Gtk2::Gdk::ModifierType) 

=back

=back

=head2 list = $device-E<gt>B<keys> 

=over


Returns a list of hash references that resemble the I<GdkDeviceKey> structure,
i.e. that have two keys: "keyval" and "modifiers".


=back

=head2 inputmode = $device-E<gt>B<mode> 

=over

=back

=head2 boolean = $device-E<gt>B<set_mode> ($mode)

=over

=over

=item * $mode (Gtk2::Gdk::InputMode) 

=back

=back

=head2 string = $device-E<gt>B<name> 

=over

=back

=head2 inputsource = $device-E<gt>B<source> 

=over

=back

=head2 $device-E<gt>B<set_source> ($source)

=over

=over

=item * $source (Gtk2::Gdk::InputSource) 

=back

=back

=head2 list = $device-E<gt>B<get_state> ($window)

=over

=over

=item * $window (Gtk2::Gdk::Window) 

=back


Returns the modifier mask and a list of values of the axes.


=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::ModifierType

=over

=item * 'shift-mask' / 'GDK_SHIFT_MASK'

=item * 'lock-mask' / 'GDK_LOCK_MASK'

=item * 'control-mask' / 'GDK_CONTROL_MASK'

=item * 'mod1-mask' / 'GDK_MOD1_MASK'

=item * 'mod2-mask' / 'GDK_MOD2_MASK'

=item * 'mod3-mask' / 'GDK_MOD3_MASK'

=item * 'mod4-mask' / 'GDK_MOD4_MASK'

=item * 'mod5-mask' / 'GDK_MOD5_MASK'

=item * 'button1-mask' / 'GDK_BUTTON1_MASK'

=item * 'button2-mask' / 'GDK_BUTTON2_MASK'

=item * 'button3-mask' / 'GDK_BUTTON3_MASK'

=item * 'button4-mask' / 'GDK_BUTTON4_MASK'

=item * 'button5-mask' / 'GDK_BUTTON5_MASK'

=item * 'release-mask' / 'GDK_RELEASE_MASK'

=item * 'modifier-mask' / 'GDK_MODIFIER_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

