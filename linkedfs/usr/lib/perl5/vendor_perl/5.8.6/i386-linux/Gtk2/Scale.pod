=head1 NAME

Gtk2::Scale

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Range
                    +----Gtk2::Scale

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 integer = $scale-E<gt>B<get_digits> 

=over

=back

=head2 $scale-E<gt>B<set_digits> ($digits)

=over

=over

=item * $digits (integer) 

=back

=back

=head2 boolean = $scale-E<gt>B<get_draw_value> 

=over

=back

=head2 $scale-E<gt>B<set_draw_value> ($draw_value)

=over

=over

=item * $draw_value (boolean) 

=back

=back

=head2 layout = $scale-E<gt>B<get_layout> 

=over

=back

=head2 (x, y) = $scale-E<gt>B<get_layout_offsets> 

=over

=back

=head2 positiontype = $scale-E<gt>B<get_value_pos> 

=over

=back

=head2 $scale-E<gt>B<set_value_pos> ($pos)

=over

=over

=item * $pos (Gtk2::PositionType) 

=back

=back


=head1 PROPERTIES

=over

=item 'digits' (integer : readable / writable)

The number of decimal places that are displayed in the value

=item 'draw-value' (boolean : readable / writable)

Whether the current value is displayed as a string next to the slider

=item 'value-pos' (Gtk2::PositionType : readable / writable)

The position in which the current value is displayed

=back


=head1 SIGNALS

=over

=item string = B<format-value> (Gtk2::Scale, double)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Range>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

