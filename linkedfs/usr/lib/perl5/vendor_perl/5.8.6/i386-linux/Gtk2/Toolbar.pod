=head1 NAME

Gtk2::Toolbar

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::Toolbar

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::Toolbar-E<gt>B<new> 

=over

=back

=head2 widget = $toolbar-E<gt>B<append_element> ($type, $widget, $text, $tooltip_text, $tooltip_private_text, $icon, $callback=undef, $user_data=undef)

=over

=over

=item * $type (Gtk2::ToolbarChildType) 

=item * $widget (Gtk2::Widget or undef) 

=item * $text (string) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $icon (Gtk2::Widget or undef) 

=item * $callback (subroutine) 

=item * $user_data (scalar) 

=back



=back

=head2 widget = $toolbar-E<gt>B<append_item> ($text, $tooltip_text, $tooltip_private_text, $icon, $callback=undef, $user_data=undef)

=over

=over

=item * $text (string) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $icon (Gtk2::Widget or undef) 

=item * $callback (subroutine) 

=item * $user_data (scalar) 

=back



=back

=head2 $toolbar-E<gt>B<append_space> 

=over

=back

=head2 $toolbar-E<gt>B<append_widget> ($widget, $tooltip_text, $tooltip_private_text)

=over

=over

=item * $widget (Gtk2::Widget) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=back



=back

=head2 $toolbar-E<gt>B<set_drop_highlight_item> ($tool_item, $index)

=over

=over

=item * $tool_item (Gtk2::ToolItem or undef) 

=item * $index (integer) 

=back

=back

=head2 integer = $toolbar-E<gt>B<get_drop_index> ($x, $y)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 iconsize = $toolbar-E<gt>B<get_icon_size> 

=over

=back

=head2 $toolbar-E<gt>B<set_icon_size> ($icon_size)

=over

=over

=item * $icon_size (Gtk2::IconSize) 

=back

=back

=head2 $toolbar-E<gt>B<insert> ($item, $pos)

=over

=over

=item * $item (Gtk2::ToolItem) 

=item * $pos (integer) 

=back

=back

=head2 widget = $toolbar-E<gt>B<insert_element> ($type, $widget, $text, $tooltip_text, $tooltip_private_text, $icon, $callback, $user_data, $position)

=over

=over

=item * $type (Gtk2::ToolbarChildType) 

=item * $widget (Gtk2::Widget or undef) 

=item * $text (string) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $icon (Gtk2::Widget or undef) 

=item * $callback (subroutine) 

=item * $user_data (scalar) 

=item * $position (integer) 

=back



=back

=head2 widget = $toolbar-E<gt>B<insert_item> ($text, $tooltip_text, $tooltip_private_text, $icon, $callback, $user_data, $position)

=over

=over

=item * $text (string) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $icon (Gtk2::Widget or undef) 

=item * $callback (subroutine) 

=item * $user_data (scalar) 

=item * $position (integer) 

=back



=back

=head2 $toolbar-E<gt>B<insert_space> ($position)

=over

=over

=item * $position (integer) 

=back

=back

=head2 widget = $toolbar-E<gt>B<insert_stock> ($stock_id, $tooltip_text, $tooltip_private_text, $callback, $user_data, $position)

=over

=over

=item * $stock_id (string) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $callback (subroutine) 

=item * $user_data (scalar) 

=item * $position (integer) 

=back



=back

=head2 $toolbar-E<gt>B<insert_widget> ($widget, $tooltip_text, $tooltip_private_text, $position)

=over

=over

=item * $widget (Gtk2::Widget) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $position (integer) 

=back



=back

=head2 integer = $toolbar-E<gt>B<get_item_index> ($item)

=over

=over

=item * $item (Gtk2::ToolItem) 

=back

=back

=head2 integer = $toolbar-E<gt>B<get_n_items> 

=over

=back

=head2 toolitem or undef = $toolbar-E<gt>B<get_nth_item> ($n)

=over

=over

=item * $n (integer) 

=back

=back

=head2 orientation = $toolbar-E<gt>B<get_orientation> 

=over

=back

=head2 $toolbar-E<gt>B<set_orientation> ($orientation)

=over

=over

=item * $orientation (Gtk2::Orientation) 

=back

=back

=head2 widget = $toolbar-E<gt>B<prepend_element> ($type, $widget, $text, $tooltip_text, $tooltip_private_text, $icon, $callback=undef, $user_data=undef)

=over

=over

=item * $type (Gtk2::ToolbarChildType) 

=item * $widget (Gtk2::Widget or undef) 

=item * $text (string) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $icon (Gtk2::Widget or undef) 

=item * $callback (subroutine) 

=item * $user_data (scalar) 

=back



=back

=head2 widget = $toolbar-E<gt>B<prepend_item> ($text, $tooltip_text, $tooltip_private_text, $icon, $callback=undef, $user_data=undef)

=over

=over

=item * $text (string) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=item * $icon (Gtk2::Widget or undef) 

=item * $callback (subroutine) 

=item * $user_data (scalar) 

=back



=back

=head2 $toolbar-E<gt>B<prepend_space> 

=over

=back

=head2 $toolbar-E<gt>B<prepend_widget> ($widget, $tooltip_text, $tooltip_private_text)

=over

=over

=item * $widget (Gtk2::Widget) 

=item * $tooltip_text (string or undef) 

=item * $tooltip_private_text (string or undef) 

=back



=back

=head2 reliefstyle = $toolbar-E<gt>B<get_relief_style> 

=over

=back

=head2 $toolbar-E<gt>B<remove_space> ($position)

=over

=over

=item * $position (integer) 

=back

=back

=head2 boolean = $toolbar-E<gt>B<get_show_arrow> 

=over

=back

=head2 $toolbar-E<gt>B<set_show_arrow> ($show_arrow)

=over

=over

=item * $show_arrow (boolean) 

=back

=back

=head2 toolbarstyle = $toolbar-E<gt>B<get_style> 

=over

=back

=head2 $toolbar-E<gt>B<set_style> ($style)

=over

=over

=item * $style (Gtk2::ToolbarStyle) 

=back

=back

=head2 boolean = $toolbar-E<gt>B<get_tooltips> 

=over

=back

=head2 $toolbar-E<gt>B<set_tooltips> ($enable)

=over

=over

=item * $enable (boolean) 

=back

=back

=head2 $toolbar-E<gt>B<unset_icon_size> 

=over

=back

=head2 $toolbar-E<gt>B<unset_style> 

=over

=back


=head1 PROPERTIES

=over

=item 'orientation' (Gtk2::Orientation : readable / writable)

The orientation of the toolbar

=item 'show-arrow' (boolean : readable / writable)

If an arrow should be shown if the toolbar doesn't fit

=item 'toolbar-style' (Gtk2::ToolbarStyle : readable / writable)

How to draw the toolbar

=back


=head1 SIGNALS

=over

=item boolean = B<move-focus> (Gtk2::Toolbar, Gtk2::DirectionType)

=item B<orientation-changed> (Gtk2::Toolbar, Gtk2::Orientation)

=item B<style-changed> (Gtk2::Toolbar, Gtk2::ToolbarStyle)

=item boolean = B<popup-context-menu> (Gtk2::Toolbar, integer, integer, integer)

=item boolean = B<focus-home-or-end> (Gtk2::Toolbar, boolean)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

