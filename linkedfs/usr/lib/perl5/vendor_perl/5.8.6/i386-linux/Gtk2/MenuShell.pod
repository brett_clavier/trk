=head1 NAME

Gtk2::MenuShell

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::MenuShell

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 $menu_shell-E<gt>B<activate_item> ($menu_item, $force_deactivate)

=over

=over

=item * $menu_item (Gtk2::Widget) 

=item * $force_deactivate (boolean) 

=back

=back

=head2 $menu_shell-E<gt>B<append> ($child)

=over

=over

=item * $child (Gtk2::Widget) 

=back

=back

=head2 $menu_shell-E<gt>B<cancel> 

=over

=back

=head2 $menu_shell-E<gt>B<deactivate> 

=over

=back

=head2 $menu_shell-E<gt>B<deselect> 

=over

=back

=head2 $menu_shell-E<gt>B<insert> ($child, $position)

=over

=over

=item * $child (Gtk2::Widget) 

=item * $position (integer) 

=back

=back

=head2 $menu_shell-E<gt>B<prepend> ($child)

=over

=over

=item * $child (Gtk2::Widget) 

=back

=back

=head2 $menu_shell-E<gt>B<select_first> ($search_sensitive)

=over

=over

=item * $search_sensitive (boolean) 

=back

=back

=head2 $menu_shell-E<gt>B<select_item> ($menu_item)

=over

=over

=item * $menu_item (Gtk2::Widget) 

=back

=back


=head1 SIGNALS

=over

=item B<deactivate> (Gtk2::MenuShell)

=item B<selection-done> (Gtk2::MenuShell)

=item B<move-current> (Gtk2::MenuShell, Gtk2::MenuDirectionType)

=item B<activate-current> (Gtk2::MenuShell, boolean)

=item B<cancel> (Gtk2::MenuShell)

=item B<cycle-focus> (Gtk2::MenuShell, Gtk2::DirectionType)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

