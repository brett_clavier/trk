=head1 NAME

Gtk2::WindowGroup

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::WindowGroup


=head1 METHODS

=head2 windowgroup = Gtk2::WindowGroup-E<gt>B<new> 

=over

=back

=head2 $window_group-E<gt>B<add_window> ($window)

=over

=over

=item * $window (Gtk2::Window) 

=back

=back

=head2 $window_group-E<gt>B<remove_window> ($window)

=over

=over

=item * $window (Gtk2::Window) 

=back

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

