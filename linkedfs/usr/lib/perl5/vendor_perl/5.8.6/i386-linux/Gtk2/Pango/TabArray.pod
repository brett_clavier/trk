=head1 NAME

Gtk2::Pango::TabArray

=head1 HIERARCHY

  Glib::Boxed
  +----Gtk2::Pango::TabArray


=head1 METHODS

=head2 tabarray = Gtk2::Pango::TabArray-E<gt>B<new> ($initial_size, $positions_in_pixels, ...)

=over

=over

=item * $initial_size (integer) 

=item * $positions_in_pixels (boolean) 

=item * ... (list) pairs of Gtk2::PangoTabAlign's and integers, the alignments and positions of the tab stops.

=back



=back

=head2 tabarray = Gtk2::Pango::TabArray-E<gt>B<new_with_positions> ($initial_size, $positions_in_pixels, ...)

=over

=over

=item * $initial_size (integer) 

=item * $positions_in_pixels (boolean) 

=item * ... (list) pairs of Gtk2::PangoTabAlign's and integers, the alignments and positions of the tab stops.

=back

Alias for L<new|tabarray = Gtk2::Pango::TabArray-E<gt>new ($initial_size, $positions_in_pixels, ...)>.

=back

=head2 boolean = $tab_array-E<gt>B<get_positions_in_pixels> 

=over

=back

=head2 $tab_array-E<gt>B<resize> ($new_size)

=over

=over

=item * $new_size (integer) 

=back

=back

=head2 integer = $tab_array-E<gt>B<get_size> 

=over

=back

=head2 (alignment, location) = $tab_array-E<gt>B<get_tab> ($tab_index)

=over

=over

=item * $tab_index (integer) 

=back

=back

=head2 $tab_array-E<gt>B<set_tab> ($tab_index, $alignment, $location)

=over

=over

=item * $tab_index (integer) 

=item * $alignment (Gtk2::Pango::TabAlign) 

=item * $location (integer) 

=back

=back

=head2 list = $tab_array-E<gt>B<get_tabs> 

=over

Returns a list of Gtk2::Pango::TabAlign's, alignments, and integers, locations. 
Even elemtents are alignments and odd elements are locations, so 0 is the first
alignment and 1 is the first location, 2 the second alignment, 3 the second 
location, etc.

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Boxed>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

