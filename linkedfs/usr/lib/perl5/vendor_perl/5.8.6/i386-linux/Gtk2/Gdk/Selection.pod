=head1 NAME

Gtk2::Gdk::Selection


=head1 METHODS

=head2 Gtk2::Gdk::Selection-E<gt>B<convert> ($requestor, $selection, $target, $time_)

=over

=over

=item * $requestor (Gtk2::Gdk::Window) 

=item * $selection (Gtk2::Gdk::Atom) 

=item * $target (Gtk2::Gdk::Atom) 

=item * $time_ (unsigned) 

=back

=back

=head2 window or undef = Gtk2::Gdk::Selection-E<gt>B<owner_get> ($selection)

=over

=over

=item * $selection (Gtk2::Gdk::Atom) 

=back

=back

=head2 window or undef = Gtk2::Gdk::Selection-E<gt>B<owner_get_for_display> ($display, $selection)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $selection (Gtk2::Gdk::Atom) 

=back

=back

=head2 boolean = Gtk2::Gdk::Selection-E<gt>B<owner_set> ($owner, $selection, $time_, $send_event)

=over

=over

=item * $owner (Gtk2::Gdk::Window or undef) 

=item * $selection (Gtk2::Gdk::Atom) 

=item * $time_ (unsigned) 

=item * $send_event (boolean) 

=back

=back

=head2 boolean = Gtk2::Gdk::Selection-E<gt>B<owner_set_for_display> ($display, $owner, $selection, $time_, $send_event)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $owner (Gtk2::Gdk::Window) 

=item * $selection (Gtk2::Gdk::Atom) 

=item * $time_ (unsigned) 

=item * $send_event (boolean) 

=back

=back

=head2 (data, prop_type, prop_format) = Gtk2::Gdk::Selection->B<property_get> ($requestor)

=over

=over

=item * $requestor (Gtk2::Gdk::Window) 

=back

Use Gtk2::Clipboard instead.

=back

=head2 Gtk2::Gdk::Selection-E<gt>B<send_notify> ($requestor, $selection, $target, $property, $time_)

=over

=over

=item * $requestor (unsigned) 

=item * $selection (Gtk2::Gdk::Atom) 

=item * $target (Gtk2::Gdk::Atom) 

=item * $property (Gtk2::Gdk::Atom) 

=item * $time_ (unsigned) 

=back

=back

=head2 Gtk2::Gdk::Selection-E<gt>B<send_notify_for_display> ($display, $requestor, $selection, $target, $property, $time_)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $requestor (unsigned) 

=item * $selection (Gtk2::Gdk::Atom) 

=item * $target (Gtk2::Gdk::Atom) 

=item * $property (Gtk2::Gdk::Atom) 

=item * $time_ (unsigned) 

=back

=back


=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

