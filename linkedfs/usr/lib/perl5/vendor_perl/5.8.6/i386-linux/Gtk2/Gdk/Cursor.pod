=head1 NAME

Gtk2::Gdk::Cursor

=head1 HIERARCHY

  Glib::Boxed
  +----Gtk2::Gdk::Cursor


=head1 METHODS

=head2 cursor = Gtk2::Gdk::Cursor-E<gt>B<new> ($cursor_type)

=over

=over

=item * $cursor_type (Gtk2::Gdk::CursorType) 

=back

=back

=head2 cursor = Gtk2::Gdk::Cursor-E<gt>B<new_for_display> ($display, $cursor_type)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $cursor_type (Gtk2::Gdk::CursorType) 

=back

=back

=head2 cursor = Gtk2::Gdk::Cursor-E<gt>B<new_from_pixbuf> ($display, $pixbuf, $x, $y)

=over

=over

=item * $display (Gtk2::Gdk::Display) 

=item * $pixbuf (Gtk2::Gdk::Pixbuf) 

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 cursor = Gtk2::Gdk::Cursor-E<gt>B<new_from_pixmap> ($source, $mask, $fg, $bg, $x, $y)

=over

=over

=item * $source (Gtk2::Gdk::Pixmap) 

=item * $mask (Gtk2::Gdk::Pixmap) 

=item * $fg (Gtk2::Gdk::Color) 

=item * $bg (Gtk2::Gdk::Color) 

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 display = $cursor-E<gt>B<get_display> 

=over

=back

=head2 cursortype = $cursor-E<gt>B<type> 

=over

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Boxed>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

