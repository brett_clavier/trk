=head1 NAME

Gtk2::ProgressBar

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::ProgressBar-E<gt>B<new> 

=over

=back

=head2 ellipsizemode = $pbar-E<gt>B<get_ellipsize> 

=over

=back

=head2 $pbar-E<gt>B<set_ellipsize> ($mode)

=over

=over

=item * $mode (Gtk2::Pango::EllipsizeMode) 

=back

=back

=head2 double = $pbar-E<gt>B<get_fraction> 

=over

=back

=head2 $pbar-E<gt>B<set_fraction> ($fraction)

=over

=over

=item * $fraction (double) 

=back

=back

=head2 progressbarorientation = $pbar-E<gt>B<get_orientation> 

=over

=back

=head2 $pbar-E<gt>B<set_orientation> ($orientation)

=over

=over

=item * $orientation (Gtk2::ProgressBarOrientation) 

=back

=back

=head2 $pbar-E<gt>B<pulse> 

=over

=back

=head2 double = $pbar-E<gt>B<get_pulse_step> 

=over

=back

=head2 $pbar-E<gt>B<set_pulse_step> ($fraction)

=over

=over

=item * $fraction (double) 

=back

=back

=head2 string or undef = $pbar-E<gt>B<get_text> 

=over

=back

=head2 $pbar-E<gt>B<set_text> ($text)

=over

=over

=item * $text (string or undef) 

=back

=back


=head1 PROPERTIES

=over

=item 'activity-blocks' (Glib::UInt : readable / writable)

The number of blocks which can fit in the progress bar area in activity mode (Deprecated)

=item 'activity-step' (Glib::UInt : readable / writable)

The increment used for each iteration in activity mode (Deprecated)

=item 'adjustment' (Gtk2::Adjustment : readable / writable)

The GtkAdjustment connected to the progress bar (Deprecated)

=item 'bar-style' (Gtk2::ProgressBarStyle : readable / writable)

Specifies the visual style of the bar in percentage mode (Deprecated)

=item 'discrete-blocks' (Glib::UInt : readable / writable)

The number of discrete blocks in a progress bar (when shown in the discrete style)

=item 'ellipsize' (Gtk2::Pango::EllipsizeMode : readable / writable)

The preferred place to ellipsize the string, if the progressbar does not have enough room to display the entire string, if at all

=item 'fraction' (double : readable / writable)

The fraction of total work that has been completed

=item 'orientation' (Gtk2::ProgressBarOrientation : readable / writable)

Orientation and growth direction of the progress bar

=item 'pulse-step' (double : readable / writable)

The fraction of total progress to move the bouncing block when pulsed

=item 'text' (string : readable / writable)

Text to be displayed in the progress bar

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

