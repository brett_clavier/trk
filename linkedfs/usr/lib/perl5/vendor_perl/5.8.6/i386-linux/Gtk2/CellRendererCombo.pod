=head1 NAME

Gtk2::CellRendererCombo

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::CellRenderer
              +----Gtk2::CellRendererText
                    +----Gtk2::CellRendererCombo


=head1 METHODS

=head2 cellrenderer = Gtk2::CellRendererCombo-E<gt>B<new> 

=over

=back


=head1 PROPERTIES

=over

=item 'has-entry' (boolean : readable / writable)

If FALSE, don't allow to enter strings other than the chosen ones

=item 'model' (Gtk2::TreeModel : readable / writable)

The model containing the possible values for the combo box

=item 'text-column' (integer : readable / writable)

A column in the data source model to get the strings from

=back


=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::CellRenderer>, L<Gtk2::CellRendererText>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

