=head1 NAME

Gtk2::IconView

=head1 HIERARCHY

  Glib::Object
  +----Gtk2::Object
        +----Gtk2::Widget
              +----Gtk2::Container
                    +----Gtk2::IconView

=head1 INTERFACES

  Gtk2::Atk::ImplementorIface


=head1 METHODS

=head2 widget = Gtk2::IconView-E<gt>B<new> 

=over

=back

=head2 widget = Gtk2::IconView-E<gt>B<new_with_model> ($model)

=over

=over

=item * $model (Gtk2::TreeModel) 

=back

=back

=head2 $icon_view-E<gt>B<item_activated> ($path)

=over

=over

=item * $path (Gtk2::TreePath) 

=back

=back

=head2 integer = $icon_view-E<gt>B<get_markup_column> 

=over

=back

=head2 $icon_view-E<gt>B<set_markup_column> ($column)

=over

=over

=item * $column (integer) 

=back

=back

=head2 treemodel = $icon_view-E<gt>B<get_model> 

=over

=back

=head2 $icon_view-E<gt>B<set_model> ($model)

=over

=over

=item * $model (Gtk2::TreeModel) 

=back

=back

=head2 orientation = $icon_view-E<gt>B<get_orientation> 

=over

=back

=head2 $icon_view-E<gt>B<set_orientation> ($orientation)

=over

=over

=item * $orientation (Gtk2::Orientation) 

=back

=back

=head2 treepath = $icon_view-E<gt>B<get_path_at_pos> ($x, $y)

=over

=over

=item * $x (integer) 

=item * $y (integer) 

=back

=back

=head2 boolean = $icon_view-E<gt>B<path_is_selected> ($path)

=over

=over

=item * $path (Gtk2::TreePath) 

=back

=back

=head2 integer = $icon_view-E<gt>B<get_pixbuf_column> 

=over

=back

=head2 $icon_view-E<gt>B<set_pixbuf_column> ($column)

=over

=over

=item * $column (integer) 

=back

=back

=head2 $icon_view-E<gt>B<select_all> 

=over

=back

=head2 $icon_view-E<gt>B<select_path> ($path)

=over

=over

=item * $path (Gtk2::TreePath) 

=back

=back

=head2 $icon_view-E<gt>B<selected_foreach> ($func, $data=undef)

=over

=over

=item * $func (scalar) 

=item * $data (scalar) 

=back

=back

=head2 list = $icon_view-E<gt>B<get_selected_items> 

=over

=back

=head2 selectionmode = $icon_view-E<gt>B<get_selection_mode> 

=over

=back

=head2 $icon_view-E<gt>B<set_selection_mode> ($mode)

=over

=over

=item * $mode (Gtk2::SelectionMode) 

=back

=back

=head2 integer = $icon_view-E<gt>B<get_text_column> 

=over

=back

=head2 $icon_view-E<gt>B<set_text_column> ($column)

=over

=over

=item * $column (integer) 

=back

=back

=head2 $icon_view-E<gt>B<unselect_all> 

=over

=back

=head2 $icon_view-E<gt>B<unselect_path> ($path)

=over

=over

=item * $path (Gtk2::TreePath) 

=back

=back


=head1 PROPERTIES

=over

=item 'column-spacing' (integer : readable / writable)

Space which is inserted between grid column

=item 'columns' (integer : readable / writable)

Number of columns to display

=item 'item-width' (integer : readable / writable)

The width used for each item

=item 'margin' (integer : readable / writable)

Space which is inserted at the edges of the icon view

=item 'markup-column' (integer : readable / writable)

Model column used to retrieve the text if using Pango markup

=item 'model' (Gtk2::TreeModel : readable / writable)

The model for the icon view

=item 'orientation' (Gtk2::Orientation : readable / writable)

How the text and icon of each item are positioned relative to each other

=item 'pixbuf-column' (integer : readable / writable)

Model column used to retrieve the icon pixbuf from

=item 'row-spacing' (integer : readable / writable)

Space which is inserted between grid rows

=item 'selection-mode' (Gtk2::SelectionMode : readable / writable)

The selection mode

=item 'spacing' (integer : readable / writable)

Space which is inserted between cells of an item

=item 'text-column' (integer : readable / writable)

Model column used to retrieve the text from

=back


=head1 SIGNALS

=over

=item B<set-scroll-adjustments> (Gtk2::IconView, Gtk2::Adjustment, Gtk2::Adjustment)

=item boolean = B<move-cursor> (Gtk2::IconView, Gtk2::MovementStep, integer)

=item B<select-all> (Gtk2::IconView)

=item B<unselect-all> (Gtk2::IconView)

=item B<selection-changed> (Gtk2::IconView)

=item B<item-activated> (Gtk2::IconView, Gtk2::TreePath)

=item B<select-cursor-item> (Gtk2::IconView)

=item B<toggle-cursor-item> (Gtk2::IconView)

=item boolean = B<activate-cursor-item> (Gtk2::IconView)

=back


=head1 ENUMS AND FLAGS

=head2 flags Gtk2::Gdk::EventMask

=over

=item * 'exposure-mask' / 'GDK_EXPOSURE_MASK'

=item * 'pointer-motion-mask' / 'GDK_POINTER_MOTION_MASK'

=item * 'pointer-motion-hint-mask' / 'GDK_POINTER_MOTION_HINT_MASK'

=item * 'button-motion-mask' / 'GDK_BUTTON_MOTION_MASK'

=item * 'button1-motion-mask' / 'GDK_BUTTON1_MOTION_MASK'

=item * 'button2-motion-mask' / 'GDK_BUTTON2_MOTION_MASK'

=item * 'button3-motion-mask' / 'GDK_BUTTON3_MOTION_MASK'

=item * 'button-press-mask' / 'GDK_BUTTON_PRESS_MASK'

=item * 'button-release-mask' / 'GDK_BUTTON_RELEASE_MASK'

=item * 'key-press-mask' / 'GDK_KEY_PRESS_MASK'

=item * 'key-release-mask' / 'GDK_KEY_RELEASE_MASK'

=item * 'enter-notify-mask' / 'GDK_ENTER_NOTIFY_MASK'

=item * 'leave-notify-mask' / 'GDK_LEAVE_NOTIFY_MASK'

=item * 'focus-change-mask' / 'GDK_FOCUS_CHANGE_MASK'

=item * 'structure-mask' / 'GDK_STRUCTURE_MASK'

=item * 'property-change-mask' / 'GDK_PROPERTY_CHANGE_MASK'

=item * 'visibility-notify-mask' / 'GDK_VISIBILITY_NOTIFY_MASK'

=item * 'proximity-in-mask' / 'GDK_PROXIMITY_IN_MASK'

=item * 'proximity-out-mask' / 'GDK_PROXIMITY_OUT_MASK'

=item * 'substructure-mask' / 'GDK_SUBSTRUCTURE_MASK'

=item * 'scroll-mask' / 'GDK_SCROLL_MASK'

=item * 'all-events-mask' / 'GDK_ALL_EVENTS_MASK'

=back



=head1 SEE ALSO

L<Gtk2>, L<Glib::Object>, L<Gtk2::Object>, L<Gtk2::Widget>, L<Gtk2::Container>

=head1 COPYRIGHT

Copyright (C) 2003-2005 by the gtk2-perl team.

This software is licensed under the LGPL.  See L<Gtk2> for a full notice.


=cut

