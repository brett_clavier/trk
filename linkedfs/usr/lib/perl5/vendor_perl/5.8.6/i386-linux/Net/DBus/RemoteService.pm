package Net::DBus::RemoteService;

use 5.006;
use strict;
use warnings;
use Carp;

our $VERSION = '0.0.1';

use Net::DBus::RemoteObject;


sub new {
    my $class = shift;
    my $self = {};

    $self->{bus} = shift;
    $self->{service_name} = shift;

    bless $self, $class;

    return $self;
}

sub get_bus {
    my $self = shift;

    return $self->{bus};
}


sub get_service_name {
    my $self = shift;
    return $self->{service_name};
}

sub get_object {
    my $self = shift;
    my $object_path = shift;
    my $interface = shift;
    
    return Net::DBus::RemoteObject->new($self,
					$object_path,
					$interface);
}

1;
 
