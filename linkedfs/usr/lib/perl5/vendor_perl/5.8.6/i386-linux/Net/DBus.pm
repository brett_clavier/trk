package Net::DBus;

use 5.006;
use strict;
use warnings;
use Carp;

use Net::DBus::Binding::Bus;
use Net::DBus::Binding::Message;
use Net::DBus::Binding::Value;
use Net::DBus::RemoteService;

our $VERSION = '0.0.1';

use Exporter;

use base qw(Exporter);

use vars qw(@EXPORT);

@EXPORT = qw(dboolean dbyte dstring dint32
             duint32 dint64 duint64 ddouble
             dpack);

require XSLoader;
XSLoader::load('Net::DBus', $VERSION);

sub system {
    my $class = shift;
    return $class->_new(&Net::DBus::Binding::Bus::SYSTEM);
}

sub session {
    my $class = shift;
    return $class->_new(&Net::DBus::Binding::Bus::SESSION);
}


sub connection {
    my $class = shift;

    my $self = {};
    
    $self->{connection} = Net::DBus::Binding::Bus->new(address => shift);
    $self->{signals} = {};
    
    bless $self, $class;

    $self->{connection}->add_filter(sub { $self->_signal_func(@_) });
    
    return $self;
}

sub _new {
    my $class = shift;
    my $self = {};
    
    $self->{connection} = Net::DBus::Binding::Bus->new(type => shift);
    $self->{signals} = {};
    
    bless $self, $class;

    $self->{connection}->add_filter(sub { $self->_signal_func(@_) });
    
    return $self;
}

sub get_connection {
    my $self = shift;
    return $self->{connection};
}

sub get_service {
    my $self = shift;
    my $name = @_ ? shift : "org.freedesktop.Broadcast";
    
    return Net::DBus::RemoteService->new($self, $name);
}

sub add_signal_receiver {
    my $self = shift;
    my $receiver = shift;
    my $signal_name = shift;
    my $interface = shift;
    my $service = shift;
    my $path = shift;

    my $rule = $self->_match_rule($signal_name, $interface, $service, $path);
    
    $self->{receivers}->{$rule} = [] unless $self->{receivers}->{$rule};
    push @{$self->{receivers}->{$rule}}, $receiver;
    
    $self->{connection}->add_match($rule);
}

sub remove_signal_receiver {
    my $self = shift;
    my $receiver = shift;
    my $signal_name = shift;
    my $interface = shift;
    my $service = shift;
    my $path = shift;
    
    my $rule = $self->_match_rule($signal_name, $interface, $service, $path);

    my @receivers;
    foreach (@{$self->{receivers}->{$rule}}) {
	if ($_ eq $receiver) {
	    $self->{connection}->remove_match($rule);
	} else {
	    push @receivers, $_;
	}
    }
    $self->{receivers}->{$rule} = \@receivers;
}


sub _match_rule {
    my $self = shift;
    my $signal_name = shift;
    my $interface = shift;
    my $service = shift;
    my $path = shift;

    my $rule = "type='signal'";
    if ($interface) {
	$rule .= ",interface='$interface'";
    }
    if ($service) {
	if ($service !~ /^:/ &&
	    $service ne "org.freedesktop.DBus") {
	    my $bus_service = $self->get_service("org.freedesktop.DBus");
	    my $bus_object = $bus_service->get_object('/org/freedesktop/DBus',
						      'org.freedesktop.DBus');
	    $service = $bus_object->GetServiceOwner($service);
	}
	$rule .= ",sender='$service'";
    }
    if ($path) {
	$rule .= ",path='$path'";
    }
    if ($signal_name) {
	$rule .= ",member='$signal_name'";
    }
    return $rule;
}


sub _rule_matches {
    my $self = shift;
    my $rule = shift;
    my $member = shift;
    my $interface = shift;
    my $sender = shift;
    my $path = shift;
    
    my %bits;
    map { 
	if (/^(\w+)='(.*)'$/) {
	    $bits{$1} = $2;
	}
    } split /,/, $rule;
    
    if (exists $bits{member} &&
	$bits{member} ne $member) {
	return 0;
    }
    if (exists $bits{interface} &&
	$bits{interface} ne $interface) {
	return 0;
    }
    if (exists $bits{sender} &&
	$bits{sender} ne $sender) {
	return 0;
    }
    if (exists $bits{path} &&
	$bits{path} ne $path) {
	return 0;
    }
    return 1;
}

sub _signal_func {
    my $self = shift;
    my $connection = shift;
    my $message = shift;

    return 0 unless $message->isa("Net::DBus::Binding::Message::Signal");
    
    my $interface = $message->get_interface;
    my $sender = $message->get_sender;
    my $path = $message->get_path;
    my $member = $message->get_member;

    my $handled = 0;
    foreach my $rule (grep { $self->_rule_matches($_, $member, $interface, $sender, $path) }
		      keys %{$self->{receivers}}) {
	foreach my $callback (@{$self->{receivers}->{$rule}}) {
	    &$callback($interface, $member, $sender, $path, $message);
            $handled = 1;
	}
    }
    return $handled;
}


sub dboolean {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_BOOLEAN,
					  $value);
}

sub dbyte {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_BYTE,
					  $value);
}

sub dstring {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_STRING,
					  $value);
}

sub dint32 {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_INT32,
					  $value);
}

sub duint32 {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_UINT32,
					  $value);
}

sub dint64 {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_INT64,
					  $value);
}

sub duint64 {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_UINT64,
					  $value);
}

sub ddouble {
    my $value = shift;
    return Net::DBus::Binding::Value->new(&Net::DBus::Binding::Message::TYPE_DOUBLE,
					  $value);
}

our %flags = (
	      'o' => &Net::DBus::Binding::Message::TYPE_BOOLEAN,
	      'b' => &Net::DBus::Binding::Message::TYPE_BYTE,
	      's' => &Net::DBus::Binding::Message::TYPE_STRING,
	      'i' => &Net::DBus::Binding::Message::TYPE_INT32,
	      'I' => &Net::DBus::Binding::Message::TYPE_UINT32,
	      'l' => &Net::DBus::Binding::Message::TYPE_INT64,
	      'L' => &Net::DBus::Binding::Message::TYPE_UINT64,
	      'd' => &Net::DBus::Binding::Message::TYPE_DOUBLE,
	      );

sub dpack {
    my $format = shift;
    my @in = @_;
    if (length $format != ($#in+1)) {
	confess "incorrect number of arguments for format string";
    }
    
    my @out;
    foreach my $flag (split //, $format) {
	my $value = shift @in;
	if (!exists $flags{$flag}) {
	    confess "unknown format flag '$flag'";
	}
	push @out, Net::DBus::Binding::Value->new($flags{$flag},
						   $value);
    }
    return @out;
}


1;
__END__

=head1 NAME

DBus - Perl extension for the DBus message system

=head1 SYNOPSIS

  use Net::DBus::Connection;
  use Net::DBus::Server;

=head1 ABSTRACT

DBus provides a Perl API for the DBus message system.
    
=head1 DESCRIPTION

DBus provides a Perl API for the DBus message system.
There is no need to access this module directly. It is
used by other Net::DBus::* module to trigger the autoloading
of the XS module containing the interface to the DBus
API. The DBus Perl interface was written against version 
0.21 of dbus, and has had rudimentary testing against the 
development branch that will become 0.22. The two modules
of main interest will be L<Net::DBus::Connection> and 
L<Net::DBus::Server>.

=head1 SEE ALSO

L<Net::DBus::Connection>, L<Net::DBus::Server>, L<Net::DBus::Message>, L<Net::DBus::Reactor>,
L<Net::DBus::Bus>, L<Net::DBus::Watch>, L<Net::DBus::Iterator>,
L<dbus-monitor(1)>, L<dbus-daemon-1(1)>, L<dbus-send(1)>, L<http://dbus.freedesktop.org>,

=head1 AUTHOR

Daniel Berrange E<lt>dan@berrange.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2004 by Daniel Berrange

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself. 

=cut
