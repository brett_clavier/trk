=pod

=head1 NAME

Net::DBus::Binding::Iterator - Reading and writing message parameters

=head1 SYNOPSIS

Creating a new message

  my $msg = new Net::DBus::Binding::Message::Signal;
  my $iterator = $msg->iterator;

  $iterator->append_boolean(1);
  $iterator->append_byte(123);


Reading from a mesage

  my $msg = ...get it from somewhere...
  my $iter = $msg->iterator();

  my $i = 0;
  while ($iter->has_next()) {
    $iter->next();
    $i++;
    if ($i == 1) {
       my $val = $iter->get_boolean();
    } elsif ($i == 2) {
       my $val = $iter->get_byte();
    }
  }

=head1 DESCRIPTION

Provides an iterator for reading or writing message
fields. This module provides a Perl API to access the
dbus_message_iter_XXX methods in the C API. The array
and dictionary types are not yet supported, and there
are bugs in the Quad support (ie it always returns -1!).

=head1 METHODS

=over 4

=cut

package Net::DBus::Binding::Iterator;


use 5.006;
use strict;
use warnings;
use Carp qw(confess);

use Net::DBus;

our $VERSION = '0.0.1';

our $have_quads = 0;

BEGIN {
  eval "pack 'Q', 1243456";
  if ($@) {
    $have_quads = 0;
  } else {
    $have_quads = 1;
  }
}

=pod

=item $res = $iter->has_next()

Determines if there are any more fields in the message
itertor to be read. Returns a positive value if there
are more fields, zero otherwise.

=item $success = $iter->next()

Skips the iterator onto the next field in the message.
Returns a positive value if the current field pointer
was successfully advanced, zero otherwise.

=item my $val = $iter->get_boolean()

=item $iter->append_boolean($val);

Read or write a boolean value from/to the
message iterator

=item my $val = $iter->get_byte()

=item $iter->append_byte($val);

Read or write a single byte value from/to the
message iterator.

=item my $val = $iter->get_string()

=item $iter->append_string($val);

Read or write a UTF-8 string value from/to the
message iterator

=item my $val = $iter->get_int32()

=item $iter->append_int32($val);

Read or write a signed 32 bit value from/to the
message iterator

=item my $val = $iter->get_uint32()

=item $iter->append_uint32($val);

Read or write an unsigned 32 bit value from/to the
message iterator

=item my $val = $iter->get_int64()

=item $iter->append_int64($val);

Read or write a signed 64 bit value from/to the
message iterator

=item my $val = $iter->get_uint64()

=item $iter->append_uint64($val);

Read or write an unsigned 64 bit value from/to the
message iterator

=item my $val = $iter->get_double()

=item $iter->append_double($val);

Read or write a double precision floating point value 
from/to the message iterator

=cut

sub get_int64 {
    my $self = shift;
    confess "Quads not supported on this platform\n" unless $have_quads;
    return $self->_get_int64;
}

sub get_uint64 {
    my $self = shift;
    confess "Quads not supported on this platform\n" unless $have_quads;
    return $self->_get_uint64;
}

sub append_int64 {
    my $self = shift;
    confess "Quads not supported on this platform\n" unless $have_quads;
    $self->_append_int64(shift);
}

sub append_uint64 {
    my $self = shift;
    confess "Quads not supported on this platform\n" unless $have_quads;
    $self->_append_uint64(shift);
}


sub get {
    my $self = shift;
    
    my $type = $self->get_arg_type;
    
    if ($type == &Net::DBus::Binding::Message::TYPE_STRING) {
	return $self->get_string;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_BOOLEAN) {
	return $self->get_boolean;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_BYTE) {
	return $self->get_byte;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_INT32) {
	return $self->get_int32;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_UINT32) {
	return $self->get_uint32;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_INT64) {
	return $self->get_int64;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_UINT64) {
	return $self->get_uint64;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_DOUBLE) {
	return $self->get_double;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_ARRAY) {
	my $iter = $self->get_array_iter();
	my @value;
	do {
	    push @value, $iter->get();
	} while ($iter->next());
	return \@value;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_DICT) {
	my $iter = $self->get_dict_iter();
	my %value;
	do {
	    $value{$iter->get_dict_key} = $iter->get;
	} while ($iter->next());
	return \%value;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_NIL) {
	return undef;
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_CUSTOM) {
	confess "cannot handle Net::DBus::Binding::Message::TYPE_CUSTOM";
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_INVALID) {
	confess "cannot handle Net::DBus::Binding::Message::TYPE_INVALID";
    } elsif ($type == &Net::DBus::Binding::Message::TYPE_OBJECT_PATH) {
	confess "cannot handle Net::DBus::Binding::Message::TYPE_OBJECT_PATH";
    }
}

sub append {
    my $self = shift;
    my $value = shift;
    
    if (ref($value)) {
	if (ref($value) eq "Net::DBus::Binding::Value") {
	    my $raw = $value->value;

	    if ($value->type == &Net::DBus::Binding::Message::TYPE_BOOLEAN) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_boolean_array($raw);
		} else {
		    $self->append_boolean($raw);
		}
	    } elsif ($value->type == &Net::DBus::Binding::Message::TYPE_BYTE) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_byte_array($raw);
		} else {
		    $self->append_byte($raw);
		}
	    } elsif ($value->type == &Net::DBus::Binding::Message::TYPE_STRING) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_string_array($raw);
		} else {
		    $self->append_string($raw);
		}
	    } elsif ($value->type == &Net::DBus::Binding::Message::TYPE_INT32) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_int32_array($raw);
		} else {
		    $self->append_int32($raw);
		}
	    } elsif ($value->type == &Net::DBus::Binding::Message::TYPE_UINT32) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_uint32_array($raw);
		} else {
		    $self->append_uint32($raw);
		}
	    } elsif ($value->type == &Net::DBus::Binding::Message::TYPE_INT64) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_int64_array($raw);
		} else {
		    $self->append_int64($raw);
		}
	    } elsif ($value->type == &Net::DBus::Binding::Message::TYPE_UINT64) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_uint64_array($raw);
		} else {
		    $self->append_uint64($raw);
		}
	    } elsif ($value->type == &Net::DBus::Binding::Message::TYPE_DOUBLE) {
		if (ref($raw) && ref($raw) eq "ARRAY") {
		    $self->append_double_array($raw);
		} else {
		    $self->append_double($raw);
		}
	    }
	} elsif (ref($value) eq "HASH") {
	    my $iter = $self->append_dict();
	    foreach my $key (keys %{$value}) {
		$iter->append_dict_key($key);
		$iter->append($value->{$key});
	    }
	} elsif (ref($value) eq "ARRAY") {
	    $self->append_string_array($value);
	} else {
	    confess "Unsupported reference type ", ref($value);
	}
    } else {
	$self->append_string($value);
    }
}

1;

=pod

=back

=head1 SEE ALSO

L<Net::DBus::Binding::Message>

=head1 AUTHOR

Daniel Berrange E<lt>dan@berrange.comE<gt>

=head1 COPYRIGHT AND LICENSE

Copyright 2004 by Daniel Berrange

This library is free software; you can redistribute it and/or modify
it under the same terms as Perl itself. 

=cut
