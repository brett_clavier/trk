require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_CACHE_H)) {
    eval 'sub __LINUX_CACHE_H () {1;}' unless defined(&__LINUX_CACHE_H);
    require 'linux/kernel.ph';
    require 'linux/config.ph';
    require 'asm/cache.ph';
    unless(defined(&L1_CACHE_ALIGN)) {
	eval 'sub L1_CACHE_ALIGN {
	    local($x) = @_;
    	    eval q( &ALIGN($x,  &L1_CACHE_BYTES));
	}' unless defined(&L1_CACHE_ALIGN);
    }
    unless(defined(&SMP_CACHE_BYTES)) {
	eval 'sub SMP_CACHE_BYTES () { &L1_CACHE_BYTES;}' unless defined(&SMP_CACHE_BYTES);
    }
    unless(defined(&____cacheline_aligned)) {
	eval 'sub ____cacheline_aligned () { &__attribute__(( &__aligned__( &SMP_CACHE_BYTES)));}' unless defined(&____cacheline_aligned);
    }
    unless(defined(&____cacheline_aligned_in_smp)) {
	if(defined(&CONFIG_SMP)) {
	    eval 'sub ____cacheline_aligned_in_smp () { &____cacheline_aligned;}' unless defined(&____cacheline_aligned_in_smp);
	} else {
	    eval 'sub ____cacheline_aligned_in_smp () {1;}' unless defined(&____cacheline_aligned_in_smp);
	}
    }
    unless(defined(&__cacheline_aligned)) {
	eval 'sub __cacheline_aligned () { &__attribute__(( &__aligned__( &SMP_CACHE_BYTES),  &__section__(".data.cacheline_aligned")));}' unless defined(&__cacheline_aligned);
    }
    unless(defined(&__cacheline_aligned_in_smp)) {
	if(defined(&CONFIG_SMP)) {
	    eval 'sub __cacheline_aligned_in_smp () { &__cacheline_aligned;}' unless defined(&__cacheline_aligned_in_smp);
	} else {
	    eval 'sub __cacheline_aligned_in_smp () {1;}' unless defined(&__cacheline_aligned_in_smp);
	}
    }
    if(!defined( &____cacheline_maxaligned_in_smp)) {
	if(defined( &CONFIG_SMP)) {
	    eval 'sub ____cacheline_maxaligned_in_smp () { &__attribute__(( &__aligned__(1<< ( &L1_CACHE_SHIFT_MAX))));}' unless defined(&____cacheline_maxaligned_in_smp);
	} else {
	    eval 'sub ____cacheline_maxaligned_in_smp () {1;}' unless defined(&____cacheline_maxaligned_in_smp);
	}
    }
}
1;
