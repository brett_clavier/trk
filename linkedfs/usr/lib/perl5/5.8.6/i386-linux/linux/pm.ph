require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_PM_H)) {
    eval 'sub _LINUX_PM_H () {1;}' unless defined(&_LINUX_PM_H);
    if(defined(&__KERNEL__)) {
	require 'linux/config.ph';
	require 'linux/list.ph';
	require 'asm/atomic.ph';
	eval("sub PM_SUSPEND () { 0; }") unless defined(&PM_SUSPEND);
	eval("sub PM_RESUME () { 1; }") unless defined(&PM_RESUME);
	eval("sub PM_SAVE_STATE () { 2; }") unless defined(&PM_SAVE_STATE);
	eval("sub PM_SET_WAKEUP () { 3; }") unless defined(&PM_SET_WAKEUP);
	eval("sub PM_GET_RESOURCES () { 4; }") unless defined(&PM_GET_RESOURCES);
	eval("sub PM_SET_RESOURCES () { 5; }") unless defined(&PM_SET_RESOURCES);
	eval("sub PM_EJECT () { 6; }") unless defined(&PM_EJECT);
	eval("sub PM_LOCK () { 7; }") unless defined(&PM_LOCK);
	eval("sub PM_UNKNOWN_DEV () { 0; }") unless defined(&PM_UNKNOWN_DEV);
	eval("sub PM_SYS_DEV () { 1; }") unless defined(&PM_SYS_DEV);
	eval("sub PM_PCI_DEV () { 2; }") unless defined(&PM_PCI_DEV);
	eval("sub PM_USB_DEV () { 3; }") unless defined(&PM_USB_DEV);
	eval("sub PM_SCSI_DEV () { 4; }") unless defined(&PM_SCSI_DEV);
	eval("sub PM_ISA_DEV () { 5; }") unless defined(&PM_ISA_DEV);
	eval("sub PM_MTD_DEV () { 6; }") unless defined(&PM_MTD_DEV);
	eval("sub PM_SYS_UNKNOWN () { 0x00000000; }") unless defined(&PM_SYS_UNKNOWN);
	eval("sub PM_SYS_KBC () { 0x41d00303; }") unless defined(&PM_SYS_KBC);
	eval("sub PM_SYS_COM () { 0x41d00500; }") unless defined(&PM_SYS_COM);
	eval("sub PM_SYS_IRDA () { 0x41d00510; }") unless defined(&PM_SYS_IRDA);
	eval("sub PM_SYS_FDC () { 0x41d00700; }") unless defined(&PM_SYS_FDC);
	eval("sub PM_SYS_VGA () { 0x41d00900; }") unless defined(&PM_SYS_VGA);
	eval("sub PM_SYS_PCMCIA () { 0x41d00e00; }") unless defined(&PM_SYS_PCMCIA);
	eval 'sub PM_PCI_ID {
	    local($dev) = @_;
    	    eval q((($dev)-> ($bus->{number}) << 16| ($dev)-> &devfn));
	}' unless defined(&PM_PCI_ID);
	if(defined(&CONFIG_PM)) {
	    eval 'sub PM_IS_ACTIVE () {
	        eval q(( &pm_active != 0));
	    }' unless defined(&PM_IS_ACTIVE);
	} else {
	    eval 'sub PM_IS_ACTIVE () {
	        eval q(0);
	    }' unless defined(&PM_IS_ACTIVE);
	    eval 'sub pm_dev {
	        eval q(* &pm_register( &pm_dev_t  &type, my $id,  &pm_callback  &callback) {  &NULL; });
	    }' unless defined(&pm_dev);
	    eval 'sub pm_send {
	        local($dev,$rqst,$data) = @_;
    		eval q({ 0; });
	    }' unless defined(&pm_send);
	    eval 'sub pm_send_all {
	        local($rqst,$data) = @_;
    		eval q({ 0; });
	    }' unless defined(&pm_send_all);
	    eval 'sub pm_dev {
	        eval q(* &pm_find( &pm_dev_t  &type, \'struct pm_dev\' * &from) { 0; });
	    }' unless defined(&pm_dev);
	}
	eval 'sub PM_SUSPEND_ON () {(( &__force  &suspend_state_t) 0);}' unless defined(&PM_SUSPEND_ON);
	eval 'sub PM_SUSPEND_STANDBY () {(( &__force  &suspend_state_t) 1);}' unless defined(&PM_SUSPEND_STANDBY);
	eval 'sub PM_SUSPEND_MEM () {(( &__force  &suspend_state_t) 3);}' unless defined(&PM_SUSPEND_MEM);
	eval 'sub PM_SUSPEND_DISK () {(( &__force  &suspend_state_t) 4);}' unless defined(&PM_SUSPEND_DISK);
	eval 'sub PM_SUSPEND_MAX () {(( &__force  &suspend_state_t) 5);}' unless defined(&PM_SUSPEND_MAX);
	eval 'sub PM_DISK_FIRMWARE () {(( &__force  &suspend_disk_method_t) 1);}' unless defined(&PM_DISK_FIRMWARE);
	eval 'sub PM_DISK_PLATFORM () {(( &__force  &suspend_disk_method_t) 2);}' unless defined(&PM_DISK_PLATFORM);
	eval 'sub PM_DISK_SHUTDOWN () {(( &__force  &suspend_disk_method_t) 3);}' unless defined(&PM_DISK_SHUTDOWN);
	eval 'sub PM_DISK_REBOOT () {(( &__force  &suspend_disk_method_t) 4);}' unless defined(&PM_DISK_REBOOT);
	eval 'sub PM_DISK_MAX () {(( &__force  &suspend_disk_method_t) 5);}' unless defined(&PM_DISK_MAX);
	if(defined(&CONFIG_PM)) {
	}
    }
}
1;
