require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_THREADS_H)) {
    eval 'sub _LINUX_THREADS_H () {1;}' unless defined(&_LINUX_THREADS_H);
    eval 'sub NR_CPUS () {$sizeof{\'long\'};}' unless defined(&NR_CPUS);
    eval 'sub MIN_THREADS_LEFT_FOR_ROOT () {4;}' unless defined(&MIN_THREADS_LEFT_FOR_ROOT);
    eval 'sub PID_MAX_DEFAULT () {0x8000;}' unless defined(&PID_MAX_DEFAULT);
    eval 'sub PID_MAX_LIMIT () {($sizeof{\'long\'} > 4? 4*1024*1024:  &PID_MAX_DEFAULT);}' unless defined(&PID_MAX_LIMIT);
}
1;
