require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_SEM_H)) {
    eval 'sub _LINUX_SEM_H () {1;}' unless defined(&_LINUX_SEM_H);
    require 'linux/ipc.ph';
    require 'asm/atomic.ph';
    eval 'sub SEM_UNDO () {0x1000;}' unless defined(&SEM_UNDO);
    eval 'sub GETPID () {11;}' unless defined(&GETPID);
    eval 'sub GETVAL () {12;}' unless defined(&GETVAL);
    eval 'sub GETALL () {13;}' unless defined(&GETALL);
    eval 'sub GETNCNT () {14;}' unless defined(&GETNCNT);
    eval 'sub GETZCNT () {15;}' unless defined(&GETZCNT);
    eval 'sub SETVAL () {16;}' unless defined(&SETVAL);
    eval 'sub SETALL () {17;}' unless defined(&SETALL);
    eval 'sub SEM_STAT () {18;}' unless defined(&SEM_STAT);
    eval 'sub SEM_INFO () {19;}' unless defined(&SEM_INFO);
    require 'asm/sembuf.ph';
    eval 'sub SEMMNI () {128;}' unless defined(&SEMMNI);
    eval 'sub SEMMSL () {250;}' unless defined(&SEMMSL);
    eval 'sub SEMMNS () {( &SEMMNI* &SEMMSL);}' unless defined(&SEMMNS);
    eval 'sub SEMOPM () {32;}' unless defined(&SEMOPM);
    eval 'sub SEMVMX () {32767;}' unless defined(&SEMVMX);
    eval 'sub SEMAEM () { &SEMVMX;}' unless defined(&SEMAEM);
    eval 'sub SEMUME () { &SEMOPM;}' unless defined(&SEMUME);
    eval 'sub SEMMNU () { &SEMMNS;}' unless defined(&SEMMNU);
    eval 'sub SEMMAP () { &SEMMNS;}' unless defined(&SEMMAP);
    eval 'sub SEMUSZ () {20;}' unless defined(&SEMUSZ);
    if(defined(&__KERNEL__)) {
	if(defined(&CONFIG_SYSVIPC)) {
	} else {
	    eval 'sub copy_semundo {
	        local($clone_flags,$tsk) = @_;
    		eval q({ 0; });
	    }' unless defined(&copy_semundo);
	    eval 'sub exit_sem {
	        local($tsk) = @_;
    		eval q({ ; });
	    }' unless defined(&exit_sem);
	}
    }
}
1;
