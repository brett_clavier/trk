require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_STRINGIFY_H)) {
    eval 'sub __LINUX_STRINGIFY_H () {1;}' unless defined(&__LINUX_STRINGIFY_H);
    eval 'sub __stringify_1 {
        local($x) = @_;
	    eval q($x);
    }' unless defined(&__stringify_1);
    eval 'sub __stringify {
        local($x) = @_;
	    eval q( &__stringify_1($x));
    }' unless defined(&__stringify);
}
1;
