require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_MODULE_H)) {
    eval 'sub _LINUX_MODULE_H () {1;}' unless defined(&_LINUX_MODULE_H);
    require 'linux/sched.ph';
    require 'linux/spinlock.ph';
    require 'linux/list.ph';
    require 'linux/stat.ph';
    require 'linux/compiler.ph';
    require 'linux/cache.ph';
    require 'linux/kmod.ph';
    require 'linux/elf.ph';
    require 'linux/stringify.ph';
    require 'linux/kobject.ph';
    require 'linux/moduleparam.ph';
    require 'asm/local.ph';
    require 'asm/module.ph';
    eval 'sub MODULE_SUPPORTED_DEVICE {
        local($name) = @_;
	    eval q();
    }' unless defined(&MODULE_SUPPORTED_DEVICE);
    unless(defined(&MODULE_SYMBOL_PREFIX)) {
	eval 'sub MODULE_SYMBOL_PREFIX () {"";}' unless defined(&MODULE_SYMBOL_PREFIX);
    }
    eval 'sub MODULE_NAME_LEN () {(64- $sizeof{\'unsigned long\'});}' unless defined(&MODULE_NAME_LEN);
    if(defined(&MODULE)) {
	eval 'sub ___module_cat {
	    local($a,$b) = @_;
    	    eval q( &__mod_  $a  $b);
	}' unless defined(&___module_cat);
	eval 'sub __module_cat {
	    local($a,$b) = @_;
    	    eval q( &___module_cat($a,$b));
	}' unless defined(&__module_cat);
	eval 'sub __MODULE_INFO {
	    local($tag, $name, $info) = @_;
    	    eval q( &static  &const \'char\'  &__module_cat($name, &__LINE__)[]  &__attribute_used__  &__attribute__(( &section(\\".modinfo\\"), &unused)) =  &__stringify($tag) \\"=\\" $info);
	}' unless defined(&__MODULE_INFO);
	eval 'sub MODULE_GENERIC_TABLE {
	    local($gtype,$name) = @_;
    	    eval q( &extern  &const \'struct gtype\' &_id  &__mod_$gtype &_table  &__attribute__ (( &unused,  &alias( &__stringify($name)))));
	}' unless defined(&MODULE_GENERIC_TABLE);
	eval 'sub THIS_MODULE () {(& &__this_module);}' unless defined(&THIS_MODULE);
    } else {
	eval 'sub MODULE_GENERIC_TABLE {
	    local($gtype,$name) = @_;
    	    eval q();
	}' unless defined(&MODULE_GENERIC_TABLE);
	eval 'sub __MODULE_INFO {
	    local($tag, $name, $info) = @_;
    	    eval q();
	}' unless defined(&__MODULE_INFO);
	eval 'sub THIS_MODULE () {(0);}' unless defined(&THIS_MODULE);
    }
    eval 'sub MODULE_INFO {
        local($tag, $info) = @_;
	    eval q( &__MODULE_INFO($tag, $tag, $info));
    }' unless defined(&MODULE_INFO);
    eval 'sub MODULE_ALIAS {
        local($_alias) = @_;
	    eval q( &MODULE_INFO( &alias, $_alias));
    }' unless defined(&MODULE_ALIAS);
    eval 'sub MODULE_LICENSE {
        local($_license) = @_;
	    eval q( &MODULE_INFO( &license, $_license));
    }' unless defined(&MODULE_LICENSE);
    eval 'sub MODULE_AUTHOR {
        local($_author) = @_;
	    eval q( &MODULE_INFO( &author, $_author));
    }' unless defined(&MODULE_AUTHOR);
    eval 'sub MODULE_DESCRIPTION {
        local($_description) = @_;
	    eval q( &MODULE_INFO( &description, $_description));
    }' unless defined(&MODULE_DESCRIPTION);
    eval 'sub MODULE_PARM_DESC {
        local($_parm, $desc) = @_;
	    eval q( &__MODULE_INFO( &parm, $_parm, $_parm \\":\\" $desc));
    }' unless defined(&MODULE_PARM_DESC);
    eval 'sub MODULE_DEVICE_TABLE {
        local($type,$name) = @_;
	    eval q( &MODULE_GENERIC_TABLE($type &_device,$name));
    }' unless defined(&MODULE_DEVICE_TABLE);
    eval 'sub MODULE_VERSION {
        local($_version) = @_;
	    eval q( &MODULE_INFO( &version, $_version));
    }' unless defined(&MODULE_VERSION);
    if(defined(&CONFIG_MODULES)) {
	eval 'sub symbol_get {
	    local($x) = @_;
    	    eval q((( &typeof($x))( &__symbol_get( &MODULE_SYMBOL_PREFIX $x))));
	}' unless defined(&symbol_get);
	unless(defined(&__GENKSYMS__)) {
	    if(defined(&CONFIG_MODVERSIONS)) {
		eval 'sub __CRC_SYMBOL {
		    local($sym, $sec) = @_;
    		    eval q( &extern  &void * &__crc_$sym  &__attribute__(( &weak));  &static  &const \'unsigned long __kcrctab_\'$sym  &__attribute_used__  &__attribute__(( &section(\\"__kcrctab\\" $sec),  &unused)) =  & &__crc_$sym;);
		}' unless defined(&__CRC_SYMBOL);
	    } else {
		eval 'sub __CRC_SYMBOL {
		    local($sym, $sec) = @_;
    		    eval q();
		}' unless defined(&__CRC_SYMBOL);
	    }
	    eval 'sub __EXPORT_SYMBOL {
	        local($sym, $sec) = @_;
    		eval q( &__CRC_SYMBOL($sym, $sec)  &static  &const \'char\'  &__kstrtab_$sym->[]  &__attribute__(( &section(\\"__ksymtab_strings\\"))) =  &MODULE_SYMBOL_PREFIX $sym;  &static  &const \'struct kernel_symbol\'  &__ksymtab_$sym  &__attribute_used__  &__attribute__(( &section(\\"__ksymtab\\" $sec),  &unused)) = { $sym,  &__kstrtab_$sym });
	    }' unless defined(&__EXPORT_SYMBOL);
	    eval 'sub EXPORT_SYMBOL {
	        local($sym) = @_;
    		eval q( &__EXPORT_SYMBOL($sym, \\"\\"));
	    }' unless defined(&EXPORT_SYMBOL);
	    eval 'sub EXPORT_SYMBOL_GPL {
	        local($sym) = @_;
    		eval q( &__EXPORT_SYMBOL($sym, \\"_gpl\\"));
	    }' unless defined(&EXPORT_SYMBOL_GPL);
	}
	eval 'sub EXPORT_SYMBOL_NOVERS {
	    local($sym) = @_;
    	    eval q( &EXPORT_SYMBOL($sym));
	}' unless defined(&EXPORT_SYMBOL_NOVERS);
	eval("sub MODULE_STATE_LIVE () { 0; }") unless defined(&MODULE_STATE_LIVE);
	eval("sub MODULE_STATE_COMING () { 1; }") unless defined(&MODULE_STATE_COMING);
	eval("sub MODULE_STATE_GOING () { 2; }") unless defined(&MODULE_STATE_GOING);
	eval 'sub MODULE_SECT_NAME_LEN () {32;}' unless defined(&MODULE_SECT_NAME_LEN);
	if(defined(&CONFIG_MODULE_UNLOAD)) {
	}
	if(defined(&CONFIG_KALLSYMS)) {
	}
	eval 'sub module_put_and_exit {
	    local($code) = @_;
    	    eval q( &__module_put_and_exit( &THIS_MODULE, $code););
	}' unless defined(&module_put_and_exit);
	if(defined(&CONFIG_MODULE_UNLOAD)) {
	    eval 'sub symbol_put {
	        local($x) = @_;
    		eval q( &__symbol_put( &MODULE_SYMBOL_PREFIX $x));
	    }' unless defined(&symbol_put);
	    eval 'sub try_module_get {
	        local($module) = @_;
    		eval q({ \'int\'  &ret = 1;  &if ($module) { my $cpu =  &get_cpu();  &if ( &likely( &module_is_live($module)))  &local_inc( ($module->{ref[$cpu]}->{count}));  &else  &ret = 0;  &put_cpu(); }  &ret; });
	    }' unless defined(&try_module_get);
	    eval 'sub module_put {
	        local($module) = @_;
    		eval q({  &if ($module) { my $cpu =  &get_cpu();  &local_dec( ($module->{ref[$cpu]}->{count}));  &if ( &unlikely(! &module_is_live($module)))  &wake_up_process( ($module->{waiter}));  &put_cpu(); } });
	    }' unless defined(&module_put);
	} else {
	    eval 'sub try_module_get {
	        local($module) = @_;
    		eval q({ !$module ||  &module_is_live($module); });
	    }' unless defined(&try_module_get);
	    eval 'sub module_put {
	        local($module) = @_;
    		eval q({ });
	    }' unless defined(&module_put);
	    eval 'sub __module_get {
	        local($module) = @_;
    		eval q({ });
	    }' unless defined(&__module_get);
	    eval 'sub symbol_put {
	        local($x) = @_;
    		eval q( &do { }  &while(0));
	    }' unless defined(&symbol_put);
	    eval 'sub symbol_put_addr {
	        local($p) = @_;
    		eval q( &do { }  &while(0));
	    }' unless defined(&symbol_put_addr);
	}
	eval 'sub module_name {
	    local($mod) = @_;
    	    eval q(({ \'struct module\' * &__mod = ($mod);  &__mod ?  ($__mod->{name}) : \\"kernel\\"; }));
	}' unless defined(&module_name);
	eval 'sub __unsafe {
	    local($mod) = @_;
    	    eval q( &do {  &if ($mod  && !($mod)-> &unsafe) {  &printk( &KERN_WARNING \\"Module %s cannot be unloaded due to unsafe usage in\\" \\" %s:%u\\\\n\\", ($mod)-> &name,  &__FILE__,  &__LINE__); ($mod)-> &unsafe = 1; } }  &while(0));
	}' unless defined(&__unsafe);
    } else {
	eval 'sub EXPORT_SYMBOL {
	    local($sym) = @_;
    	    eval q();
	}' unless defined(&EXPORT_SYMBOL);
	eval 'sub EXPORT_SYMBOL_GPL {
	    local($sym) = @_;
    	    eval q();
	}' unless defined(&EXPORT_SYMBOL_GPL);
	eval 'sub EXPORT_SYMBOL_NOVERS {
	    local($sym) = @_;
    	    eval q();
	}' unless defined(&EXPORT_SYMBOL_NOVERS);
	eval 'sub symbol_get {
	    local($x) = @_;
    	    eval q(({  &extern  &typeof($x) $x  &__attribute__(( &weak)); ($x); }));
	}' unless defined(&symbol_get);
	eval 'sub symbol_put {
	    local($x) = @_;
    	    eval q( &do { }  &while(0));
	}' unless defined(&symbol_put);
	eval 'sub symbol_put_addr {
	    local($x) = @_;
    	    eval q( &do { }  &while(0));
	}' unless defined(&symbol_put_addr);
	eval 'sub __module_get {
	    local($module) = @_;
    	    eval q({ });
	}' unless defined(&__module_get);
	eval 'sub try_module_get {
	    local($module) = @_;
    	    eval q({ 1; });
	}' unless defined(&try_module_get);
	eval 'sub module_put {
	    local($module) = @_;
    	    eval q({ });
	}' unless defined(&module_put);
	eval 'sub module_name {
	    local($mod) = @_;
    	    eval q(\\"kernel\\");
	}' unless defined(&module_name);
	eval 'sub __unsafe {
	    local($mod) = @_;
    	    eval q();
	}' unless defined(&__unsafe);
	eval 'sub module {
	    eval q(* &module_get_kallsym(my $symnum, \'unsigned long\' * &value, \'char\' * &type, \'char\'  $namebuf[128]) {  &NULL; });
	}' unless defined(&module);
	eval 'sub module_kallsyms_lookup_name {
	    local($name) = @_;
    	    eval q({ 0; });
	}' unless defined(&module_kallsyms_lookup_name);
	eval 'sub is_exported {
	    local($name,$mod) = @_;
    	    eval q({ 0; });
	}' unless defined(&is_exported);
	eval 'sub register_module_notifier {
	    local($nb) = @_;
    	    eval q({ 0; });
	}' unless defined(&register_module_notifier);
	eval 'sub unregister_module_notifier {
	    local($nb) = @_;
    	    eval q({ 0; });
	}' unless defined(&unregister_module_notifier);
	eval 'sub module_put_and_exit {
	    local($code) = @_;
    	    eval q( &do_exit($code));
	}' unless defined(&module_put_and_exit);
	eval 'sub print_modules {
	    local($void) = @_;
    	    eval q({ });
	}' unless defined(&print_modules);
	eval 'sub module_add_driver {
	    local($module,$driver) = @_;
    	    eval q({ });
	}' unless defined(&module_add_driver);
	eval 'sub module_remove_driver {
	    local($driver) = @_;
    	    eval q({ });
	}' unless defined(&module_remove_driver);
    }
    eval 'sub symbol_request {
        local($x) = @_;
	    eval q( &try_then_request_module( &symbol_get($x), \\"symbol:\\" $x));
    }' unless defined(&symbol_request);
    if(defined(&MODULE)) {
	eval 'sub MODULE_PARM {
	    local($var,$type) = @_;
    	    eval q(\'struct obsolete_modparm\'  &__parm_$var  &__attribute__(( &section(\\"__obsparm\\"))) = {  &__stringify($var), $type,  &MODULE_PARM_ };);
	}' unless defined(&MODULE_PARM);
    } else {
	eval 'sub MODULE_PARM {
	    local($var,$type) = @_;
    	    eval q( &static  &void  &__attribute__(( &__unused__)) * &__parm_$var =  &MODULE_PARM_;);
	}' unless defined(&MODULE_PARM);
    }
    eval 'sub __MODULE_STRING {
        local($x) = @_;
	    eval q( &__stringify($x));
    }' unless defined(&__MODULE_STRING);
    eval 'sub HAVE_INTER_MODULE () {1;}' unless defined(&HAVE_INTER_MODULE);
}
1;
