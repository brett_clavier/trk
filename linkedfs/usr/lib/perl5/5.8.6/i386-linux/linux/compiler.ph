require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_COMPILER_H)) {
    eval 'sub __LINUX_COMPILER_H () {1;}' unless defined(&__LINUX_COMPILER_H);
    unless(defined(&__ASSEMBLY__)) {
	if(defined( &__KERNEL__)  && defined( &__CHECKER__)) {
	    eval 'sub __user () { &__attribute__(( &noderef,  &address_space(1)));}' unless defined(&__user);
	    eval 'sub __kernel () {1;}' unless defined(&__kernel);
	    eval 'sub __safe () { &__attribute__(( &safe));}' unless defined(&__safe);
	    eval 'sub __force () { &__attribute__(( &force));}' unless defined(&__force);
	    eval 'sub __iomem () { &__attribute__(( &noderef,  &address_space(2)));}' unless defined(&__iomem);
	    eval 'sub __acquires {
	        local($x) = @_;
    		eval q( &__attribute__(( &context(0,1))));
	    }' unless defined(&__acquires);
	    eval 'sub __releases {
	        local($x) = @_;
    		eval q( &__attribute__(( &context(1,0))));
	    }' unless defined(&__releases);
	    eval 'sub __acquire {
	        local($x) = @_;
    		eval q( &__context__(1));
	    }' unless defined(&__acquire);
	    eval 'sub __release {
	        local($x) = @_;
    		eval q( &__context__(-1));
	    }' unless defined(&__release);
	    eval 'sub __cond_lock {
	        local($x) = @_;
    		eval q((($x) ? ({  &__context__(1); 1; }) : 0));
	    }' unless defined(&__cond_lock);
	} else {
	    eval 'sub __user () {1;}' unless defined(&__user);
	    eval 'sub __kernel () {1;}' unless defined(&__kernel);
	    eval 'sub __safe () {1;}' unless defined(&__safe);
	    eval 'sub __force () {1;}' unless defined(&__force);
	    eval 'sub __iomem () {1;}' unless defined(&__iomem);
	    eval 'sub __chk_user_ptr {
	        local($x) = @_;
    		eval q(( &void)0);
	    }' unless defined(&__chk_user_ptr);
	    eval 'sub __chk_io_ptr {
	        local($x) = @_;
    		eval q(( &void)0);
	    }' unless defined(&__chk_io_ptr);
	    eval 'sub __builtin_warning () {( &x,  &y...) (1);}' unless defined(&__builtin_warning);
	    eval 'sub __acquires {
	        local($x) = @_;
    		eval q();
	    }' unless defined(&__acquires);
	    eval 'sub __releases {
	        local($x) = @_;
    		eval q();
	    }' unless defined(&__releases);
	    eval 'sub __acquire {
	        local($x) = @_;
    		eval q(( &void)0);
	    }' unless defined(&__acquire);
	    eval 'sub __release {
	        local($x) = @_;
    		eval q(( &void)0);
	    }' unless defined(&__release);
	    eval 'sub __cond_lock {
	        local($x) = @_;
    		eval q(($x));
	    }' unless defined(&__cond_lock);
	}
	if(defined(&__KERNEL__)) {
	    if((defined(&__GNUC__) ? &__GNUC__ : 0) > 3) {
		require 'linux/compiler-gcc+.ph';
	    }
 elsif((defined(&__GNUC__) ? &__GNUC__ : 0) == 3) {
		require 'linux/compiler-gcc3.ph';
	    }
 elsif((defined(&__GNUC__) ? &__GNUC__ : 0) == 2) {
		require 'linux/compiler-gcc2.ph';
	    } else {
		die("Sorry\,\ your\ compiler\ is\ too\ old\/not\ recognized\.");
	    }
	    if(defined(&__INTEL_COMPILER)) {
		require 'linux/compiler-intel.ph';
	    }
	    eval 'sub likely {
	        local($x) = @_;
    		eval q( &__builtin_expect(!!($x), 1));
	    }' unless defined(&likely);
	    eval 'sub unlikely {
	        local($x) = @_;
    		eval q( &__builtin_expect(!!($x), 0));
	    }' unless defined(&unlikely);
	    unless(defined(&barrier)) {
		eval 'sub barrier () {
		    eval q( &__memory_barrier());
		}' unless defined(&barrier);
	    }
	    unless(defined(&RELOC_HIDE)) {
		eval 'sub RELOC_HIDE {
		    local($ptr, $off) = @_;
    		    eval q(({ \'unsigned long __ptr\';  &__ptr =  ($ptr); ( &typeof($ptr)) ( &__ptr + ($off)); }));
		}' unless defined(&RELOC_HIDE);
	    }
	}
    }
    unless(defined(&__deprecated)) {
	eval 'sub __deprecated () {1;}' unless defined(&__deprecated);
    }
    unless(defined(&__must_check)) {
	eval 'sub __must_check () {1;}' unless defined(&__must_check);
    }
    unless(defined(&__attribute_used__)) {
	eval 'sub __attribute_used__ () {1;}' unless defined(&__attribute_used__);
    }
    unless(defined(&__attribute_pure__)) {
	eval 'sub __attribute_pure__ () {1;}' unless defined(&__attribute_pure__);
    }
    unless(defined(&__attribute_const__)) {
	eval 'sub __attribute_const__ () {1;}' unless defined(&__attribute_const__);
    }
    unless(defined(&noinline)) {
	eval 'sub noinline () {1;}' unless defined(&noinline);
    }
    unless(defined(&__always_inline)) {
	eval 'sub __always_inline () { &inline;}' unless defined(&__always_inline);
    }
}
1;
