require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_COMPLETION_H)) {
    eval 'sub __LINUX_COMPLETION_H () {1;}' unless defined(&__LINUX_COMPLETION_H);
    require 'linux/wait.ph';
    eval 'sub COMPLETION_INITIALIZER {
        local($work) = @_;
	    eval q({ 0,  &__WAIT_QUEUE_HEAD_INITIALIZER(($work). &wait) });
    }' unless defined(&COMPLETION_INITIALIZER);
    eval 'sub DECLARE_COMPLETION {
        local($work) = @_;
	    eval q(\'struct completion\' $work =  &COMPLETION_INITIALIZER($work));
    }' unless defined(&DECLARE_COMPLETION);
    eval 'sub init_completion {
        local($x) = @_;
	    eval q({  ($x->{done}) = 0;  &init_waitqueue_head( ($x->{wait})); });
    }' unless defined(&init_completion);
    eval 'sub INIT_COMPLETION {
        local($x) = @_;
	    eval q((($x). &done = 0));
    }' unless defined(&INIT_COMPLETION);
}
1;
