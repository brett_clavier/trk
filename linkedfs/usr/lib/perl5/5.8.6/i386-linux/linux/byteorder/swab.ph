require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_BYTEORDER_SWAB_H)) {
    eval 'sub _LINUX_BYTEORDER_SWAB_H () {1;}' unless defined(&_LINUX_BYTEORDER_SWAB_H);
    require 'linux/compiler.ph';
    eval 'sub ___swab16 {
        local($x) = @_;
	    eval q(( &__extension__({  &__u16  &__x = ($x); (( &__u16)( ((( &__u16)( &__x) & ( &__u16)0xff) << 8) | ((( &__u16)( &__x) & ( &__u16)0xff00) >> 8) )); })));
    }' unless defined(&___swab16);
    eval 'sub ___swab32 {
        local($x) = @_;
	    eval q(( &__extension__({  &__u32  &__x = ($x); (( &__u32)( ((( &__u32)( &__x) & ( &__u32)0xff) << 24) | ((( &__u32)( &__x) & ( &__u32)0xff00) << 8) | ((( &__u32)( &__x) & ( &__u32)0xff0000) >> 8) | ((( &__u32)( &__x) & ( &__u32)0xff000000) >> 24) )); })));
    }' unless defined(&___swab32);
    eval 'sub ___swab64 {
        local($x) = @_;
	    eval q(( &__extension__({  &__u64  &__x = ($x); (( &__u64)( ( &__u64)((( &__u64)( &__x) & ( &__u64)0xff) << 56) | ( &__u64)((( &__u64)( &__x) & ( &__u64)0xff00) << 40) | ( &__u64)((( &__u64)( &__x) & ( &__u64)0xff0000) << 24) | ( &__u64)((( &__u64)( &__x) & ( &__u64)0xff000000) << 8) | ( &__u64)((( &__u64)( &__x) & ( &__u64)1095216660480) >> 8) | ( &__u64)((( &__u64)( &__x) & ( &__u64)280375465082880) >> 24) | ( &__u64)((( &__u64)( &__x) & ( &__u64)7.17761190612173e+16) >> 40) | ( &__u64)((( &__u64)( &__x) & ( &__u64)1.83746864796716e+19) >> 56) )); })));
    }' unless defined(&___swab64);
    eval 'sub ___constant_swab16 {
        local($x) = @_;
	    eval q((( &__u16)( ((( &__u16)($x) & ( &__u16)0xff) << 8) | ((( &__u16)($x) & ( &__u16)0xff00) >> 8) )));
    }' unless defined(&___constant_swab16);
    eval 'sub ___constant_swab32 {
        local($x) = @_;
	    eval q((( &__u32)( ((( &__u32)($x) & ( &__u32)0xff) << 24) | ((( &__u32)($x) & ( &__u32)0xff00) << 8) | ((( &__u32)($x) & ( &__u32)0xff0000) >> 8) | ((( &__u32)($x) & ( &__u32)0xff000000) >> 24) )));
    }' unless defined(&___constant_swab32);
    eval 'sub ___constant_swab64 {
        local($x) = @_;
	    eval q((( &__u64)( ( &__u64)((( &__u64)($x) & ( &__u64)0xff) << 56) | ( &__u64)((( &__u64)($x) & ( &__u64)0xff00) << 40) | ( &__u64)((( &__u64)($x) & ( &__u64)0xff0000) << 24) | ( &__u64)((( &__u64)($x) & ( &__u64)0xff000000) << 8) | ( &__u64)((( &__u64)($x) & ( &__u64)1095216660480) >> 8) | ( &__u64)((( &__u64)($x) & ( &__u64)280375465082880) >> 24) | ( &__u64)((( &__u64)($x) & ( &__u64)7.17761190612173e+16) >> 40) | ( &__u64)((( &__u64)($x) & ( &__u64)1.83746864796716e+19) >> 56) )));
    }' unless defined(&___constant_swab64);
    unless(defined(&__arch__swab16)) {
	eval 'sub __arch__swab16 {
	    local($x) = @_;
    	    eval q(( &__extension__({  &__u16  &__tmp = ($x) ;  &___swab16( &__tmp); })));
	}' unless defined(&__arch__swab16);
    }
    unless(defined(&__arch__swab32)) {
	eval 'sub __arch__swab32 {
	    local($x) = @_;
    	    eval q(( &__extension__({  &__u32  &__tmp = ($x) ;  &___swab32( &__tmp); })));
	}' unless defined(&__arch__swab32);
    }
    unless(defined(&__arch__swab64)) {
	eval 'sub __arch__swab64 {
	    local($x) = @_;
    	    eval q(( &__extension__({  &__u64  &__tmp = ($x) ;  &___swab64( &__tmp); })));
	}' unless defined(&__arch__swab64);
    }
    unless(defined(&__arch__swab16p)) {
	eval 'sub __arch__swab16p {
	    local($x) = @_;
    	    eval q( &__arch__swab16(*($x)));
	}' unless defined(&__arch__swab16p);
    }
    unless(defined(&__arch__swab32p)) {
	eval 'sub __arch__swab32p {
	    local($x) = @_;
    	    eval q( &__arch__swab32(*($x)));
	}' unless defined(&__arch__swab32p);
    }
    unless(defined(&__arch__swab64p)) {
	eval 'sub __arch__swab64p {
	    local($x) = @_;
    	    eval q( &__arch__swab64(*($x)));
	}' unless defined(&__arch__swab64p);
    }
    unless(defined(&__arch__swab16s)) {
	eval 'sub __arch__swab16s {
	    local($x) = @_;
    	    eval q( &do { *($x) =  &__arch__swab16p(($x)); }  &while (0));
	}' unless defined(&__arch__swab16s);
    }
    unless(defined(&__arch__swab32s)) {
	eval 'sub __arch__swab32s {
	    local($x) = @_;
    	    eval q( &do { *($x) =  &__arch__swab32p(($x)); }  &while (0));
	}' unless defined(&__arch__swab32s);
    }
    unless(defined(&__arch__swab64s)) {
	eval 'sub __arch__swab64s {
	    local($x) = @_;
    	    eval q( &do { *($x) =  &__arch__swab64p(($x)); }  &while (0));
	}' unless defined(&__arch__swab64s);
    }
    if(defined( &__GNUC__)  && ((defined(&__GNUC__) ? &__GNUC__ : 0) >= 2)  && defined( &__OPTIMIZE__)) {
	eval 'sub __swab16 {
	    local($x) = @_;
    	    eval q(( &__builtin_constant_p(( &__u16)($x)) ?  &___swab16(($x)) :  &__fswab16(($x))));
	}' unless defined(&__swab16);
	eval 'sub __swab32 {
	    local($x) = @_;
    	    eval q(( &__builtin_constant_p(( &__u32)($x)) ?  &___swab32(($x)) :  &__fswab32(($x))));
	}' unless defined(&__swab32);
	eval 'sub __swab64 {
	    local($x) = @_;
    	    eval q(( &__builtin_constant_p(( &__u64)($x)) ?  &___swab64(($x)) :  &__fswab64(($x))));
	}' unless defined(&__swab64);
    } else {
	eval 'sub __swab16 {
	    local($x) = @_;
    	    eval q( &__fswab16($x));
	}' unless defined(&__swab16);
	eval 'sub __swab32 {
	    local($x) = @_;
    	    eval q( &__fswab32($x));
	}' unless defined(&__swab32);
	eval 'sub __swab64 {
	    local($x) = @_;
    	    eval q( &__fswab64($x));
	}' unless defined(&__swab64);
    }
    eval 'sub __fswab16 {
        local($x) = @_;
	    eval q({  &__arch__swab16($x); });
    }' unless defined(&__fswab16);
    eval 'sub __swab16p {
        local($x) = @_;
	    eval q({  &__arch__swab16p($x); });
    }' unless defined(&__swab16p);
    eval 'sub __swab16s {
        local($addr) = @_;
	    eval q({  &__arch__swab16s($addr); });
    }' unless defined(&__swab16s);
    eval 'sub __fswab32 {
        local($x) = @_;
	    eval q({  &__arch__swab32($x); });
    }' unless defined(&__fswab32);
    eval 'sub __swab32p {
        local($x) = @_;
	    eval q({  &__arch__swab32p($x); });
    }' unless defined(&__swab32p);
    eval 'sub __swab32s {
        local($addr) = @_;
	    eval q({  &__arch__swab32s($addr); });
    }' unless defined(&__swab32s);
    if(defined(&__BYTEORDER_HAS_U64__)) {
# some #ifdef were dropped here -- fill in the blanks
	eval 'sub __fswab64 {
	    local($x) = @_;
    	    eval q({ });
	}' unless defined(&__fswab64);
	eval 'sub __swab64p {
	    local($x) = @_;
    	    eval q({  &__arch__swab64p($x); });
	}' unless defined(&__swab64p);
	eval 'sub __swab64s {
	    local($addr) = @_;
    	    eval q({  &__arch__swab64s($addr); });
	}' unless defined(&__swab64s);
    }
    if(defined( &__KERNEL__)) {
	eval 'sub swab16 () { &__swab16;}' unless defined(&swab16);
	eval 'sub swab32 () { &__swab32;}' unless defined(&swab32);
	eval 'sub swab64 () { &__swab64;}' unless defined(&swab64);
	eval 'sub swab16p () { &__swab16p;}' unless defined(&swab16p);
	eval 'sub swab32p () { &__swab32p;}' unless defined(&swab32p);
	eval 'sub swab64p () { &__swab64p;}' unless defined(&swab64p);
	eval 'sub swab16s () { &__swab16s;}' unless defined(&swab16s);
	eval 'sub swab32s () { &__swab32s;}' unless defined(&swab32s);
	eval 'sub swab64s () { &__swab64s;}' unless defined(&swab64s);
    }
}
1;
