require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_WAIT_H)) {
    eval 'sub _LINUX_WAIT_H () {1;}' unless defined(&_LINUX_WAIT_H);
    eval 'sub WNOHANG () {0x1;}' unless defined(&WNOHANG);
    eval 'sub WUNTRACED () {0x2;}' unless defined(&WUNTRACED);
    eval 'sub WSTOPPED () { &WUNTRACED;}' unless defined(&WSTOPPED);
    eval 'sub WEXITED () {0x4;}' unless defined(&WEXITED);
    eval 'sub WCONTINUED () {0x8;}' unless defined(&WCONTINUED);
    eval 'sub WNOWAIT () {0x1000000;}' unless defined(&WNOWAIT);
    eval 'sub __WNOTHREAD () {0x20000000;}' unless defined(&__WNOTHREAD);
    eval 'sub __WALL () {0x40000000;}' unless defined(&__WALL);
    eval 'sub __WCLONE () {0x80000000;}' unless defined(&__WCLONE);
    eval 'sub P_ALL () {0;}' unless defined(&P_ALL);
    eval 'sub P_PID () {1;}' unless defined(&P_PID);
    eval 'sub P_PGID () {2;}' unless defined(&P_PGID);
    if(defined(&__KERNEL__)) {
	require 'linux/config.ph';
	require 'linux/list.ph';
	require 'linux/stddef.ph';
	require 'linux/spinlock.ph';
	require 'asm/system.ph';
	require 'asm/current.ph';
	eval 'sub WQ_FLAG_EXCLUSIVE () {0x1;}' unless defined(&WQ_FLAG_EXCLUSIVE);
	eval 'sub __WAITQUEUE_INITIALIZER {
	    local($name, $tsk) = @_;
    	    eval q({ . &task = $tsk, . &func =  &default_wake_function, . &task_list = {  &NULL,  &NULL } });
	}' unless defined(&__WAITQUEUE_INITIALIZER);
	eval 'sub DECLARE_WAITQUEUE {
	    local($name, $tsk) = @_;
    	    eval q( &wait_queue_t $name =  &__WAITQUEUE_INITIALIZER($name, $tsk));
	}' unless defined(&DECLARE_WAITQUEUE);
	eval 'sub __WAIT_QUEUE_HEAD_INITIALIZER {
	    local($name) = @_;
    	    eval q({ . &lock =  &SPIN_LOCK_UNLOCKED, . &task_list = { ($name). &task_list, ($name). &task_list } });
	}' unless defined(&__WAIT_QUEUE_HEAD_INITIALIZER);
	eval 'sub DECLARE_WAIT_QUEUE_HEAD {
	    local($name) = @_;
    	    eval q( &wait_queue_head_t $name =  &__WAIT_QUEUE_HEAD_INITIALIZER($name));
	}' unless defined(&DECLARE_WAIT_QUEUE_HEAD);
	eval 'sub __WAIT_BIT_KEY_INITIALIZER {
	    local($word, $bit) = @_;
    	    eval q({ . &flags = $word, . &bit_nr = $bit, });
	}' unless defined(&__WAIT_BIT_KEY_INITIALIZER);
	eval 'sub init_waitqueue_head {
	    local($q) = @_;
    	    eval q({  ($q->{lock}) =  &SPIN_LOCK_UNLOCKED;  &INIT_LIST_HEAD( ($q->{task_list})); });
	}' unless defined(&init_waitqueue_head);
	eval 'sub init_waitqueue_entry {
	    local($q,$p) = @_;
    	    eval q({  ($q->{flags}) = 0;  ($q->{task}) = $p;  ($q->{func}) =  &default_wake_function; });
	}' unless defined(&init_waitqueue_entry);
	eval 'sub init_waitqueue_func_entry {
	    local($q,$func) = @_;
    	    eval q({  ($q->{flags}) = 0;  ($q->{task}) =  &NULL;  ($q->{func}) = $func; });
	}' unless defined(&init_waitqueue_func_entry);
	eval 'sub waitqueue_active {
	    local($q) = @_;
    	    eval q({ ! &list_empty( ($q->{task_list})); });
	}' unless defined(&waitqueue_active);
	eval 'sub is_sync_wait {
	    local($wait) = @_;
    	    eval q((!($wait) || (($wait)-> &task)));
	}' unless defined(&is_sync_wait);
	eval 'sub __add_wait_queue {
	    local($head,$new) = @_;
    	    eval q({  &list_add( ($new->{task_list}),  ($head->{task_list})); });
	}' unless defined(&__add_wait_queue);
	eval 'sub __remove_wait_queue {
	    local($head,$old) = @_;
    	    eval q({  &list_del( ($old->{task_list})); });
	}' unless defined(&__remove_wait_queue);
	eval 'sub wake_up {
	    local($x) = @_;
    	    eval q( &__wake_up($x,  &TASK_UNINTERRUPTIBLE |  &TASK_INTERRUPTIBLE, 1,  &NULL));
	}' unless defined(&wake_up);
	eval 'sub wake_up_nr {
	    local($x, $nr) = @_;
    	    eval q( &__wake_up($x,  &TASK_UNINTERRUPTIBLE |  &TASK_INTERRUPTIBLE, $nr,  &NULL));
	}' unless defined(&wake_up_nr);
	eval 'sub wake_up_all {
	    local($x) = @_;
    	    eval q( &__wake_up($x,  &TASK_UNINTERRUPTIBLE |  &TASK_INTERRUPTIBLE, 0,  &NULL));
	}' unless defined(&wake_up_all);
	eval 'sub wake_up_interruptible {
	    local($x) = @_;
    	    eval q( &__wake_up($x,  &TASK_INTERRUPTIBLE, 1,  &NULL));
	}' unless defined(&wake_up_interruptible);
	eval 'sub wake_up_interruptible_nr {
	    local($x, $nr) = @_;
    	    eval q( &__wake_up($x,  &TASK_INTERRUPTIBLE, $nr,  &NULL));
	}' unless defined(&wake_up_interruptible_nr);
	eval 'sub wake_up_interruptible_all {
	    local($x) = @_;
    	    eval q( &__wake_up($x,  &TASK_INTERRUPTIBLE, 0,  &NULL));
	}' unless defined(&wake_up_interruptible_all);
	eval 'sub wake_up_locked {
	    local($x) = @_;
    	    eval q( &__wake_up_locked(($x),  &TASK_UNINTERRUPTIBLE |  &TASK_INTERRUPTIBLE));
	}' unless defined(&wake_up_locked);
	eval 'sub wake_up_interruptible_sync {
	    local($x) = @_;
    	    eval q( &__wake_up_sync(($x), &TASK_INTERRUPTIBLE, 1));
	}' unless defined(&wake_up_interruptible_sync);
	eval 'sub __wait_event {
	    local($wq, $condition) = @_;
    	    eval q( &do {  &DEFINE_WAIT( &__wait);  &for (;;) {  &prepare_to_wait($wq, & &__wait,  &TASK_UNINTERRUPTIBLE);  &if ($condition)  &break;  &schedule(); }  &finish_wait($wq, & &__wait); }  &while (0));
	}' unless defined(&__wait_event);
	eval 'sub wait_event {
	    local($wq, $condition) = @_;
    	    eval q( &do {  &if ($condition)  &break;  &__wait_event($wq, $condition); }  &while (0));
	}' unless defined(&wait_event);
	eval 'sub __wait_event_timeout {
	    local($wq, $condition, $ret) = @_;
    	    eval q( &do {  &DEFINE_WAIT( &__wait);  &for (;;) {  &prepare_to_wait($wq, & &__wait,  &TASK_UNINTERRUPTIBLE);  &if ($condition)  &break; $ret =  &schedule_timeout($ret);  &if (!$ret)  &break; }  &finish_wait($wq, & &__wait); }  &while (0));
	}' unless defined(&__wait_event_timeout);
	eval 'sub wait_event_timeout {
	    local($wq, $condition, $timeout) = @_;
    	    eval q(({ \'long __ret\' = $timeout;  &if (!($condition))  &__wait_event_timeout($wq, $condition,  &__ret);  &__ret; }));
	}' unless defined(&wait_event_timeout);
	eval 'sub __wait_event_interruptible {
	    local($wq, $condition, $ret) = @_;
    	    eval q( &do {  &DEFINE_WAIT( &__wait);  &for (;;) {  &prepare_to_wait($wq, & &__wait,  &TASK_INTERRUPTIBLE);  &if ($condition)  &break;  &if (! &signal_pending( &current)) {  &schedule();  &continue; } $ret = - &ERESTARTSYS;  &break; }  &finish_wait($wq, & &__wait); }  &while (0));
	}' unless defined(&__wait_event_interruptible);
	eval 'sub wait_event_interruptible {
	    local($wq, $condition) = @_;
    	    eval q(({ \'int\'  &__ret = 0;  &if (!($condition))  &__wait_event_interruptible($wq, $condition,  &__ret);  &__ret; }));
	}' unless defined(&wait_event_interruptible);
	eval 'sub __wait_event_interruptible_timeout {
	    local($wq, $condition, $ret) = @_;
    	    eval q( &do {  &DEFINE_WAIT( &__wait);  &for (;;) {  &prepare_to_wait($wq, & &__wait,  &TASK_INTERRUPTIBLE);  &if ($condition)  &break;  &if (! &signal_pending( &current)) { $ret =  &schedule_timeout($ret);  &if (!$ret)  &break;  &continue; } $ret = - &ERESTARTSYS;  &break; }  &finish_wait($wq, & &__wait); }  &while (0));
	}' unless defined(&__wait_event_interruptible_timeout);
	eval 'sub wait_event_interruptible_timeout {
	    local($wq, $condition, $timeout) = @_;
    	    eval q(({ \'long __ret\' = $timeout;  &if (!($condition))  &__wait_event_interruptible_timeout($wq, $condition,  &__ret);  &__ret; }));
	}' unless defined(&wait_event_interruptible_timeout);
	eval 'sub __wait_event_interruptible_exclusive {
	    local($wq, $condition, $ret) = @_;
    	    eval q( &do {  &DEFINE_WAIT( &__wait);  &for (;;) {  &prepare_to_wait_exclusive($wq, & &__wait,  &TASK_INTERRUPTIBLE);  &if ($condition)  &break;  &if (! &signal_pending( &current)) {  &schedule();  &continue; } $ret = - &ERESTARTSYS;  &break; }  &finish_wait($wq, & &__wait); }  &while (0));
	}' unless defined(&__wait_event_interruptible_exclusive);
	eval 'sub wait_event_interruptible_exclusive {
	    local($wq, $condition) = @_;
    	    eval q(({ \'int\'  &__ret = 0;  &if (!($condition))  &__wait_event_interruptible_exclusive($wq, $condition,  &__ret);  &__ret; }));
	}' unless defined(&wait_event_interruptible_exclusive);
	eval 'sub DEFINE_WAIT {
	    local($name) = @_;
    	    eval q( &wait_queue_t $name = { . &task =  &current, . &func =  &autoremove_wake_function, . &task_list = { . &next = ($name). &task_list, . &prev = ($name). &task_list, }, });
	}' unless defined(&DEFINE_WAIT);
	eval 'sub DEFINE_WAIT_BIT {
	    local($name, $word, $bit) = @_;
    	    eval q(\'struct wait_bit_queue\' $name = { . &key =  &__WAIT_BIT_KEY_INITIALIZER($word, $bit), . &wait = { . &task =  &current, . &func =  &wake_bit_function, . &task_list =  &LIST_HEAD_INIT(($name). ($wait->{task_list})), }, });
	}' unless defined(&DEFINE_WAIT_BIT);
	eval 'sub init_wait {
	    local($wait) = @_;
    	    eval q( &do { ($wait)-> &task =  &current; ($wait)-> &func =  &autoremove_wake_function;  &INIT_LIST_HEAD(($wait)-> &task_list); }  &while (0));
	}' unless defined(&init_wait);
    }
}
1;
