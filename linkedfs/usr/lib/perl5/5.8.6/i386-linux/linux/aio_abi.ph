require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX__AIO_ABI_H)) {
    eval 'sub __LINUX__AIO_ABI_H () {1;}' unless defined(&__LINUX__AIO_ABI_H);
    require 'asm/byteorder.ph';
    eval("sub IOCB_CMD_PREAD () { 0; }") unless defined(&IOCB_CMD_PREAD);
    eval("sub IOCB_CMD_PWRITE () { 1; }") unless defined(&IOCB_CMD_PWRITE);
    eval("sub IOCB_CMD_FSYNC () { 2; }") unless defined(&IOCB_CMD_FSYNC);
    eval("sub IOCB_CMD_FDSYNC () { 3; }") unless defined(&IOCB_CMD_FDSYNC);
    eval("sub IOCB_CMD_NOOP () { 6; }") unless defined(&IOCB_CMD_NOOP);
    if(defined( &__LITTLE_ENDIAN)) {
	eval 'sub PADDED {
	    local($x,$y) = @_;
    	    eval q($x, $y);
	}' unless defined(&PADDED);
    }
 elsif(defined( &__BIG_ENDIAN)) {
	eval 'sub PADDED {
	    local($x,$y) = @_;
    	    eval q($y, $x);
	}' unless defined(&PADDED);
    } else {
	die("edit\ for\ your\ odd\ byteorder\.");
    }
    undef(&IFBIG) if defined(&IFBIG);
    undef(&IFLITTLE) if defined(&IFLITTLE);
}
1;
