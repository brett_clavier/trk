require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_GFP_H)) {
    eval 'sub __LINUX_GFP_H () {1;}' unless defined(&__LINUX_GFP_H);
    require 'linux/mmzone.ph';
    require 'linux/stddef.ph';
    require 'linux/linkage.ph';
    require 'linux/config.ph';
    eval 'sub __GFP_DMA () {0x1;}' unless defined(&__GFP_DMA);
    eval 'sub __GFP_HIGHMEM () {0x2;}' unless defined(&__GFP_HIGHMEM);
    eval 'sub __GFP_WAIT () {0x10;}' unless defined(&__GFP_WAIT);
    eval 'sub __GFP_HIGH () {0x20;}' unless defined(&__GFP_HIGH);
    eval 'sub __GFP_IO () {0x40;}' unless defined(&__GFP_IO);
    eval 'sub __GFP_FS () {0x80;}' unless defined(&__GFP_FS);
    eval 'sub __GFP_COLD () {0x100;}' unless defined(&__GFP_COLD);
    eval 'sub __GFP_NOWARN () {0x200;}' unless defined(&__GFP_NOWARN);
    eval 'sub __GFP_REPEAT () {0x400;}' unless defined(&__GFP_REPEAT);
    eval 'sub __GFP_NOFAIL () {0x800;}' unless defined(&__GFP_NOFAIL);
    eval 'sub __GFP_NORETRY () {0x1000;}' unless defined(&__GFP_NORETRY);
    eval 'sub __GFP_NO_GROW () {0x2000;}' unless defined(&__GFP_NO_GROW);
    eval 'sub __GFP_COMP () {0x4000;}' unless defined(&__GFP_COMP);
    eval 'sub __GFP_BITS_SHIFT () {16;}' unless defined(&__GFP_BITS_SHIFT);
    eval 'sub __GFP_BITS_MASK () {((1<<  &__GFP_BITS_SHIFT) - 1);}' unless defined(&__GFP_BITS_MASK);
    eval 'sub GFP_LEVEL_MASK () {( &__GFP_WAIT| &__GFP_HIGH| &__GFP_IO| &__GFP_FS|  &__GFP_COLD| &__GFP_NOWARN| &__GFP_REPEAT|  &__GFP_NOFAIL| &__GFP_NORETRY| &__GFP_NO_GROW| &__GFP_COMP);}' unless defined(&GFP_LEVEL_MASK);
    eval 'sub GFP_ATOMIC () {( &__GFP_HIGH);}' unless defined(&GFP_ATOMIC);
    eval 'sub GFP_NOIO () {( &__GFP_WAIT);}' unless defined(&GFP_NOIO);
    eval 'sub GFP_NOFS () {( &__GFP_WAIT |  &__GFP_IO);}' unless defined(&GFP_NOFS);
    eval 'sub GFP_KERNEL () {( &__GFP_WAIT |  &__GFP_IO |  &__GFP_FS);}' unless defined(&GFP_KERNEL);
    eval 'sub GFP_USER () {( &__GFP_WAIT |  &__GFP_IO |  &__GFP_FS);}' unless defined(&GFP_USER);
    eval 'sub GFP_HIGHUSER () {( &__GFP_WAIT |  &__GFP_IO |  &__GFP_FS |  &__GFP_HIGHMEM);}' unless defined(&GFP_HIGHUSER);
    eval 'sub GFP_DMA () { &__GFP_DMA;}' unless defined(&GFP_DMA);
    unless(defined(&HAVE_ARCH_FREE_PAGE)) {
    }
    eval 'sub page {
        eval q(* &alloc_pages_node(\'int\'  &nid, my $gfp_mask, my $order) {  &if ( &unlikely( $order >=  &MAX_ORDER))  &NULL;  &__alloc_pages( $gfp_mask,  $order,  &NODE_DATA( &nid)-> &node_zonelists + ( $gfp_mask &  &GFP_ZONEMASK)); });
    }' unless defined(&page);
    if(defined(&CONFIG_NUMA)) {
	eval 'sub alloc_pages {
	    local($gfp_mask,$order) = @_;
    	    eval q({  &if ( &unlikely($order >=  &MAX_ORDER))  &NULL;  &alloc_pages_current($gfp_mask, $order); });
	}' unless defined(&alloc_pages);
    } else {
	eval 'sub alloc_pages {
	    local($gfp_mask, $order) = @_;
    	    eval q( &alloc_pages_node( &numa_node_id(), $gfp_mask, $order));
	}' unless defined(&alloc_pages);
	eval 'sub alloc_page_vma {
	    local($gfp_mask, $vma, $addr) = @_;
    	    eval q( &alloc_pages($gfp_mask, 0));
	}' unless defined(&alloc_page_vma);
    }
    eval 'sub alloc_page {
        local($gfp_mask) = @_;
	    eval q( &alloc_pages($gfp_mask, 0));
    }' unless defined(&alloc_page);
    eval 'sub __get_free_page {
        local($gfp_mask) = @_;
	    eval q( &__get_free_pages(($gfp_mask),0));
    }' unless defined(&__get_free_page);
    eval 'sub __get_dma_pages {
        local($gfp_mask, $order) = @_;
	    eval q( &__get_free_pages(($gfp_mask) |  &GFP_DMA,($order)));
    }' unless defined(&__get_dma_pages);
    eval 'sub __free_page {
        local($page) = @_;
	    eval q( &__free_pages(($page), 0));
    }' unless defined(&__free_page);
    eval 'sub free_page {
        local($addr) = @_;
	    eval q( &free_pages(($addr),0));
    }' unless defined(&free_page);
}
1;
