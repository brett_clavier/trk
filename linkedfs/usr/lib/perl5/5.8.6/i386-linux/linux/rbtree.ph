require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_RBTREE_H)) {
    eval 'sub _LINUX_RBTREE_H () {1;}' unless defined(&_LINUX_RBTREE_H);
    require 'linux/kernel.ph';
    require 'linux/stddef.ph';
    eval 'sub RB_RED () {0;}' unless defined(&RB_RED);
    eval 'sub RB_BLACK () {1;}' unless defined(&RB_BLACK);
    eval 'sub RB_ROOT () { {  &NULL, };}' unless defined(&RB_ROOT);
    eval 'sub rb_entry {
        local($ptr, $type, $member) = @_;
	    eval q( &container_of($ptr, $type, $member));
    }' unless defined(&rb_entry);
    eval 'sub rb_link_node {
        local($node,$parent,$rb_link) = @_;
	    eval q({  ($node->{rb_parent}) = $parent;  ($node->{rb_color}) =  &RB_RED;  ($node->{rb_left}) =  ($node->{rb_right}) =  &NULL; *$rb_link = $node; });
    }' unless defined(&rb_link_node);
}
1;
