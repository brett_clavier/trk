require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_RWSEM_SPINLOCK_H)) {
    eval 'sub _LINUX_RWSEM_SPINLOCK_H () {1;}' unless defined(&_LINUX_RWSEM_SPINLOCK_H);
    unless(defined(&_LINUX_RWSEM_H)) {
	die("please don't include linux/rwsem-spinlock.h directly, use linux/rwsem.h instead");
    }
    require 'linux/spinlock.ph';
    require 'linux/list.ph';
    if(defined(&__KERNEL__)) {
	require 'linux/types.ph';
	if((defined(&RWSEM_DEBUG) ? &RWSEM_DEBUG : 0)) {
	}
	if((defined(&RWSEM_DEBUG) ? &RWSEM_DEBUG : 0)) {
	    eval 'sub __RWSEM_DEBUG_INIT () {, 0;}' unless defined(&__RWSEM_DEBUG_INIT);
	} else {
	    eval 'sub __RWSEM_DEBUG_INIT () {1;}' unless defined(&__RWSEM_DEBUG_INIT);
	}
	eval 'sub __RWSEM_INITIALIZER {
	    local($name) = @_;
    	    eval q({ 0,  &SPIN_LOCK_UNLOCKED,  &LIST_HEAD_INIT(($name). &wait_list)  &__RWSEM_DEBUG_INIT });
	}' unless defined(&__RWSEM_INITIALIZER);
	eval 'sub DECLARE_RWSEM {
	    local($name) = @_;
    	    eval q(\'struct rw_semaphore\' $name =  &__RWSEM_INITIALIZER($name));
	}' unless defined(&DECLARE_RWSEM);
    }
}
1;
