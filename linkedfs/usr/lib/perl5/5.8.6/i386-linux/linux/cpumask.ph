require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__LINUX_CPUMASK_H)) {
    eval 'sub __LINUX_CPUMASK_H () {1;}' unless defined(&__LINUX_CPUMASK_H);
    require 'linux/kernel.ph';
    require 'linux/threads.ph';
    require 'linux/bitmap.ph';
    require 'asm/bug.ph';
    eval 'sub cpu_set {
        local($cpu, $dst) = @_;
	    eval q( &__cpu_set(($cpu), ($dst)));
    }' unless defined(&cpu_set);
    eval 'sub __cpu_set {
        local($cpu,$dstp) = @_;
	    eval q({  &set_bit($cpu,  ($dstp->{bits})); });
    }' unless defined(&__cpu_set);
    eval 'sub cpu_clear {
        local($cpu, $dst) = @_;
	    eval q( &__cpu_clear(($cpu), ($dst)));
    }' unless defined(&cpu_clear);
    eval 'sub __cpu_clear {
        local($cpu,$dstp) = @_;
	    eval q({  &clear_bit($cpu,  ($dstp->{bits})); });
    }' unless defined(&__cpu_clear);
    eval 'sub cpus_setall {
        local($dst) = @_;
	    eval q( &__cpus_setall(($dst),  &NR_CPUS));
    }' unless defined(&cpus_setall);
    eval 'sub __cpus_setall {
        local($dstp,$nbits) = @_;
	    eval q({  &bitmap_fill( ($dstp->{bits}), $nbits); });
    }' unless defined(&__cpus_setall);
    eval 'sub cpus_clear {
        local($dst) = @_;
	    eval q( &__cpus_clear(($dst),  &NR_CPUS));
    }' unless defined(&cpus_clear);
    eval 'sub __cpus_clear {
        local($dstp,$nbits) = @_;
	    eval q({  &bitmap_zero( ($dstp->{bits}), $nbits); });
    }' unless defined(&__cpus_clear);
    eval 'sub cpu_isset {
        local($cpu, $cpumask) = @_;
	    eval q( &test_bit(($cpu), ($cpumask). &bits));
    }' unless defined(&cpu_isset);
    eval 'sub cpu_test_and_set {
        local($cpu, $cpumask) = @_;
	    eval q( &__cpu_test_and_set(($cpu), ($cpumask)));
    }' unless defined(&cpu_test_and_set);
    eval 'sub __cpu_test_and_set {
        local($cpu,$addr) = @_;
	    eval q({  &test_and_set_bit($cpu,  ($addr->{bits})); });
    }' unless defined(&__cpu_test_and_set);
    eval 'sub cpus_and {
        local($dst, $src1, $src2) = @_;
	    eval q( &__cpus_and(($dst), ($src1), ($src2),  &NR_CPUS));
    }' unless defined(&cpus_and);
    eval 'sub __cpus_and {
        local($dstp,$src1p,$src2p,$nbits) = @_;
	    eval q({  &bitmap_and( ($dstp->{bits}),  ($src1p->{bits}),  ($src2p->{bits}), $nbits); });
    }' unless defined(&__cpus_and);
    eval 'sub cpus_or {
        local($dst, $src1, $src2) = @_;
	    eval q( &__cpus_or(($dst), ($src1), ($src2),  &NR_CPUS));
    }' unless defined(&cpus_or);
    eval 'sub __cpus_or {
        local($dstp,$src1p,$src2p,$nbits) = @_;
	    eval q({  &bitmap_or( ($dstp->{bits}),  ($src1p->{bits}),  ($src2p->{bits}), $nbits); });
    }' unless defined(&__cpus_or);
    eval 'sub cpus_xor {
        local($dst, $src1, $src2) = @_;
	    eval q( &__cpus_xor(($dst), ($src1), ($src2),  &NR_CPUS));
    }' unless defined(&cpus_xor);
    eval 'sub __cpus_xor {
        local($dstp,$src1p,$src2p,$nbits) = @_;
	    eval q({  &bitmap_xor( ($dstp->{bits}),  ($src1p->{bits}),  ($src2p->{bits}), $nbits); });
    }' unless defined(&__cpus_xor);
    eval 'sub cpus_andnot {
        local($dst, $src1, $src2) = @_;
	    eval q( &__cpus_andnot(($dst), ($src1), ($src2),  &NR_CPUS));
    }' unless defined(&cpus_andnot);
    eval 'sub __cpus_andnot {
        local($dstp,$src1p,$src2p,$nbits) = @_;
	    eval q({  &bitmap_andnot( ($dstp->{bits}),  ($src1p->{bits}),  ($src2p->{bits}), $nbits); });
    }' unless defined(&__cpus_andnot);
    eval 'sub cpus_complement {
        local($dst, $src) = @_;
	    eval q( &__cpus_complement(($dst), ($src),  &NR_CPUS));
    }' unless defined(&cpus_complement);
    eval 'sub __cpus_complement {
        local($dstp,$srcp,$nbits) = @_;
	    eval q({  &bitmap_complement( ($dstp->{bits}),  ($srcp->{bits}), $nbits); });
    }' unless defined(&__cpus_complement);
    eval 'sub cpus_equal {
        local($src1, $src2) = @_;
	    eval q( &__cpus_equal(($src1), ($src2),  &NR_CPUS));
    }' unless defined(&cpus_equal);
    eval 'sub __cpus_equal {
        local($src1p,$src2p,$nbits) = @_;
	    eval q({  &bitmap_equal( ($src1p->{bits}),  ($src2p->{bits}), $nbits); });
    }' unless defined(&__cpus_equal);
    eval 'sub cpus_intersects {
        local($src1, $src2) = @_;
	    eval q( &__cpus_intersects(($src1), ($src2),  &NR_CPUS));
    }' unless defined(&cpus_intersects);
    eval 'sub __cpus_intersects {
        local($src1p,$src2p,$nbits) = @_;
	    eval q({  &bitmap_intersects( ($src1p->{bits}),  ($src2p->{bits}), $nbits); });
    }' unless defined(&__cpus_intersects);
    eval 'sub cpus_subset {
        local($src1, $src2) = @_;
	    eval q( &__cpus_subset(($src1), ($src2),  &NR_CPUS));
    }' unless defined(&cpus_subset);
    eval 'sub __cpus_subset {
        local($src1p,$src2p,$nbits) = @_;
	    eval q({  &bitmap_subset( ($src1p->{bits}),  ($src2p->{bits}), $nbits); });
    }' unless defined(&__cpus_subset);
    eval 'sub cpus_empty {
        local($src) = @_;
	    eval q( &__cpus_empty(($src),  &NR_CPUS));
    }' unless defined(&cpus_empty);
    eval 'sub __cpus_empty {
        local($srcp,$nbits) = @_;
	    eval q({  &bitmap_empty( ($srcp->{bits}), $nbits); });
    }' unless defined(&__cpus_empty);
    eval 'sub cpus_full {
        local($cpumask) = @_;
	    eval q( &__cpus_full(($cpumask),  &NR_CPUS));
    }' unless defined(&cpus_full);
    eval 'sub __cpus_full {
        local($srcp,$nbits) = @_;
	    eval q({  &bitmap_full( ($srcp->{bits}), $nbits); });
    }' unless defined(&__cpus_full);
    eval 'sub cpus_weight {
        local($cpumask) = @_;
	    eval q( &__cpus_weight(($cpumask),  &NR_CPUS));
    }' unless defined(&cpus_weight);
    eval 'sub __cpus_weight {
        local($srcp,$nbits) = @_;
	    eval q({  &bitmap_weight( ($srcp->{bits}), $nbits); });
    }' unless defined(&__cpus_weight);
    eval 'sub cpus_shift_right {
        local($dst, $src, $n) = @_;
	    eval q( &__cpus_shift_right(($dst), ($src), ($n),  &NR_CPUS));
    }' unless defined(&cpus_shift_right);
    eval 'sub __cpus_shift_right {
        local($dstp,$srcp,$n,$nbits) = @_;
	    eval q({  &bitmap_shift_right( ($dstp->{bits}),  ($srcp->{bits}), $n, $nbits); });
    }' unless defined(&__cpus_shift_right);
    eval 'sub cpus_shift_left {
        local($dst, $src, $n) = @_;
	    eval q( &__cpus_shift_left(($dst), ($src), ($n),  &NR_CPUS));
    }' unless defined(&cpus_shift_left);
    eval 'sub __cpus_shift_left {
        local($dstp,$srcp,$n,$nbits) = @_;
	    eval q({  &bitmap_shift_left( ($dstp->{bits}),  ($srcp->{bits}), $n, $nbits); });
    }' unless defined(&__cpus_shift_left);
    eval 'sub first_cpu {
        local($src) = @_;
	    eval q( &__first_cpu(($src),  &NR_CPUS));
    }' unless defined(&first_cpu);
    eval 'sub __first_cpu {
        local($srcp,$nbits) = @_;
	    eval q({  &min_t(\'int\', $nbits,  &find_first_bit( ($srcp->{bits}), $nbits)); });
    }' unless defined(&__first_cpu);
    eval 'sub next_cpu {
        local($n, $src) = @_;
	    eval q( &__next_cpu(($n), ($src),  &NR_CPUS));
    }' unless defined(&next_cpu);
    eval 'sub __next_cpu {
        local($n,$srcp,$nbits) = @_;
	    eval q({  &min_t(\'int\', $nbits,  &find_next_bit( ($srcp->{bits}), $nbits, $n+1)); });
    }' unless defined(&__next_cpu);
    eval 'sub cpumask_of_cpu {
        local($cpu) = @_;
	    eval q(({  &typeof( &_unused_cpumask_arg_)  &m;  &if ($sizeof{ &m} == $sizeof{\'unsigned long\'}) {  ($m->{bits[0]}) = 1<<($cpu); }  &else {  &cpus_clear( &m);  &cpu_set(($cpu),  &m); }  &m; }));
    }' unless defined(&cpumask_of_cpu);
    eval 'sub CPU_MASK_LAST_WORD () { &BITMAP_LAST_WORD_MASK( &NR_CPUS);}' unless defined(&CPU_MASK_LAST_WORD);
    if((defined(&NR_CPUS) ? &NR_CPUS : 0) <= (defined(&BITS_PER_LONG) ? &BITS_PER_LONG : 0)) {
	eval 'sub CPU_MASK_ALL () {(( &cpumask_t) { { [ &BITS_TO_LONGS( &NR_CPUS)-1] =  &CPU_MASK_LAST_WORD } });}' unless defined(&CPU_MASK_ALL);
    } else {
	eval 'sub CPU_MASK_ALL () {(( &cpumask_t) { { [0...  &BITS_TO_LONGS( &NR_CPUS)-2] = ~0, [ &BITS_TO_LONGS( &NR_CPUS)-1] =  &CPU_MASK_LAST_WORD } });}' unless defined(&CPU_MASK_ALL);
    }
    eval 'sub CPU_MASK_NONE () {(( &cpumask_t) { { [0...  &BITS_TO_LONGS( &NR_CPUS)-1] = 0 } });}' unless defined(&CPU_MASK_NONE);
    eval 'sub CPU_MASK_CPU0 () {(( &cpumask_t) { { [0] = 1 } });}' unless defined(&CPU_MASK_CPU0);
    eval 'sub cpus_addr {
        local($src) = @_;
	    eval q((($src). &bits));
    }' unless defined(&cpus_addr);
    eval 'sub cpumask_scnprintf {
        local($buf, $len, $src) = @_;
	    eval q( &__cpumask_scnprintf(($buf), ($len), ($src),  &NR_CPUS));
    }' unless defined(&cpumask_scnprintf);
    eval 'sub __cpumask_scnprintf {
        local($buf,$len,$srcp,$nbits) = @_;
	    eval q({  &bitmap_scnprintf($buf, $len,  ($srcp->{bits}), $nbits); });
    }' unless defined(&__cpumask_scnprintf);
    eval 'sub cpumask_parse {
        local($ubuf, $ulen, $src) = @_;
	    eval q( &__cpumask_parse(($ubuf), ($ulen), ($src),  &NR_CPUS));
    }' unless defined(&cpumask_parse);
    eval 'sub __cpumask_parse {
        local($buf,$len,$dstp,$nbits) = @_;
	    eval q({  &bitmap_parse($buf, $len,  ($dstp->{bits}), $nbits); });
    }' unless defined(&__cpumask_parse);
    if((defined(&NR_CPUS) ? &NR_CPUS : 0) > 1) {
	eval 'sub for_each_cpu_mask {
	    local($cpu, $mask) = @_;
    	    eval q( &for (($cpu) =  &first_cpu($mask); ($cpu) <  &NR_CPUS; ($cpu) =  &next_cpu(($cpu), ($mask))));
	}' unless defined(&for_each_cpu_mask);
    } else {
	eval 'sub for_each_cpu_mask {
	    local($cpu, $mask) = @_;
    	    eval q( &for (($cpu) = 0; ($cpu) < 1; ($cpu)++));
	}' unless defined(&for_each_cpu_mask);
    }
    if((defined(&NR_CPUS) ? &NR_CPUS : 0) > 1) {
	eval 'sub num_online_cpus () {
	    eval q( &cpus_weight( &cpu_online_map));
	}' unless defined(&num_online_cpus);
	eval 'sub num_possible_cpus () {
	    eval q( &cpus_weight( &cpu_possible_map));
	}' unless defined(&num_possible_cpus);
	eval 'sub num_present_cpus () {
	    eval q( &cpus_weight( &cpu_present_map));
	}' unless defined(&num_present_cpus);
	eval 'sub cpu_online {
	    local($cpu) = @_;
    	    eval q( &cpu_isset(($cpu),  &cpu_online_map));
	}' unless defined(&cpu_online);
	eval 'sub cpu_possible {
	    local($cpu) = @_;
    	    eval q( &cpu_isset(($cpu),  &cpu_possible_map));
	}' unless defined(&cpu_possible);
	eval 'sub cpu_present {
	    local($cpu) = @_;
    	    eval q( &cpu_isset(($cpu),  &cpu_present_map));
	}' unless defined(&cpu_present);
    } else {
	eval 'sub num_online_cpus () {
	    eval q(1);
	}' unless defined(&num_online_cpus);
	eval 'sub num_possible_cpus () {
	    eval q(1);
	}' unless defined(&num_possible_cpus);
	eval 'sub num_present_cpus () {
	    eval q(1);
	}' unless defined(&num_present_cpus);
	eval 'sub cpu_online {
	    local($cpu) = @_;
    	    eval q((($cpu) == 0));
	}' unless defined(&cpu_online);
	eval 'sub cpu_possible {
	    local($cpu) = @_;
    	    eval q((($cpu) == 0));
	}' unless defined(&cpu_possible);
	eval 'sub cpu_present {
	    local($cpu) = @_;
    	    eval q((($cpu) == 0));
	}' unless defined(&cpu_present);
    }
    eval 'sub any_online_cpu {
        local($mask) = @_;
	    eval q(({ \'int\'  &cpu;  &for_each_cpu_mask( &cpu, ($mask))  &if ( &cpu_online( &cpu))  &break;  &cpu; }));
    }' unless defined(&any_online_cpu);
    eval 'sub for_each_cpu {
        local($cpu) = @_;
	    eval q( &for_each_cpu_mask(($cpu),  &cpu_possible_map));
    }' unless defined(&for_each_cpu);
    eval 'sub for_each_online_cpu {
        local($cpu) = @_;
	    eval q( &for_each_cpu_mask(($cpu),  &cpu_online_map));
    }' unless defined(&for_each_online_cpu);
    eval 'sub for_each_present_cpu {
        local($cpu) = @_;
	    eval q( &for_each_cpu_mask(($cpu),  &cpu_present_map));
    }' unless defined(&for_each_present_cpu);
}
1;
