require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_NUMA_H)) {
    eval 'sub _LINUX_NUMA_H () {1;}' unless defined(&_LINUX_NUMA_H);
    require 'linux/config.ph';
    if(defined(&CONFIG_DISCONTIGMEM)) {
	require 'asm/numnodes.ph';
    }
    unless(defined(&NODES_SHIFT)) {
	eval 'sub NODES_SHIFT () {0;}' unless defined(&NODES_SHIFT);
    }
    eval 'sub MAX_NUMNODES () {(1<<  &NODES_SHIFT);}' unless defined(&MAX_NUMNODES);
}
1;
