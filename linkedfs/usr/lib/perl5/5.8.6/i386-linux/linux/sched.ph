require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_SCHED_H)) {
    eval 'sub _LINUX_SCHED_H () {1;}' unless defined(&_LINUX_SCHED_H);
    require 'asm/param.ph';
    require 'linux/capability.ph';
    require 'linux/threads.ph';
    require 'linux/kernel.ph';
    require 'linux/types.ph';
    require 'linux/timex.ph';
    require 'linux/jiffies.ph';
    require 'linux/rbtree.ph';
    require 'linux/thread_info.ph';
    require 'linux/cpumask.ph';
    require 'asm/system.ph';
    require 'asm/semaphore.ph';
    require 'asm/page.ph';
    require 'asm/ptrace.ph';
    require 'asm/mmu.ph';
    require 'linux/smp.ph';
    require 'linux/sem.ph';
    require 'linux/signal.ph';
    require 'linux/securebits.ph';
    require 'linux/fs_struct.ph';
    require 'linux/compiler.ph';
    require 'linux/completion.ph';
    require 'linux/pid.ph';
    require 'linux/percpu.ph';
    require 'linux/topology.ph';
    eval 'sub CSIGNAL () {0xff;}' unless defined(&CSIGNAL);
    eval 'sub CLONE_VM () {0x100;}' unless defined(&CLONE_VM);
    eval 'sub CLONE_FS () {0x200;}' unless defined(&CLONE_FS);
    eval 'sub CLONE_FILES () {0x400;}' unless defined(&CLONE_FILES);
    eval 'sub CLONE_SIGHAND () {0x800;}' unless defined(&CLONE_SIGHAND);
    eval 'sub CLONE_PTRACE () {0x2000;}' unless defined(&CLONE_PTRACE);
    eval 'sub CLONE_VFORK () {0x4000;}' unless defined(&CLONE_VFORK);
    eval 'sub CLONE_PARENT () {0x8000;}' unless defined(&CLONE_PARENT);
    eval 'sub CLONE_THREAD () {0x10000;}' unless defined(&CLONE_THREAD);
    eval 'sub CLONE_NEWNS () {0x20000;}' unless defined(&CLONE_NEWNS);
    eval 'sub CLONE_SYSVSEM () {0x40000;}' unless defined(&CLONE_SYSVSEM);
    eval 'sub CLONE_SETTLS () {0x80000;}' unless defined(&CLONE_SETTLS);
    eval 'sub CLONE_PARENT_SETTID () {0x100000;}' unless defined(&CLONE_PARENT_SETTID);
    eval 'sub CLONE_CHILD_CLEARTID () {0x200000;}' unless defined(&CLONE_CHILD_CLEARTID);
    eval 'sub CLONE_DETACHED () {0x400000;}' unless defined(&CLONE_DETACHED);
    eval 'sub CLONE_UNTRACED () {0x800000;}' unless defined(&CLONE_UNTRACED);
    eval 'sub CLONE_CHILD_SETTID () {0x1000000;}' unless defined(&CLONE_CHILD_SETTID);
    eval 'sub CLONE_STOPPED () {0x2000000;}' unless defined(&CLONE_STOPPED);
    eval 'sub CLONE_KERNEL () {( &CLONE_FS |  &CLONE_FILES |  &CLONE_SIGHAND);}' unless defined(&CLONE_KERNEL);
    eval 'sub FSHIFT () {11;}' unless defined(&FSHIFT);
    eval 'sub FIXED_1 () {(1<< &FSHIFT);}' unless defined(&FIXED_1);
    eval 'sub LOAD_FREQ () {(5* &HZ);}' unless defined(&LOAD_FREQ);
    eval 'sub EXP_1 () {1884;}' unless defined(&EXP_1);
    eval 'sub EXP_5 () {2014;}' unless defined(&EXP_5);
    eval 'sub EXP_15 () {2037;}' unless defined(&EXP_15);
    eval 'sub CALC_LOAD {
        local($load,$exp,$n) = @_;
	    eval q($load *= $exp; $load += $n*( &FIXED_1-$exp); $load >>=  &FSHIFT;);
    }' unless defined(&CALC_LOAD);
    eval 'sub CT_TO_SECS {
        local($x) = @_;
	    eval q((($x) /  &HZ));
    }' unless defined(&CT_TO_SECS);
    eval 'sub CT_TO_USECS {
        local($x) = @_;
	    eval q(((($x) %  &HZ) * 1000000/ &HZ));
    }' unless defined(&CT_TO_USECS);
    require 'linux/time.ph';
    require 'linux/param.ph';
    require 'linux/resource.ph';
    require 'linux/timer.ph';
    require 'asm/processor.ph';
    eval 'sub TASK_RUNNING () {0;}' unless defined(&TASK_RUNNING);
    eval 'sub TASK_INTERRUPTIBLE () {1;}' unless defined(&TASK_INTERRUPTIBLE);
    eval 'sub TASK_UNINTERRUPTIBLE () {2;}' unless defined(&TASK_UNINTERRUPTIBLE);
    eval 'sub TASK_STOPPED () {4;}' unless defined(&TASK_STOPPED);
    eval 'sub TASK_TRACED () {8;}' unless defined(&TASK_TRACED);
    eval 'sub EXIT_ZOMBIE () {16;}' unless defined(&EXIT_ZOMBIE);
    eval 'sub EXIT_DEAD () {32;}' unless defined(&EXIT_DEAD);
    eval 'sub __set_task_state {
        local($tsk, $state_value) = @_;
	    eval q( &do { ($tsk)-> &state = ($state_value); }  &while (0));
    }' unless defined(&__set_task_state);
    eval 'sub set_task_state {
        local($tsk, $state_value) = @_;
	    eval q( &set_mb(($tsk)-> &state, ($state_value)));
    }' unless defined(&set_task_state);
    eval 'sub __set_current_state {
        local($state_value) = @_;
	    eval q( &do {  ($current->{state}) = ($state_value); }  &while (0));
    }' unless defined(&__set_current_state);
    eval 'sub set_current_state {
        local($state_value) = @_;
	    eval q( &set_mb( ($current->{state}), ($state_value)));
    }' unless defined(&set_current_state);
    eval 'sub SCHED_NORMAL () {0;}' unless defined(&SCHED_NORMAL);
    eval 'sub SCHED_FIFO () {1;}' unless defined(&SCHED_FIFO);
    eval 'sub SCHED_RR () {2;}' unless defined(&SCHED_RR);
    if(defined(&__KERNEL__)) {
	require 'linux/spinlock.ph';
	eval 'sub __sched () { &__attribute__(( &__section__(".sched.text")));}' unless defined(&__sched);
	eval 'sub MAX_SCHEDULE_TIMEOUT () { &LONG_MAX;}' unless defined(&MAX_SCHEDULE_TIMEOUT);
	eval 'sub DEFAULT_MAX_MAP_COUNT () {65536;}' unless defined(&DEFAULT_MAX_MAP_COUNT);
	require 'linux/aio.ph';
	eval 'sub MAX_USER_RT_PRIO () {100;}' unless defined(&MAX_USER_RT_PRIO);
	eval 'sub MAX_RT_PRIO () { &MAX_USER_RT_PRIO;}' unless defined(&MAX_RT_PRIO);
	eval 'sub MAX_PRIO () {( &MAX_RT_PRIO + 40);}' unless defined(&MAX_PRIO);
	eval 'sub rt_task {
	    local($p) = @_;
    	    eval q(( &unlikely(($p)-> &prio <  &MAX_RT_PRIO)));
	}' unless defined(&rt_task);
	if(defined(&CONFIG_KEYS)) {
	}
	eval 'sub INIT_USER () {( &root_user);}' unless defined(&INIT_USER);
	if(defined(&CONFIG_SCHEDSTATS)) {
	}
	eval("sub SCHED_IDLE () { 0; }") unless defined(&SCHED_IDLE);
	eval("sub NOT_IDLE () { 1; }") unless defined(&NOT_IDLE);
	eval("sub NEWLY_IDLE () { 2; }") unless defined(&NEWLY_IDLE);
	eval("sub MAX_IDLE_TYPES () { 3; }") unless defined(&MAX_IDLE_TYPES);
	if(defined(&CONFIG_SMP)) {
	    eval 'sub SCHED_LOAD_SCALE () {128;}' unless defined(&SCHED_LOAD_SCALE);
	    eval 'sub SD_LOAD_BALANCE () {1;}' unless defined(&SD_LOAD_BALANCE);
	    eval 'sub SD_BALANCE_NEWIDLE () {2;}' unless defined(&SD_BALANCE_NEWIDLE);
	    eval 'sub SD_BALANCE_EXEC () {4;}' unless defined(&SD_BALANCE_EXEC);
	    eval 'sub SD_WAKE_IDLE () {8;}' unless defined(&SD_WAKE_IDLE);
	    eval 'sub SD_WAKE_AFFINE () {16;}' unless defined(&SD_WAKE_AFFINE);
	    eval 'sub SD_WAKE_BALANCE () {32;}' unless defined(&SD_WAKE_BALANCE);
	    eval 'sub SD_SHARE_CPUPOWER () {64;}' unless defined(&SD_SHARE_CPUPOWER);
	    if(defined(&CONFIG_SCHEDSTATS)) {
	    }
	    if(defined(&ARCH_HAS_SCHED_DOMAIN)) {
	    }
	}
	eval 'sub NGROUPS_SMALL () {32;}' unless defined(&NGROUPS_SMALL);
	eval 'sub NGROUPS_PER_BLOCK () {(( &PAGE_SIZE / $sizeof{ &gid_t}));}' unless defined(&NGROUPS_PER_BLOCK);
	eval 'sub get_group_info {
	    local($group_info) = @_;
    	    eval q( &do {  &atomic_inc(($group_info)-> &usage); }  &while (0));
	}' unless defined(&get_group_info);
	eval 'sub put_group_info {
	    local($group_info) = @_;
    	    eval q( &do {  &if ( &atomic_dec_and_test(($group_info)-> &usage))  &groups_free($group_info); }  &while (0));
	}' unless defined(&put_group_info);
	eval 'sub GROUP_AT {
	    local($gi, $i) = @_;
    	    eval q((($gi)-> $blocks[($i)/ &NGROUPS_PER_BLOCK][($i)% &NGROUPS_PER_BLOCK]));
	}' unless defined(&GROUP_AT);
	if(defined(&CONFIG_SCHEDSTATS)) {
	}
	if(defined(&CONFIG_KEYS)) {
	}
	if(defined(&CONFIG_NUMA)) {
	}
	eval 'sub process_group {
	    local($tsk) = @_;
    	    eval q({  ($tsk->{signal}->{pgrp}); });
	}' unless defined(&process_group);
	eval 'sub get_task_struct {
	    local($tsk) = @_;
    	    eval q( &do {  &atomic_inc(($tsk)-> &usage); }  &while(0));
	}' unless defined(&get_task_struct);
	eval 'sub put_task_struct {
	    local($tsk) = @_;
    	    eval q( &do {  &if ( &atomic_dec_and_test(($tsk)-> &usage))  &__put_task_struct($tsk); }  &while(0));
	}' unless defined(&put_task_struct);
	eval 'sub PF_ALIGNWARN () {0x1;}' unless defined(&PF_ALIGNWARN);
	eval 'sub PF_STARTING () {0x2;}' unless defined(&PF_STARTING);
	eval 'sub PF_EXITING () {0x4;}' unless defined(&PF_EXITING);
	eval 'sub PF_DEAD () {0x8;}' unless defined(&PF_DEAD);
	eval 'sub PF_FORKNOEXEC () {0x40;}' unless defined(&PF_FORKNOEXEC);
	eval 'sub PF_SUPERPRIV () {0x100;}' unless defined(&PF_SUPERPRIV);
	eval 'sub PF_DUMPCORE () {0x200;}' unless defined(&PF_DUMPCORE);
	eval 'sub PF_SIGNALED () {0x400;}' unless defined(&PF_SIGNALED);
	eval 'sub PF_MEMALLOC () {0x800;}' unless defined(&PF_MEMALLOC);
	eval 'sub PF_MEMDIE () {0x1000;}' unless defined(&PF_MEMDIE);
	eval 'sub PF_FLUSHER () {0x2000;}' unless defined(&PF_FLUSHER);
	eval 'sub PF_FREEZE () {0x4000;}' unless defined(&PF_FREEZE);
	eval 'sub PF_NOFREEZE () {0x8000;}' unless defined(&PF_NOFREEZE);
	eval 'sub PF_FROZEN () {0x10000;}' unless defined(&PF_FROZEN);
	eval 'sub PF_FSTRANS () {0x20000;}' unless defined(&PF_FSTRANS);
	eval 'sub PF_KSWAPD () {0x40000;}' unless defined(&PF_KSWAPD);
	eval 'sub PF_SWAPOFF () {0x80000;}' unless defined(&PF_SWAPOFF);
	eval 'sub PF_LESS_THROTTLE () {0x100000;}' unless defined(&PF_LESS_THROTTLE);
	eval 'sub PF_SYNCWRITE () {0x200000;}' unless defined(&PF_SYNCWRITE);
	eval 'sub PF_BORROWED_MM () {0x400000;}' unless defined(&PF_BORROWED_MM);
	if(defined(&CONFIG_SMP)) {
	} else {
	    eval 'sub set_cpus_allowed {
	        local($p,$new_mask) = @_;
    		eval q({ 0; });
	    }' unless defined(&set_cpus_allowed);
	}
	if(defined(&CONFIG_SMP)) {
	} else {
	    eval 'sub sched_exec () {
	        eval q({});
	    }' unless defined(&sched_exec);
	}
	unless(defined(&__HAVE_ARCH_KSTACK_END)) {
	    eval 'sub kstack_end {
	        local($addr) = @_;
    		eval q({ !(($addr+$sizeof{ &void}-1) & ( &THREAD_SIZE-$sizeof{ &void})); });
	    }' unless defined(&kstack_end);
	}
	eval 'sub find_task_by_pid {
	    local($nr) = @_;
    	    eval q( &find_task_by_pid_type( &PIDTYPE_PID, $nr));
	}' unless defined(&find_task_by_pid);
	eval 'sub user_struct {
	    eval q(* &get_uid(\'struct user_struct\' * &u) {  &atomic_inc( ($u->{__count}));  &u; });
	}' unless defined(&user_struct);
	require 'asm/current.ph';
	if(defined(&CONFIG_SMP)) {
	} else {
	}
	eval 'sub dequeue_signal_lock {
	    local($tsk,$mask,$info) = @_;
    	    eval q({ my $flags; \'int\'  &ret;  &spin_lock_irqsave( ($tsk->{sighand}->{siglock}),  $flags);  &ret =  &dequeue_signal($tsk, $mask, $info);  &spin_unlock_irqrestore( ($tsk->{sighand}->{siglock}),  $flags);  &ret; } );
	}' unless defined(&dequeue_signal_lock);
	eval 'sub SEND_SIG_NOINFO () {( 0);}' unless defined(&SEND_SIG_NOINFO);
	eval 'sub SEND_SIG_PRIV () {( 1);}' unless defined(&SEND_SIG_PRIV);
	eval 'sub SEND_SIG_FORCED () {( 2);}' unless defined(&SEND_SIG_FORCED);
	eval 'sub sas_ss_flags {
	    local($sp) = @_;
    	    eval q({ ( ($current->{sas_ss_size}) == 0?  &SS_DISABLE :  &on_sig_stack($sp) ?  &SS_ONSTACK : 0); });
	}' unless defined(&sas_ss_flags);
	if(defined(&CONFIG_SECURITY)) {
	} else {
	    eval 'sub capable {
	        local($cap) = @_;
    		eval q({  &if ( &cap_raised( ($current->{cap_effective}), $cap)) {  ($current->{flags}) |=  &PF_SUPERPRIV; 1; } 0; });
	    }' unless defined(&capable);
	}
	eval 'sub mmdrop {
	    local($mm) = @_;
    	    eval q({  &if ( &atomic_dec_and_test( ($mm->{mm_count})))  &__mmdrop($mm); });
	}' unless defined(&mmdrop);
	if(defined(&CONFIG_SMP)) {
	} else {
	    eval 'sub wait_task_inactive {
	        local($p) = @_;
    		eval q( &do { }  &while (0));
	    }' unless defined(&wait_task_inactive);
	}
	eval 'sub remove_parent {
	    local($p) = @_;
    	    eval q( &list_del_init(($p)-> &sibling));
	}' unless defined(&remove_parent);
	eval 'sub add_parent {
	    local($p, $parent) = @_;
    	    eval q( &list_add_tail(($p)-> &sibling,($parent)-> &children));
	}' unless defined(&add_parent);
	eval 'sub REMOVE_LINKS {
	    local($p) = @_;
    	    eval q( &do {  &if ( &thread_group_leader($p))  &list_del_init(($p)-> &tasks);  &remove_parent($p); }  &while (0));
	}' unless defined(&REMOVE_LINKS);
	eval 'sub SET_LINKS {
	    local($p) = @_;
    	    eval q( &do {  &if ( &thread_group_leader($p))  &list_add_tail(($p)-> &tasks, ($init_task->{tasks}));  &add_parent($p, ($p)-> &parent); }  &while (0));
	}' unless defined(&SET_LINKS);
	eval 'sub next_task {
	    local($p) = @_;
    	    eval q( &list_entry(($p)-> ($tasks->{next}), \'struct task_struct\',  &tasks));
	}' unless defined(&next_task);
	eval 'sub prev_task {
	    local($p) = @_;
    	    eval q( &list_entry(($p)-> ($tasks->{prev}), \'struct task_struct\',  &tasks));
	}' unless defined(&prev_task);
	eval 'sub for_each_process {
	    local($p) = @_;
    	    eval q( &for ($p =  &init_task ; ($p =  &next_task($p)) !=  &init_task ; ));
	}' unless defined(&for_each_process);
	eval 'sub do_each_thread {
	    local($g, $t) = @_;
    	    eval q( &for ($g = $t =  &init_task ; ($g = $t =  &next_task($g)) !=  &init_task ; )  &do);
	}' unless defined(&do_each_thread);
	eval 'sub while_each_thread {
	    local($g, $t) = @_;
    	    eval q( &while (($t =  &next_thread($t)) != $g));
	}' unless defined(&while_each_thread);
	eval 'sub thread_group_leader {
	    local($p) = @_;
    	    eval q(( ($p->{pid}) ==  ($p->{tgid})));
	}' unless defined(&thread_group_leader);
	eval 'sub thread_group_empty {
	    local($p) = @_;
    	    eval q({  &list_empty( ($p->{pids[&PIDTYPE_TGID]}->{pid_list})); });
	}' unless defined(&thread_group_empty);
	eval 'sub delay_group_leader {
	    local($p) = @_;
    	    eval q(( &thread_group_leader($p)  && ! &thread_group_empty($p)));
	}' unless defined(&delay_group_leader);
	eval 'sub task_unlock {
	    local($p) = @_;
    	    eval q({  &spin_unlock( ($p->{alloc_lock})); });
	}' unless defined(&task_unlock);
	eval 'sub clear_tsk_thread_flag {
	    local($tsk,$flag) = @_;
    	    eval q({  &clear_ti_thread_flag( ($tsk->{thread_info}),$flag); });
	}' unless defined(&clear_tsk_thread_flag);
	eval 'sub test_and_set_tsk_thread_flag {
	    local($tsk,$flag) = @_;
    	    eval q({  &test_and_set_ti_thread_flag( ($tsk->{thread_info}),$flag); });
	}' unless defined(&test_and_set_tsk_thread_flag);
	eval 'sub test_and_clear_tsk_thread_flag {
	    local($tsk,$flag) = @_;
    	    eval q({  &test_and_clear_ti_thread_flag( ($tsk->{thread_info}),$flag); });
	}' unless defined(&test_and_clear_tsk_thread_flag);
	eval 'sub test_tsk_thread_flag {
	    local($tsk,$flag) = @_;
    	    eval q({  &test_ti_thread_flag( ($tsk->{thread_info}),$flag); });
	}' unless defined(&test_tsk_thread_flag);
	eval 'sub set_tsk_need_resched {
	    local($tsk) = @_;
    	    eval q({  &set_tsk_thread_flag($tsk, &TIF_NEED_RESCHED); });
	}' unless defined(&set_tsk_need_resched);
	eval 'sub clear_tsk_need_resched {
	    local($tsk) = @_;
    	    eval q({  &clear_tsk_thread_flag($tsk, &TIF_NEED_RESCHED); });
	}' unless defined(&clear_tsk_need_resched);
	eval 'sub signal_pending {
	    local($p) = @_;
    	    eval q({  &unlikely( &test_tsk_thread_flag($p, &TIF_SIGPENDING)); });
	}' unless defined(&signal_pending);
	eval 'sub cond_resched {
	    local($void) = @_;
    	    eval q({  &if ( &need_resched())  &__cond_resched(); });
	}' unless defined(&cond_resched);
	if(defined(&CONFIG_SMP)) {
	    eval 'sub task_cpu {
	        local($p) = @_;
    		eval q({  ($p->{thread_info}->{c$pu}); });
	    }' unless defined(&task_cpu);
	    eval 'sub set_task_cpu {
	        local($p,$cpu) = @_;
    		eval q({  ($p->{thread_info}->{c$pu}) = $cpu; });
	    }' unless defined(&set_task_cpu);
	} else {
	    eval 'sub task_cpu {
	        local($p) = @_;
    		eval q({ 0; });
	    }' unless defined(&task_cpu);
	    eval 'sub set_task_cpu {
	        local($p,$cpu) = @_;
    		eval q({ });
	    }' unless defined(&set_task_cpu);
	}
	if(defined(&HAVE_ARCH_PICK_MMAP_LAYOUT)) {
	} else {
	    eval 'sub arch_pick_mmap_layout {
	        local($mm) = @_;
    		eval q({  ($mm->{mmap_base}) =  &TASK_UNMAPPED_BASE;  ($mm->{get_unmapped_area}) =  &arch_get_unmapped_area;  ($mm->{unmap_area}) =  &arch_unmap_area; });
	    }' unless defined(&arch_pick_mmap_layout);
	}
	if(defined(&CONFIG_MAGIC_SYSRQ)) {
	}
    }
}
1;
