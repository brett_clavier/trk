require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_ERRNO_H)) {
    eval 'sub _LINUX_ERRNO_H () {1;}' unless defined(&_LINUX_ERRNO_H);
    require 'asm/errno.ph';
    if(defined(&__KERNEL__)) {
	eval 'sub ERESTARTSYS () {512;}' unless defined(&ERESTARTSYS);
	eval 'sub ERESTARTNOINTR () {513;}' unless defined(&ERESTARTNOINTR);
	eval 'sub ERESTARTNOHAND () {514;}' unless defined(&ERESTARTNOHAND);
	eval 'sub ENOIOCTLCMD () {515;}' unless defined(&ENOIOCTLCMD);
	eval 'sub ERESTART_RESTARTBLOCK () {516;}' unless defined(&ERESTART_RESTARTBLOCK);
	eval 'sub EBADHANDLE () {521;}' unless defined(&EBADHANDLE);
	eval 'sub ENOTSYNC () {522;}' unless defined(&ENOTSYNC);
	eval 'sub EBADCOOKIE () {523;}' unless defined(&EBADCOOKIE);
	eval 'sub ENOTSUPP () {524;}' unless defined(&ENOTSUPP);
	eval 'sub ETOOSMALL () {525;}' unless defined(&ETOOSMALL);
	eval 'sub ESERVERFAULT () {526;}' unless defined(&ESERVERFAULT);
	eval 'sub EBADTYPE () {527;}' unless defined(&EBADTYPE);
	eval 'sub EJUKEBOX () {528;}' unless defined(&EJUKEBOX);
	eval 'sub EIOCBQUEUED () {529;}' unless defined(&EIOCBQUEUED);
	eval 'sub EIOCBRETRY () {530;}' unless defined(&EIOCBRETRY);
    }
}
1;
