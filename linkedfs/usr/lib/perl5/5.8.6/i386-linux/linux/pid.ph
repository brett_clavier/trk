require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_PID_H)) {
    eval 'sub _LINUX_PID_H () {1;}' unless defined(&_LINUX_PID_H);
    eval("sub PIDTYPE_PID () { 0; }") unless defined(&PIDTYPE_PID);
    eval("sub PIDTYPE_TGID () { 1; }") unless defined(&PIDTYPE_TGID);
    eval("sub PIDTYPE_PGID () { 2; }") unless defined(&PIDTYPE_PGID);
    eval("sub PIDTYPE_SID () { 3; }") unless defined(&PIDTYPE_SID);
    eval("sub PIDTYPE_MAX () { 4; }") unless defined(&PIDTYPE_MAX);
    eval 'sub pid_task {
        local($elem, $type) = @_;
	    eval q( &list_entry($elem, \'struct task_struct\',  ($pids[$type]->{pid_list})));
    }' unless defined(&pid_task);
    eval 'sub do_each_task_pid {
        local($who, $type, $task) = @_;
	    eval q( &if (($task =  &find_task_by_pid_type($type, $who))) {  &prefetch(($task)-> ($pids[$type]->{pid_list}->{next}));  &do {);
    }' unless defined(&do_each_task_pid);
    eval 'sub while_each_task_pid {
        local($who, $type, $task) = @_;
	    eval q(}  &while ($task =  &pid_task(($task)-> ($pids[$type]->{pid_list}->{next}), $type),  &prefetch(($task)-> ($pids[$type]->{pid_list}->{next})),  &hlist_unhashed(($task)-> ($pids[$type]->{pid_chain}))); });
    }' unless defined(&while_each_task_pid);
}
1;
