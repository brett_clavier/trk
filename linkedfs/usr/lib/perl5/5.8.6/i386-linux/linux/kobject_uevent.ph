require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_KOBJECT_EVENT_H_)) {
    eval 'sub _KOBJECT_EVENT_H_ () {1;}' unless defined(&_KOBJECT_EVENT_H_);
    eval 'sub HOTPLUG_PATH_LEN () {256;}' unless defined(&HOTPLUG_PATH_LEN);
    eval("sub KOBJ_ADD () { (__forcekobject_action_t)0x01; }") unless defined(&KOBJ_ADD);
    eval("sub KOBJ_REMOVE () { (__forcekobject_action_t)0x02; }") unless defined(&KOBJ_REMOVE);
    eval("sub KOBJ_CHANGE () { (__forcekobject_action_t)0x03; }") unless defined(&KOBJ_CHANGE);
    eval("sub KOBJ_MOUNT () { (__forcekobject_action_t)0x04; }") unless defined(&KOBJ_MOUNT);
    eval("sub KOBJ_UMOUNT () { (__forcekobject_action_t)0x05; }") unless defined(&KOBJ_UMOUNT);
    eval("sub KOBJ_OFFLINE () { (__forcekobject_action_t)0x06; }") unless defined(&KOBJ_OFFLINE);
    eval("sub KOBJ_ONLINE () { (__forcekobject_action_t)0x07; }") unless defined(&KOBJ_ONLINE);
    if(defined(&CONFIG_KOBJECT_UEVENT)) {
    } else {
	eval 'sub kobject_uevent {
	    local($kobj,$action,$attr) = @_;
    	    eval q({ 0; });
	}' unless defined(&kobject_uevent);
	eval 'sub kobject_uevent_atomic {
	    local($kobj,$action,$attr) = @_;
    	    eval q({ 0; });
	}' unless defined(&kobject_uevent_atomic);
    }
}
1;
