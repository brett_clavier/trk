require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_KREF_H_)) {
    eval 'sub _KREF_H_ () {1;}' unless defined(&_KREF_H_);
    if(defined(&__KERNEL__)) {
	require 'linux/types.ph';
	require 'asm/atomic.ph';
    }
}
1;
