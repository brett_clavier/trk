# This file was created by h2ph version 2
unless (defined &_FILE_OFFSET_BITS) { sub _FILE_OFFSET_BITS() { 64 } }

unless (defined &_LARGEFILE_SOURCE) { sub _LARGEFILE_SOURCE() { 1 } }

unless (defined &_POSIX_C_SOURCE) { sub _POSIX_C_SOURCE() { 199506 } }

unless (defined &_POSIX_SOURCE) { sub _POSIX_SOURCE() { 1 } }

unless (defined &__ELF__) { sub __ELF__() { 1 } }

unless (defined &__GLIBC_MINOR__) { sub __GLIBC_MINOR__() { 3 } }

unless (defined &__GLIBC__) { sub __GLIBC__() { 2 } }

unless (defined &__GNUC_MINOR__) { sub __GNUC_MINOR__() { 4 } }

unless (defined &__GNUC__) { sub __GNUC__() { 3 } }

unless (defined &__GNU_LIBRARY__) { sub __GNU_LIBRARY__() { 6 } }

unless (defined &__STDC__) { sub __STDC__() { 1 } }

unless (defined &__USE_BSD) { sub __USE_BSD() { 1 } }

unless (defined &__USE_FILE_OFFSET64) { sub __USE_FILE_OFFSET64() { 1 } }

unless (defined &__USE_LARGEFILE) { sub __USE_LARGEFILE() { 1 } }

unless (defined &__USE_MISC) { sub __USE_MISC() { 1 } }

unless (defined &__USE_POSIX) { sub __USE_POSIX() { 1 } }

unless (defined &__USE_POSIX199309) { sub __USE_POSIX199309() { 1 } }

unless (defined &__USE_POSIX199506) { sub __USE_POSIX199506() { 1 } }

unless (defined &__USE_POSIX2) { sub __USE_POSIX2() { 1 } }

unless (defined &__USE_SVID) { sub __USE_SVID() { 1 } }

unless (defined &__i386) { sub __i386() { 1 } }

unless (defined &__i386__) { sub __i386__() { 1 } }

unless (defined &__i586) { sub __i586() { 1 } }

unless (defined &__i586__) { sub __i586__() { 1 } }

unless (defined &__linux) { sub __linux() { 1 } }

unless (defined &__linux__) { sub __linux__() { 1 } }

unless (defined &__unix) { sub __unix() { 1 } }

unless (defined &__unix__) { sub __unix__() { 1 } }

unless (defined &i386) { sub i386() { 1 } }

unless (defined &linux) { sub linux() { 1 } }

unless (defined &unix) { sub unix() { 1 } }

