require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_SYS_SYSMACROS_H)) {
    eval 'sub _SYS_SYSMACROS_H () {1;}' unless defined(&_SYS_SYSMACROS_H);
    require 'features.ph';
    if(defined(&__GLIBC_HAVE_LONG_LONG)) {
	if(defined (defined(&__GNUC__) ? &__GNUC__ : 0)  && (defined(&__GNUC__) ? &__GNUC__ : 0) >= 2) {
	    eval 'sub __NTH {
	        eval q(( &gnu_dev_major ) { (( &__dev >> 8) & 0xfff) | ( ( &__dev >> 32) & ~0xfff); });
	    }' unless defined(&__NTH);
	    eval 'sub __NTH {
	        eval q(( &gnu_dev_minor ) { ( &__dev & 0xff) | ( ( &__dev >> 12) & ~0xff); });
	    }' unless defined(&__NTH);
	    eval 'sub __NTH {
	        eval q(( &gnu_dev_makedev (my $__major, my $__minor)) { (( $__minor & 0xff) | (( $__major & 0xfff) << 8) | (( ( $__minor & ~0xff)) << 12) | (( ( $__major & ~0xfff)) << 32)); });
	    }' unless defined(&__NTH);
	}
	eval 'sub major {
	    local($dev) = @_;
    	    eval q( &gnu_dev_major ($dev));
	}' unless defined(&major);
	eval 'sub minor {
	    local($dev) = @_;
    	    eval q( &gnu_dev_minor ($dev));
	}' unless defined(&minor);
	eval 'sub makedev {
	    local($maj, $min) = @_;
    	    eval q( &gnu_dev_makedev ($maj, $min));
	}' unless defined(&makedev);
    }
}
1;
