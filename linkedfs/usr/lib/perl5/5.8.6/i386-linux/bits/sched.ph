require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__need_schedparam)) {
    unless(defined(&_SCHED_H)) {
	die("Never include <bits/sched.h> directly; use <sched.h> instead.");
    }
    eval 'sub SCHED_OTHER () {0;}' unless defined(&SCHED_OTHER);
    eval 'sub SCHED_FIFO () {1;}' unless defined(&SCHED_FIFO);
    eval 'sub SCHED_RR () {2;}' unless defined(&SCHED_RR);
    if(defined(&__USE_MISC)) {
	eval 'sub CSIGNAL () {0xff;}' unless defined(&CSIGNAL);
	eval 'sub CLONE_VM () {0x100;}' unless defined(&CLONE_VM);
	eval 'sub CLONE_FS () {0x200;}' unless defined(&CLONE_FS);
	eval 'sub CLONE_FILES () {0x400;}' unless defined(&CLONE_FILES);
	eval 'sub CLONE_SIGHAND () {0x800;}' unless defined(&CLONE_SIGHAND);
	eval 'sub CLONE_PTRACE () {0x2000;}' unless defined(&CLONE_PTRACE);
	eval 'sub CLONE_VFORK () {0x4000;}' unless defined(&CLONE_VFORK);
	eval 'sub CLONE_PARENT () {0x8000;}' unless defined(&CLONE_PARENT);
	eval 'sub CLONE_THREAD () {0x10000;}' unless defined(&CLONE_THREAD);
	eval 'sub CLONE_NEWNS () {0x20000;}' unless defined(&CLONE_NEWNS);
	eval 'sub CLONE_SYSVSEM () {0x40000;}' unless defined(&CLONE_SYSVSEM);
	eval 'sub CLONE_SETTLS () {0x80000;}' unless defined(&CLONE_SETTLS);
	eval 'sub CLONE_PARENT_SETTID () {0x100000;}' unless defined(&CLONE_PARENT_SETTID);
	eval 'sub CLONE_CHILD_CLEARTID () {0x200000;}' unless defined(&CLONE_CHILD_CLEARTID);
	eval 'sub CLONE_DETACHED () {0x400000;}' unless defined(&CLONE_DETACHED);
	eval 'sub CLONE_UNTRACED () {0x800000;}' unless defined(&CLONE_UNTRACED);
	eval 'sub CLONE_CHILD_SETTID () {0x1000000;}' unless defined(&CLONE_CHILD_SETTID);
	eval 'sub CLONE_STOPPED () {0x2000000;}' unless defined(&CLONE_STOPPED);
    }
    if(defined(&__USE_MISC)) {
    }
}
if(!defined (defined(&__defined_schedparam) ? &__defined_schedparam : 0)  && (defined (defined(&__need_schedparam) ? &__need_schedparam : 0) || defined (defined(&_SCHED_H) ? &_SCHED_H : 0))) {
    eval 'sub __defined_schedparam () {1;}' unless defined(&__defined_schedparam);
    undef(&__need_schedparam) if defined(&__need_schedparam);
}
if(defined (defined(&_SCHED_H) ? &_SCHED_H : 0)  && !defined (defined(&__cpu_set_t_defined) ? &__cpu_set_t_defined : 0)) {
    eval 'sub __cpu_set_t_defined () {1;}' unless defined(&__cpu_set_t_defined);
    eval 'sub __CPU_SETSIZE () {1024;}' unless defined(&__CPU_SETSIZE);
    eval 'sub __NCPUBITS () {(8* $sizeof{ &__cpu_mask});}' unless defined(&__NCPUBITS);
    eval 'sub __CPUELT {
        local($cpu) = @_;
	    eval q((($cpu) /  &__NCPUBITS));
    }' unless defined(&__CPUELT);
    eval 'sub __CPUMASK {
        local($cpu) = @_;
	    eval q((( &__cpu_mask) 1<< (($cpu) %  &__NCPUBITS)));
    }' unless defined(&__CPUMASK);
    eval 'sub __CPU_ZERO {
        local($cpusetp) = @_;
	    eval q( &do { \'unsigned int __i\';  &cpu_set_t * &__arr = ($cpusetp);  &for ( &__i = 0;  &__i < $sizeof{ &cpu_set_t} / $sizeof{ &__cpu_mask}; ++ &__i)  ($__arr->{__bits[&__i]}) = 0; }  &while (0));
    }' unless defined(&__CPU_ZERO);
    eval 'sub __CPU_SET {
        local($cpu, $cpusetp) = @_;
	    eval q((($cpusetp)-> $__bits[ &__CPUELT ($cpu)] |=  &__CPUMASK ($cpu)));
    }' unless defined(&__CPU_SET);
    eval 'sub __CPU_CLR {
        local($cpu, $cpusetp) = @_;
	    eval q((($cpusetp)-> $__bits[ &__CPUELT ($cpu)] &= ~ &__CPUMASK ($cpu)));
    }' unless defined(&__CPU_CLR);
    eval 'sub __CPU_ISSET {
        local($cpu, $cpusetp) = @_;
	    eval q(((($cpusetp)-> $__bits[ &__CPUELT ($cpu)] &  &__CPUMASK ($cpu)) != 0));
    }' unless defined(&__CPU_ISSET);
}
1;
