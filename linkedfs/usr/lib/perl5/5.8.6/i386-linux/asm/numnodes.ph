require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_MAX_NUMNODES_H)) {
    eval 'sub _ASM_MAX_NUMNODES_H () {1;}' unless defined(&_ASM_MAX_NUMNODES_H);
    require 'linux/config.ph';
    if(defined(&CONFIG_X86_NUMAQ)) {
	eval 'sub NODES_SHIFT () {4;}' unless defined(&NODES_SHIFT);
    }
 elsif(defined( &CONFIG_ACPI_SRAT)) {
	eval 'sub NODES_SHIFT () {3;}' unless defined(&NODES_SHIFT);
    }
}
1;
