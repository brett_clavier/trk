require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_I386_RWLOCK_H)) {
    eval 'sub _ASM_I386_RWLOCK_H () {1;}' unless defined(&_ASM_I386_RWLOCK_H);
    eval 'sub RW_LOCK_BIAS () {0x1000000;}' unless defined(&RW_LOCK_BIAS);
    eval 'sub RW_LOCK_BIAS_STR () {"0x01000000";}' unless defined(&RW_LOCK_BIAS_STR);
    eval 'sub __build_read_lock_ptr {
        local($rw, $helper) = @_;
	    eval q( &asm  &volatile( &LOCK \\"subl $1,(%0)\\\\n\\\\t\\" \\"jns 1f\\\\n\\" \\"call \\" $helper \\"\\\\n\\\\t\\" \\"1:\\\\n\\" ::\\"a\\" ($rw) : \\"memory\\"));
    }' unless defined(&__build_read_lock_ptr);
    eval 'sub __build_read_lock_const {
        local($rw, $helper) = @_;
	    eval q( &asm  &volatile( &LOCK \\"subl $1,%0\\\\n\\\\t\\" \\"jns 1f\\\\n\\" \\"pushl %%eax\\\\n\\\\t\\" \\"leal %0,%%eax\\\\n\\\\t\\" \\"call \\" $helper \\"\\\\n\\\\t\\" \\"popl %%eax\\\\n\\\\t\\" \\"1:\\\\n\\" :\\"=m\\" (*( &volatile \'int\' *)$rw) : : \\"memory\\"));
    }' unless defined(&__build_read_lock_const);
    eval 'sub __build_read_lock {
        local($rw, $helper) = @_;
	    eval q( &do {  &if ( &__builtin_constant_p($rw))  &__build_read_lock_const($rw, $helper);  &else  &__build_read_lock_ptr($rw, $helper); }  &while (0));
    }' unless defined(&__build_read_lock);
    eval 'sub __build_write_lock_ptr {
        local($rw, $helper) = @_;
	    eval q( &asm  &volatile( &LOCK \\"subl $\\"  &RW_LOCK_BIAS_STR \\",(%0)\\\\n\\\\t\\" \\"jz 1f\\\\n\\" \\"call \\" $helper \\"\\\\n\\\\t\\" \\"1:\\\\n\\" ::\\"a\\" ($rw) : \\"memory\\"));
    }' unless defined(&__build_write_lock_ptr);
    eval 'sub __build_write_lock_const {
        local($rw, $helper) = @_;
	    eval q( &asm  &volatile( &LOCK \\"subl $\\"  &RW_LOCK_BIAS_STR \\",%0\\\\n\\\\t\\" \\"jz 1f\\\\n\\" \\"pushl %%eax\\\\n\\\\t\\" \\"leal %0,%%eax\\\\n\\\\t\\" \\"call \\" $helper \\"\\\\n\\\\t\\" \\"popl %%eax\\\\n\\\\t\\" \\"1:\\\\n\\" :\\"=m\\" (*( &volatile \'int\' *)$rw) : : \\"memory\\"));
    }' unless defined(&__build_write_lock_const);
    eval 'sub __build_write_lock {
        local($rw, $helper) = @_;
	    eval q( &do {  &if ( &__builtin_constant_p($rw))  &__build_write_lock_const($rw, $helper);  &else  &__build_write_lock_ptr($rw, $helper); }  &while (0));
    }' unless defined(&__build_write_lock);
}
1;
