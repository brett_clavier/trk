require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_MMZONE_H_)) {
    eval 'sub _ASM_MMZONE_H_ () {1;}' unless defined(&_ASM_MMZONE_H_);
    require 'asm/smp.ph';
    if(defined(&CONFIG_DISCONTIGMEM)) {
	if(defined(&CONFIG_NUMA)) {
	    if(defined(&CONFIG_X86_NUMAQ)) {
		require 'asm/numaq.ph';
	    } else {
		require 'asm/srat.ph';
	    }
	} else {
	    eval 'sub get_memcfg_numa () { &get_memcfg_numa_flat;}' unless defined(&get_memcfg_numa);
	    eval 'sub get_zholes_size {
	        local($n) = @_;
    		eval q((0));
	    }' unless defined(&get_zholes_size);
	}
	eval 'sub NODE_DATA {
	    local($nid) = @_;
    	    eval q(( $node_data[$nid]));
	}' unless defined(&NODE_DATA);
	eval 'sub MAX_NR_PAGES () {16777216;}' unless defined(&MAX_NR_PAGES);
	eval 'sub MAX_ELEMENTS () {256;}' unless defined(&MAX_ELEMENTS);
	eval 'sub PAGES_PER_ELEMENT () {( &MAX_NR_PAGES/ &MAX_ELEMENTS);}' unless defined(&PAGES_PER_ELEMENT);
# some #ifdef were dropped here -- fill in the blanks
	eval 'sub pfn_to_nid {
	    local($pfn) = @_;
    	    eval q({ });
	}' unless defined(&pfn_to_nid);
	eval 'sub pglist_data {
	    eval q(* &pfn_to_pgdat { ( &NODE_DATA( &pfn_to_nid( &pfn))); });
	}' unless defined(&pglist_data);
	eval 'sub reserve_bootmem {
	    local($addr, $size) = @_;
    	    eval q( &reserve_bootmem_node( &NODE_DATA(0), ($addr), ($size)));
	}' unless defined(&reserve_bootmem);
	eval 'sub alloc_bootmem {
	    local($x) = @_;
    	    eval q( &__alloc_bootmem_node( &NODE_DATA(0), ($x),  &SMP_CACHE_BYTES,  &__pa( &MAX_DMA_ADDRESS)));
	}' unless defined(&alloc_bootmem);
	eval 'sub alloc_bootmem_low {
	    local($x) = @_;
    	    eval q( &__alloc_bootmem_node( &NODE_DATA(0), ($x),  &SMP_CACHE_BYTES, 0));
	}' unless defined(&alloc_bootmem_low);
	eval 'sub alloc_bootmem_pages {
	    local($x) = @_;
    	    eval q( &__alloc_bootmem_node( &NODE_DATA(0), ($x),  &PAGE_SIZE,  &__pa( &MAX_DMA_ADDRESS)));
	}' unless defined(&alloc_bootmem_pages);
	eval 'sub alloc_bootmem_low_pages {
	    local($x) = @_;
    	    eval q( &__alloc_bootmem_node( &NODE_DATA(0), ($x),  &PAGE_SIZE, 0));
	}' unless defined(&alloc_bootmem_low_pages);
	eval 'sub alloc_bootmem_node {
	    local($ignore, $x) = @_;
    	    eval q( &__alloc_bootmem_node( &NODE_DATA(0), ($x),  &SMP_CACHE_BYTES,  &__pa( &MAX_DMA_ADDRESS)));
	}' unless defined(&alloc_bootmem_node);
	eval 'sub alloc_bootmem_pages_node {
	    local($ignore, $x) = @_;
    	    eval q( &__alloc_bootmem_node( &NODE_DATA(0), ($x),  &PAGE_SIZE,  &__pa( &MAX_DMA_ADDRESS)));
	}' unless defined(&alloc_bootmem_pages_node);
	eval 'sub alloc_bootmem_low_pages_node {
	    local($ignore, $x) = @_;
    	    eval q( &__alloc_bootmem_node( &NODE_DATA(0), ($x),  &PAGE_SIZE, 0));
	}' unless defined(&alloc_bootmem_low_pages_node);
	eval 'sub node_localnr {
	    local($pfn, $nid) = @_;
    	    eval q((($pfn) -  ($node_data[$nid]->{node_start_pfn})));
	}' unless defined(&node_localnr);
	eval 'sub kvaddr_to_nid {
	    local($kaddr) = @_;
    	    eval q( &pfn_to_nid( &__pa($kaddr) >>  &PAGE_SHIFT));
	}' unless defined(&kvaddr_to_nid);
	eval 'sub node_mem_map {
	    local($nid) = @_;
    	    eval q(( &NODE_DATA($nid)-> &node_mem_map));
	}' unless defined(&node_mem_map);
	eval 'sub node_start_pfn {
	    local($nid) = @_;
    	    eval q(( &NODE_DATA($nid)-> &node_start_pfn));
	}' unless defined(&node_start_pfn);
	eval 'sub node_end_pfn {
	    local($nid) = @_;
    	    eval q(({  &pg_data_t * &__pgdat =  &NODE_DATA($nid);  ($__pgdat->{node_start_pfn}) +  ($__pgdat->{node_spanned_pages}); }));
	}' unless defined(&node_end_pfn);
	eval 'sub local_mapnr {
	    local($kvaddr) = @_;
    	    eval q(({ \'unsigned long __pfn\' =  &__pa($kvaddr) >>  &PAGE_SHIFT; ( &__pfn -  &node_start_pfn( &pfn_to_nid( &__pfn))); }));
	}' unless defined(&local_mapnr);
	eval 'sub kern_addr_valid {
	    local($kaddr) = @_;
    	    eval q((0));
	}' unless defined(&kern_addr_valid);
	eval 'sub pfn_to_page {
	    local($pfn) = @_;
    	    eval q(({ \'unsigned long __pfn\' = $pfn; \'int\'  &__node =  &pfn_to_nid( &__pfn);  &node_mem_map( &__node)[ &node_localnr( &__pfn, &__node)]; }));
	}' unless defined(&pfn_to_page);
	eval 'sub page_to_pfn {
	    local($pg) = @_;
    	    eval q(({ \'struct page\' * &__page = $pg; \'struct zone\' * &__zone =  &page_zone( &__page); ( &__page -  ($__zone->{zone_mem_map})) +  ($__zone->{zone_start_pfn}); }));
	}' unless defined(&page_to_pfn);
	eval 'sub pmd_page {
	    local($pmd) = @_;
    	    eval q(( &pfn_to_page( &pmd_val($pmd) >>  &PAGE_SHIFT)));
	}' unless defined(&pmd_page);
	if(defined(&CONFIG_X86_NUMAQ)) {
	    eval 'sub pfn_valid {
	        local($pfn) = @_;
    		eval q((($pfn) <  &num_physpages));
	    }' unless defined(&pfn_valid);
	} else {
	    eval 'sub pfn_valid {
	        local($pfn) = @_;
    		eval q({ \'int\'  &nid =  &pfn_to_nid($pfn);  &if ( &nid >= 0) ($pfn <  &node_end_pfn( &nid)); 0; });
	    }' unless defined(&pfn_valid);
	}
	if(defined(&CONFIG_X86_NUMAQ)) {
	}
 elsif((defined(&CONFIG_ACPI_SRAT) ? &CONFIG_ACPI_SRAT : 0)) {
	}
    }
}
1;
