require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_SEMAPHORE_H)) {
    eval 'sub _I386_SEMAPHORE_H () {1;}' unless defined(&_I386_SEMAPHORE_H);
    require 'linux/linkage.ph';
    if(defined(&__KERNEL__)) {
	require 'asm/system.ph';
	require 'asm/atomic.ph';
	require 'linux/wait.ph';
	require 'linux/rwsem.ph';
	eval 'sub __SEMAPHORE_INITIALIZER {
	    local($name, $n) = @_;
    	    eval q({ . &count =  &ATOMIC_INIT($n), . &sleepers = 0, . &wait =  &__WAIT_QUEUE_HEAD_INITIALIZER(($name). &wait) });
	}' unless defined(&__SEMAPHORE_INITIALIZER);
	eval 'sub __MUTEX_INITIALIZER {
	    local($name) = @_;
    	    eval q( &__SEMAPHORE_INITIALIZER($name,1));
	}' unless defined(&__MUTEX_INITIALIZER);
	eval 'sub __DECLARE_SEMAPHORE_GENERIC {
	    local($name,$count) = @_;
    	    eval q(\'struct semaphore\' $name =  &__SEMAPHORE_INITIALIZER($name,$count));
	}' unless defined(&__DECLARE_SEMAPHORE_GENERIC);
	eval 'sub DECLARE_MUTEX {
	    local($name) = @_;
    	    eval q( &__DECLARE_SEMAPHORE_GENERIC($name,1));
	}' unless defined(&DECLARE_MUTEX);
	eval 'sub DECLARE_MUTEX_LOCKED {
	    local($name) = @_;
    	    eval q( &__DECLARE_SEMAPHORE_GENERIC($name,0));
	}' unless defined(&DECLARE_MUTEX_LOCKED);
	eval 'sub sema_init {
	    local($sem,$val) = @_;
    	    eval q({  &atomic_set( ($sem->{count}), $val);  ($sem->{sleepers}) = 0;  &init_waitqueue_head( ($sem->{wait})); });
	}' unless defined(&sema_init);
	eval 'sub init_MUTEX {
	    local($sem) = @_;
    	    eval q({  &sema_init($sem, 1); });
	}' unless defined(&init_MUTEX);
	eval 'sub init_MUTEX_LOCKED {
	    local($sem) = @_;
    	    eval q({  &sema_init($sem, 0); });
	}' unless defined(&init_MUTEX_LOCKED);
    }
}
1;
