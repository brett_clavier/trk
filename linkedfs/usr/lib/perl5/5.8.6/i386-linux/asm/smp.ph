require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&__ASM_SMP_H)) {
    eval 'sub __ASM_SMP_H () {1;}' unless defined(&__ASM_SMP_H);
    unless(defined(&__ASSEMBLY__)) {
	require 'linux/config.ph';
	require 'linux/kernel.ph';
	require 'linux/threads.ph';
	require 'linux/cpumask.ph';
    }
    if(defined(&CONFIG_X86_LOCAL_APIC)) {
	unless(defined(&__ASSEMBLY__)) {
	    require 'asm/fixmap.ph';
	    require 'asm/bitops.ph';
	    require 'asm/mpspec.ph';
	    if(defined(&CONFIG_X86_IO_APIC)) {
		require 'asm/io_apic.ph';
	    }
	    require 'asm/apic.ph';
	}
    }
    eval 'sub BAD_APICID () {0xff;}' unless defined(&BAD_APICID);
    if(defined(&CONFIG_SMP)) {
	unless(defined(&__ASSEMBLY__)) {
	    eval 'sub MAX_APICID () {256;}' unless defined(&MAX_APICID);
	    eval 'sub smp_processor_id () {
	        eval q(( &current_thread_info()-> &cpu));
	    }' unless defined(&smp_processor_id);
	    eval 'sub cpu_possible_map () { &cpu_callout_map;}' unless defined(&cpu_possible_map);
	    if(defined(&CONFIG_X86_LOCAL_APIC)) {
		if(defined(&APIC_DEFINITION)) {
		} else {
		    require 'mach_apicdef.ph';
		    eval 'sub hard_smp_processor_id {
		        local($void) = @_;
    			eval q({  &GET_APIC_ID(*( &APIC_BASE+ &APIC_ID)); });
		    }' unless defined(&hard_smp_processor_id);
		}
		eval 'sub logical_smp_processor_id {
		    local($void) = @_;
    		    eval q({  &GET_APIC_LOGICAL_ID(*( &APIC_BASE+ &APIC_LDR)); });
		}' unless defined(&logical_smp_processor_id);
	    }
	}
	eval 'sub NO_PROC_ID () {0xff;}' unless defined(&NO_PROC_ID);
    }
}
1;
