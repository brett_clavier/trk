require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_RESOURCE_H)) {
    eval 'sub _I386_RESOURCE_H () {1;}' unless defined(&_I386_RESOURCE_H);
    eval 'sub RLIMIT_CPU () {0;}' unless defined(&RLIMIT_CPU);
    eval 'sub RLIMIT_FSIZE () {1;}' unless defined(&RLIMIT_FSIZE);
    eval 'sub RLIMIT_DATA () {2;}' unless defined(&RLIMIT_DATA);
    eval 'sub RLIMIT_STACK () {3;}' unless defined(&RLIMIT_STACK);
    eval 'sub RLIMIT_CORE () {4;}' unless defined(&RLIMIT_CORE);
    eval 'sub RLIMIT_RSS () {5;}' unless defined(&RLIMIT_RSS);
    eval 'sub RLIMIT_NPROC () {6;}' unless defined(&RLIMIT_NPROC);
    eval 'sub RLIMIT_NOFILE () {7;}' unless defined(&RLIMIT_NOFILE);
    eval 'sub RLIMIT_MEMLOCK () {8;}' unless defined(&RLIMIT_MEMLOCK);
    eval 'sub RLIMIT_AS () {9;}' unless defined(&RLIMIT_AS);
    eval 'sub RLIMIT_LOCKS () {10;}' unless defined(&RLIMIT_LOCKS);
    eval 'sub RLIMIT_SIGPENDING () {11;}' unless defined(&RLIMIT_SIGPENDING);
    eval 'sub RLIMIT_MSGQUEUE () {12;}' unless defined(&RLIMIT_MSGQUEUE);
    eval 'sub RLIM_NLIMITS () {13;}' unless defined(&RLIM_NLIMITS);
    eval 'sub RLIM_INFINITY () {(~0);}' unless defined(&RLIM_INFINITY);
    if(defined(&__KERNEL__)) {
	eval 'sub INIT_RLIMITS () {{ {  &RLIM_INFINITY,  &RLIM_INFINITY }, {  &RLIM_INFINITY,  &RLIM_INFINITY }, {  &RLIM_INFINITY,  &RLIM_INFINITY }, {  &_STK_LIM,  &RLIM_INFINITY }, { 0,  &RLIM_INFINITY }, {  &RLIM_INFINITY,  &RLIM_INFINITY }, { 0, 0}, {  &INR_OPEN,  &INR_OPEN }, {  &MLOCK_LIMIT,  &MLOCK_LIMIT }, {  &RLIM_INFINITY,  &RLIM_INFINITY }, {  &RLIM_INFINITY,  &RLIM_INFINITY }, {  &MAX_SIGPENDING,  &MAX_SIGPENDING }, {  &MQ_BYTES_MAX,  &MQ_BYTES_MAX }, };}' unless defined(&INIT_RLIMITS);
    }
}
1;
