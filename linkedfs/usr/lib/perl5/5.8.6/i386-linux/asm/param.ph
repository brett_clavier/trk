require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASMi386_PARAM_H)) {
    eval 'sub _ASMi386_PARAM_H () {1;}' unless defined(&_ASMi386_PARAM_H);
    if(defined(&__KERNEL__)) {
	eval 'sub HZ () {1000;}' unless defined(&HZ);
	eval 'sub USER_HZ () {100;}' unless defined(&USER_HZ);
	eval 'sub CLOCKS_PER_SEC () {( &USER_HZ);}' unless defined(&CLOCKS_PER_SEC);
    }
    unless(defined(&HZ)) {
	eval 'sub HZ () {100;}' unless defined(&HZ);
    }
    eval 'sub EXEC_PAGESIZE () {4096;}' unless defined(&EXEC_PAGESIZE);
    unless(defined(&NOGROUP)) {
	eval 'sub NOGROUP () {(-1);}' unless defined(&NOGROUP);
    }
    eval 'sub MAXHOSTNAMELEN () {64;}' unless defined(&MAXHOSTNAMELEN);
    eval 'sub COMMAND_LINE_SIZE () {256;}' unless defined(&COMMAND_LINE_SIZE);
}
1;
