require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_BYTEORDER_H)) {
    eval 'sub _I386_BYTEORDER_H () {1;}' unless defined(&_I386_BYTEORDER_H);
    require 'asm/types.ph';
    require 'linux/compiler.ph';
    if(defined(&__GNUC__)) {
	eval 'sub ___arch__swab32 {
	    local($x) = @_;
    	    eval q({  &__asm__(\\"bswap %0\\" : \\"=r\\" ($x) : \\"0\\" ($x)); $x; });
	}' unless defined(&___arch__swab32);
# some #ifdef were dropped here -- fill in the blanks
	eval 'sub ___arch__swab64 {
	    local($val) = @_;
    	    eval q({ \'union union\' { \'struct struct\' {  &__u32  &a, &b; }  &s;  &__u64  &u; }  &v;  ($v->{u}) = $val;  ($v->{u}); } );
	}' unless defined(&___arch__swab64);
	eval 'sub __arch__swab64 {
	    local($x) = @_;
    	    eval q( &___arch__swab64($x));
	}' unless defined(&__arch__swab64);
	eval 'sub __arch__swab32 {
	    local($x) = @_;
    	    eval q( &___arch__swab32($x));
	}' unless defined(&__arch__swab32);
	eval 'sub __BYTEORDER_HAS_U64__ () {1;}' unless defined(&__BYTEORDER_HAS_U64__);
    }
    require 'linux/byteorder/little_endian.ph';
}
1;
