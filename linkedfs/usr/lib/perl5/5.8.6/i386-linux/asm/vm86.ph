require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_LINUX_VM86_H)) {
    eval 'sub _LINUX_VM86_H () {1;}' unless defined(&_LINUX_VM86_H);
    eval 'sub TF_MASK () {0x100;}' unless defined(&TF_MASK);
    eval 'sub IF_MASK () {0x200;}' unless defined(&IF_MASK);
    eval 'sub IOPL_MASK () {0x3000;}' unless defined(&IOPL_MASK);
    eval 'sub NT_MASK () {0x4000;}' unless defined(&NT_MASK);
    eval 'sub VM_MASK () {0x20000;}' unless defined(&VM_MASK);
    eval 'sub AC_MASK () {0x40000;}' unless defined(&AC_MASK);
    eval 'sub VIF_MASK () {0x80000;}' unless defined(&VIF_MASK);
    eval 'sub VIP_MASK () {0x100000;}' unless defined(&VIP_MASK);
    eval 'sub ID_MASK () {0x200000;}' unless defined(&ID_MASK);
    eval 'sub BIOSSEG () {0xf000;}' unless defined(&BIOSSEG);
    eval 'sub CPU_086 () {0;}' unless defined(&CPU_086);
    eval 'sub CPU_186 () {1;}' unless defined(&CPU_186);
    eval 'sub CPU_286 () {2;}' unless defined(&CPU_286);
    eval 'sub CPU_386 () {3;}' unless defined(&CPU_386);
    eval 'sub CPU_486 () {4;}' unless defined(&CPU_486);
    eval 'sub CPU_586 () {5;}' unless defined(&CPU_586);
    eval 'sub VM86_TYPE {
        local($retval) = @_;
	    eval q((($retval) & 0xff));
    }' unless defined(&VM86_TYPE);
    eval 'sub VM86_ARG {
        local($retval) = @_;
	    eval q((($retval) >> 8));
    }' unless defined(&VM86_ARG);
    eval 'sub VM86_SIGNAL () {0;}' unless defined(&VM86_SIGNAL);
    eval 'sub VM86_UNKNOWN () {1;}' unless defined(&VM86_UNKNOWN);
    eval 'sub VM86_INTx () {2;}' unless defined(&VM86_INTx);
    eval 'sub VM86_STI () {3;}' unless defined(&VM86_STI);
    eval 'sub VM86_PICRETURN () {4;}' unless defined(&VM86_PICRETURN);
    eval 'sub VM86_TRAP () {6;}' unless defined(&VM86_TRAP);
    eval 'sub VM86_PLUS_INSTALL_CHECK () {0;}' unless defined(&VM86_PLUS_INSTALL_CHECK);
    eval 'sub VM86_ENTER () {1;}' unless defined(&VM86_ENTER);
    eval 'sub VM86_ENTER_NO_BYPASS () {2;}' unless defined(&VM86_ENTER_NO_BYPASS);
    eval 'sub VM86_REQUEST_IRQ () {3;}' unless defined(&VM86_REQUEST_IRQ);
    eval 'sub VM86_FREE_IRQ () {4;}' unless defined(&VM86_FREE_IRQ);
    eval 'sub VM86_GET_IRQ_BITS () {5;}' unless defined(&VM86_GET_IRQ_BITS);
    eval 'sub VM86_GET_AND_RESET_IRQ () {6;}' unless defined(&VM86_GET_AND_RESET_IRQ);
    eval 'sub VM86_SCREEN_BITMAP () {0x1;}' unless defined(&VM86_SCREEN_BITMAP);
    if(defined(&__KERNEL__)) {
	eval 'sub VM86_TSS_ESP0 () { &flags;}' unless defined(&VM86_TSS_ESP0);
    }
}
1;
