require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_I386_PTRACE_H)) {
    eval 'sub _I386_PTRACE_H () {1;}' unless defined(&_I386_PTRACE_H);
    eval 'sub EBX () {0;}' unless defined(&EBX);
    eval 'sub ECX () {1;}' unless defined(&ECX);
    eval 'sub EDX () {2;}' unless defined(&EDX);
    eval 'sub ESI () {3;}' unless defined(&ESI);
    eval 'sub EDI () {4;}' unless defined(&EDI);
    eval 'sub EBP () {5;}' unless defined(&EBP);
    eval 'sub EAX () {6;}' unless defined(&EAX);
    eval 'sub DS () {7;}' unless defined(&DS);
    eval 'sub ES () {8;}' unless defined(&ES);
    eval 'sub FS () {9;}' unless defined(&FS);
    eval 'sub GS () {10;}' unless defined(&GS);
    eval 'sub ORIG_EAX () {11;}' unless defined(&ORIG_EAX);
    eval 'sub EIP () {12;}' unless defined(&EIP);
    eval 'sub CS () {13;}' unless defined(&CS);
    eval 'sub EFL () {14;}' unless defined(&EFL);
    eval 'sub UESP () {15;}' unless defined(&UESP);
    eval 'sub SS () {16;}' unless defined(&SS);
    eval 'sub FRAME_SIZE () {17;}' unless defined(&FRAME_SIZE);
    eval 'sub PTRACE_GETREGS () {12;}' unless defined(&PTRACE_GETREGS);
    eval 'sub PTRACE_SETREGS () {13;}' unless defined(&PTRACE_SETREGS);
    eval 'sub PTRACE_GETFPREGS () {14;}' unless defined(&PTRACE_GETFPREGS);
    eval 'sub PTRACE_SETFPREGS () {15;}' unless defined(&PTRACE_SETFPREGS);
    eval 'sub PTRACE_GETFPXREGS () {18;}' unless defined(&PTRACE_GETFPXREGS);
    eval 'sub PTRACE_SETFPXREGS () {19;}' unless defined(&PTRACE_SETFPXREGS);
    eval 'sub PTRACE_OLDSETOPTIONS () {21;}' unless defined(&PTRACE_OLDSETOPTIONS);
    eval 'sub PTRACE_GET_THREAD_AREA () {25;}' unless defined(&PTRACE_GET_THREAD_AREA);
    eval 'sub PTRACE_SET_THREAD_AREA () {26;}' unless defined(&PTRACE_SET_THREAD_AREA);
    eval("sub EF_CF () { 0x00000001; }") unless defined(&EF_CF);
    eval("sub EF_PF () { 0x00000004; }") unless defined(&EF_PF);
    eval("sub EF_AF () { 0x00000010; }") unless defined(&EF_AF);
    eval("sub EF_ZF () { 0x00000040; }") unless defined(&EF_ZF);
    eval("sub EF_SF () { 0x00000080; }") unless defined(&EF_SF);
    eval("sub EF_TF () { 0x00000100; }") unless defined(&EF_TF);
    eval("sub EF_IE () { 0x00000200; }") unless defined(&EF_IE);
    eval("sub EF_DF () { 0x00000400; }") unless defined(&EF_DF);
    eval("sub EF_OF () { 0x00000800; }") unless defined(&EF_OF);
    eval("sub EF_IOPL () { 0x00003000; }") unless defined(&EF_IOPL);
    eval("sub EF_IOPL_RING0 () { 0x00000000; }") unless defined(&EF_IOPL_RING0);
    eval("sub EF_IOPL_RING1 () { 0x00001000; }") unless defined(&EF_IOPL_RING1);
    eval("sub EF_IOPL_RING2 () { 0x00002000; }") unless defined(&EF_IOPL_RING2);
    eval("sub EF_NT () { 0x00004000; }") unless defined(&EF_NT);
    eval("sub EF_RF () { 0x00010000; }") unless defined(&EF_RF);
    eval("sub EF_VM () { 0x00020000; }") unless defined(&EF_VM);
    eval("sub EF_AC () { 0x00040000; }") unless defined(&EF_AC);
    eval("sub EF_VIF () { 0x00080000; }") unless defined(&EF_VIF);
    eval("sub EF_VIP () { 0x00100000; }") unless defined(&EF_VIP);
    eval("sub EF_ID () { 0x00200000; }") unless defined(&EF_ID);
    if(defined(&__KERNEL__)) {
	eval 'sub user_mode {
	    local($regs) = @_;
    	    eval q((( &VM_MASK & ($regs)-> &eflags) || (3& ($regs)-> &xcs)));
	}' unless defined(&user_mode);
	eval 'sub instruction_pointer {
	    local($regs) = @_;
    	    eval q((($regs)-> &eip));
	}' unless defined(&instruction_pointer);
	if(defined( &CONFIG_SMP)  && defined( &CONFIG_FRAME_POINTER)) {
	} else {
	    eval 'sub profile_pc {
	        local($regs) = @_;
    		eval q( &instruction_pointer($regs));
	    }' unless defined(&profile_pc);
	}
    }
}
1;
