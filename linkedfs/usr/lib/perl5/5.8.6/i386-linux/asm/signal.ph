require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASMi386_SIGNAL_H)) {
    eval 'sub _ASMi386_SIGNAL_H () {1;}' unless defined(&_ASMi386_SIGNAL_H);
    require 'linux/types.ph';
    require 'linux/linkage.ph';
    require 'linux/time.ph';
    require 'linux/compiler.ph';
    if(defined(&__KERNEL__)) {
	eval 'sub _NSIG () {64;}' unless defined(&_NSIG);
	eval 'sub _NSIG_BPW () {32;}' unless defined(&_NSIG_BPW);
	eval 'sub _NSIG_WORDS () {( &_NSIG /  &_NSIG_BPW);}' unless defined(&_NSIG_WORDS);
    } else {
	eval 'sub NSIG () {32;}' unless defined(&NSIG);
    }
    eval 'sub SIGHUP () {1;}' unless defined(&SIGHUP);
    eval 'sub SIGINT () {2;}' unless defined(&SIGINT);
    eval 'sub SIGQUIT () {3;}' unless defined(&SIGQUIT);
    eval 'sub SIGILL () {4;}' unless defined(&SIGILL);
    eval 'sub SIGTRAP () {5;}' unless defined(&SIGTRAP);
    eval 'sub SIGABRT () {6;}' unless defined(&SIGABRT);
    eval 'sub SIGIOT () {6;}' unless defined(&SIGIOT);
    eval 'sub SIGBUS () {7;}' unless defined(&SIGBUS);
    eval 'sub SIGFPE () {8;}' unless defined(&SIGFPE);
    eval 'sub SIGKILL () {9;}' unless defined(&SIGKILL);
    eval 'sub SIGUSR1 () {10;}' unless defined(&SIGUSR1);
    eval 'sub SIGSEGV () {11;}' unless defined(&SIGSEGV);
    eval 'sub SIGUSR2 () {12;}' unless defined(&SIGUSR2);
    eval 'sub SIGPIPE () {13;}' unless defined(&SIGPIPE);
    eval 'sub SIGALRM () {14;}' unless defined(&SIGALRM);
    eval 'sub SIGTERM () {15;}' unless defined(&SIGTERM);
    eval 'sub SIGSTKFLT () {16;}' unless defined(&SIGSTKFLT);
    eval 'sub SIGCHLD () {17;}' unless defined(&SIGCHLD);
    eval 'sub SIGCONT () {18;}' unless defined(&SIGCONT);
    eval 'sub SIGSTOP () {19;}' unless defined(&SIGSTOP);
    eval 'sub SIGTSTP () {20;}' unless defined(&SIGTSTP);
    eval 'sub SIGTTIN () {21;}' unless defined(&SIGTTIN);
    eval 'sub SIGTTOU () {22;}' unless defined(&SIGTTOU);
    eval 'sub SIGURG () {23;}' unless defined(&SIGURG);
    eval 'sub SIGXCPU () {24;}' unless defined(&SIGXCPU);
    eval 'sub SIGXFSZ () {25;}' unless defined(&SIGXFSZ);
    eval 'sub SIGVTALRM () {26;}' unless defined(&SIGVTALRM);
    eval 'sub SIGPROF () {27;}' unless defined(&SIGPROF);
    eval 'sub SIGWINCH () {28;}' unless defined(&SIGWINCH);
    eval 'sub SIGIO () {29;}' unless defined(&SIGIO);
    eval 'sub SIGPOLL () { &SIGIO;}' unless defined(&SIGPOLL);
    eval 'sub SIGPWR () {30;}' unless defined(&SIGPWR);
    eval 'sub SIGSYS () {31;}' unless defined(&SIGSYS);
    eval 'sub SIGUNUSED () {31;}' unless defined(&SIGUNUSED);
    eval 'sub SIGRTMIN () {32;}' unless defined(&SIGRTMIN);
    eval 'sub SIGRTMAX () { &_NSIG;}' unless defined(&SIGRTMAX);
    eval 'sub SA_NOCLDSTOP () {0x1;}' unless defined(&SA_NOCLDSTOP);
    eval 'sub SA_NOCLDWAIT () {0x2;}' unless defined(&SA_NOCLDWAIT);
    eval 'sub SA_SIGINFO () {0x4;}' unless defined(&SA_SIGINFO);
    eval 'sub SA_ONSTACK () {0x8000000;}' unless defined(&SA_ONSTACK);
    eval 'sub SA_RESTART () {0x10000000;}' unless defined(&SA_RESTART);
    eval 'sub SA_NODEFER () {0x40000000;}' unless defined(&SA_NODEFER);
    eval 'sub SA_RESETHAND () {0x80000000;}' unless defined(&SA_RESETHAND);
    eval 'sub SA_NOMASK () { &SA_NODEFER;}' unless defined(&SA_NOMASK);
    eval 'sub SA_ONESHOT () { &SA_RESETHAND;}' unless defined(&SA_ONESHOT);
    eval 'sub SA_INTERRUPT () {0x20000000;}' unless defined(&SA_INTERRUPT);
    eval 'sub SA_RESTORER () {0x4000000;}' unless defined(&SA_RESTORER);
    eval 'sub SS_ONSTACK () {1;}' unless defined(&SS_ONSTACK);
    eval 'sub SS_DISABLE () {2;}' unless defined(&SS_DISABLE);
    eval 'sub MINSIGSTKSZ () {2048;}' unless defined(&MINSIGSTKSZ);
    eval 'sub SIGSTKSZ () {8192;}' unless defined(&SIGSTKSZ);
    if(defined(&__KERNEL__)) {
	eval 'sub SA_PROBE () { &SA_ONESHOT;}' unless defined(&SA_PROBE);
	eval 'sub SA_SAMPLE_RANDOM () { &SA_RESTART;}' unless defined(&SA_SAMPLE_RANDOM);
	eval 'sub SA_SHIRQ () {0x4000000;}' unless defined(&SA_SHIRQ);
    }
    eval 'sub SIG_BLOCK () {0;}' unless defined(&SIG_BLOCK);
    eval 'sub SIG_UNBLOCK () {1;}' unless defined(&SIG_UNBLOCK);
    eval 'sub SIG_SETMASK () {2;}' unless defined(&SIG_SETMASK);
    eval 'sub SIG_DFL () {(( &__sighandler_t)0);}' unless defined(&SIG_DFL);
    eval 'sub SIG_IGN () {(( &__sighandler_t)1);}' unless defined(&SIG_IGN);
    eval 'sub SIG_ERR () {(( &__sighandler_t)-1);}' unless defined(&SIG_ERR);
    if(defined(&__KERNEL__)) {
    } else {
	eval 'sub sa_handler () { ($_u->{_sa_handler});}' unless defined(&sa_handler);
	eval 'sub sa_sigaction () { ($_u->{_sa_sigaction});}' unless defined(&sa_sigaction);
    }
    if(defined(&__KERNEL__)) {
	require 'asm/sigcontext.ph';
	eval 'sub __HAVE_ARCH_SIG_BITOPS () {1;}' unless defined(&__HAVE_ARCH_SIG_BITOPS);
	eval 'sub sigaddset {
	    local($set,$_sig) = @_;
    	    eval q({  &__asm__(\\"btsl %1,%0\\" : \\"=m\\"(*$set) : \\"Ir\\"($_sig - 1) : \\"cc\\"); });
	}' unless defined(&sigaddset);
	eval 'sub sigdelset {
	    local($set,$_sig) = @_;
    	    eval q({  &__asm__(\\"btrl %1,%0\\" : \\"=m\\"(*$set) : \\"Ir\\"($_sig - 1) : \\"cc\\"); });
	}' unless defined(&sigdelset);
	eval 'sub __const_sigismember {
	    local($set,$_sig) = @_;
    	    eval q({ my $sig = $_sig - 1; 1& ( ($set->{sig[$sig/_NSIG_BPW]}) >> ( $sig %  &_NSIG_BPW)); });
	}' unless defined(&__const_sigismember);
	eval 'sub __gen_sigismember {
	    local($set,$_sig) = @_;
    	    eval q({ \'int\'  &ret;  &__asm__(\\"btl %2,%1\\\\n\\\\tsbbl %0,%0\\" : \\"=r\\"( &ret) : \\"m\\"(*$set), \\"Ir\\"($_sig-1) : \\"cc\\");  &ret; });
	}' unless defined(&__gen_sigismember);
	eval 'sub sigismember {
	    local($set,$sig) = @_;
    	    eval q(( &__builtin_constant_p($sig) ?  &__const_sigismember(($set),($sig)) :  &__gen_sigismember(($set),($sig))));
	}' unless defined(&sigismember);
	eval 'sub sigfindinword {
	    local($word) = @_;
    	    eval q({  &__asm__(\\"bsfl %1,%0\\" : \\"=r\\"($word) : \\"rm\\"($word) : \\"cc\\"); $word; });
	}' unless defined(&sigfindinword);
	eval 'sub ptrace_signal_deliver {
	    local($regs, $cookie) = @_;
    	    eval q( &do { }  &while (0));
	}' unless defined(&ptrace_signal_deliver);
    }
}
1;
