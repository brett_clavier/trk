require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_THREAD_INFO_H)) {
    eval 'sub _ASM_THREAD_INFO_H () {1;}' unless defined(&_ASM_THREAD_INFO_H);
    if(defined(&__KERNEL__)) {
	require 'linux/config.ph';
	require 'linux/compiler.ph';
	require 'asm/page.ph';
	unless(defined(&__ASSEMBLY__)) {
	    require 'asm/processor.ph';
	}
	unless(defined(&__ASSEMBLY__)) {
	} else {
	    require 'asm/asm_offsets.ph';
	}
	eval 'sub PREEMPT_ACTIVE () {0x10000000;}' unless defined(&PREEMPT_ACTIVE);
	if(defined(&CONFIG_4KSTACKS)) {
	    eval 'sub THREAD_SIZE () {(4096);}' unless defined(&THREAD_SIZE);
	} else {
	    eval 'sub THREAD_SIZE () {(8192);}' unless defined(&THREAD_SIZE);
	}
	eval 'sub STACK_WARN () {( &THREAD_SIZE/8);}' unless defined(&STACK_WARN);
	unless(defined(&__ASSEMBLY__)) {
	    eval 'sub INIT_THREAD_INFO {
	        local($tsk) = @_;
    		eval q({ . &task = $tsk, . &exec_domain =  &default_exec_domain, . &flags = 0, . &cpu = 0, . &preempt_count = 1, . &addr_limit =  &KERNEL_DS, . &restart_block = { . &fn =  &do_no_restart_syscall, }, });
	    }' unless defined(&INIT_THREAD_INFO);
	    eval 'sub init_thread_info () {( ($init_thread_union->{thread_info}));}' unless defined(&init_thread_info);
	    eval 'sub init_stack () {( ($init_thread_union->{stack}));}' unless defined(&init_stack);
	    if(defined(&CONFIG_DEBUG_STACK_USAGE)) {
		eval 'sub alloc_thread_info {
		    local($tsk) = @_;
    		    eval q(({ \'struct thread_info\' * &ret;  &ret =  &kmalloc( &THREAD_SIZE,  &GFP_KERNEL);  &if ( &ret)  &memset( &ret, 0,  &THREAD_SIZE);  &ret; }));
		}' unless defined(&alloc_thread_info);
	    } else {
		eval 'sub alloc_thread_info {
		    local($tsk) = @_;
    		    eval q( &kmalloc( &THREAD_SIZE,  &GFP_KERNEL));
		}' unless defined(&alloc_thread_info);
	    }
	    eval 'sub free_thread_info {
	        local($info) = @_;
    		eval q( &kfree($info));
	    }' unless defined(&free_thread_info);
	    eval 'sub get_thread_info {
	        local($ti) = @_;
    		eval q( &get_task_struct(($ti)-> &task));
	    }' unless defined(&get_thread_info);
	    eval 'sub put_thread_info {
	        local($ti) = @_;
    		eval q( &put_task_struct(($ti)-> &task));
	    }' unless defined(&put_thread_info);
	} else {
	    eval 'sub GET_THREAD_INFO {
	        local($reg) = @_;
    		eval q( &movl $- &THREAD_SIZE, $reg;  &andl % &esp, $reg);
	    }' unless defined(&GET_THREAD_INFO);
	    eval 'sub GET_THREAD_INFO_WITH_ESP {
	        local($reg) = @_;
    		eval q( &andl $- &THREAD_SIZE, $reg);
	    }' unless defined(&GET_THREAD_INFO_WITH_ESP);
	}
	eval 'sub TIF_SYSCALL_TRACE () {0;}' unless defined(&TIF_SYSCALL_TRACE);
	eval 'sub TIF_NOTIFY_RESUME () {1;}' unless defined(&TIF_NOTIFY_RESUME);
	eval 'sub TIF_SIGPENDING () {2;}' unless defined(&TIF_SIGPENDING);
	eval 'sub TIF_NEED_RESCHED () {3;}' unless defined(&TIF_NEED_RESCHED);
	eval 'sub TIF_SINGLESTEP () {4;}' unless defined(&TIF_SINGLESTEP);
	eval 'sub TIF_IRET () {5;}' unless defined(&TIF_IRET);
	eval 'sub TIF_SYSCALL_AUDIT () {7;}' unless defined(&TIF_SYSCALL_AUDIT);
	eval 'sub TIF_POLLING_NRFLAG () {16;}' unless defined(&TIF_POLLING_NRFLAG);
	eval 'sub _TIF_SYSCALL_TRACE () {(1<< &TIF_SYSCALL_TRACE);}' unless defined(&_TIF_SYSCALL_TRACE);
	eval 'sub _TIF_NOTIFY_RESUME () {(1<< &TIF_NOTIFY_RESUME);}' unless defined(&_TIF_NOTIFY_RESUME);
	eval 'sub _TIF_SIGPENDING () {(1<< &TIF_SIGPENDING);}' unless defined(&_TIF_SIGPENDING);
	eval 'sub _TIF_NEED_RESCHED () {(1<< &TIF_NEED_RESCHED);}' unless defined(&_TIF_NEED_RESCHED);
	eval 'sub _TIF_SINGLESTEP () {(1<< &TIF_SINGLESTEP);}' unless defined(&_TIF_SINGLESTEP);
	eval 'sub _TIF_IRET () {(1<< &TIF_IRET);}' unless defined(&_TIF_IRET);
	eval 'sub _TIF_SYSCALL_AUDIT () {(1<< &TIF_SYSCALL_AUDIT);}' unless defined(&_TIF_SYSCALL_AUDIT);
	eval 'sub _TIF_POLLING_NRFLAG () {(1<< &TIF_POLLING_NRFLAG);}' unless defined(&_TIF_POLLING_NRFLAG);
	eval 'sub _TIF_WORK_MASK () {(0xffff & ~( &_TIF_SYSCALL_TRACE| &_TIF_SYSCALL_AUDIT| &_TIF_SINGLESTEP));}' unless defined(&_TIF_WORK_MASK);
	eval 'sub _TIF_ALLWORK_MASK () {0xffff;}' unless defined(&_TIF_ALLWORK_MASK);
	eval 'sub TS_USEDFPU () {0x1;}' unless defined(&TS_USEDFPU);
    }
}
1;
