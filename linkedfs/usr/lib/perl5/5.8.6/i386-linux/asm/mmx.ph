require '_h2ph_pre.ph';

no warnings 'redefine';

unless(defined(&_ASM_MMX_H)) {
    eval 'sub _ASM_MMX_H () {1;}' unless defined(&_ASM_MMX_H);
    require 'linux/types.ph';
}
1;
