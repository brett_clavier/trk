��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  �  �  �   q     	     	     3	  %   S	  *   y	      �	      �	     �	  P   
  �   W
  |     "   �     �  1   �     �  G        \     l       +   �     �  v   �  g   K  z   �  .   .  B   ]  b   �  o        s  �   �  d   -  "   �     �  P   �  J   #  L   n  �   �  1   K  8   }        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: bn_IN
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-03-23 16:34+0530
Last-Translator: Runa Bhattacharjee <runab@redhat.com>
Language-Team: Bangla <redhat-translation@bengalinux.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.9.1
Plural-Forms: Plural-Forms: nplurals=2; plural=(n != 1);

 
ত্রুটি - Kudzu চালাতে হলে আপনাকে root পরিচয় ব্যবহার করতে হবে।
 %s ডিভাইস CD-ROM ড্রাইভ IDE নিয়ন্ত্রক IEEE 1394 নিয়ন্ত্রক PCMCIA/Cardbus নিয়ন্ত্রক RAID নিয়ন্ত্রক SCSI নিয়ন্ত্রক USB নিয়ন্ত্রক %s-র জন্য %s উপনাম অর্পণ করা হয়েছে হার্ডওয়্যারে বিশৃঙ্খলা না ঘটিয়ে কেবলমাত্র 'সুরক্ষিত' অনুসন্ধান করো যে ফাইল থেকে হার্ডওয়্যার সম্বন্ধে তথ্য পড়া হবে ফ্লপি ড্রাইভ হার্ড ডিস্ক কার্ণেলের সংস্করণ কীবোর্ড %s কে %s-র সাথে সংযোগ করা হয়েছে মোডেম মনিটার মাউস নেটওয়ার্ক কার্ড প্রিন্টার কেবলমাত্র নির্ধারিত 'class'-র জন্য অনুসন্ধান করো কেবলমাত্র নির্ধারিত 'bus'-এ অনুসন্ধান করো কেবলমাত্র অনুসন্ধান করো, stdout-এ তথ্য প্রিন্ট করো %s loadkeys চালানো হয়েছে %s-র জন্য mouseconfig চালানো হয়েছে সনাক্ত করা হার্ডওয়্যার ফাইল থেকে পড়ো সনাক্ত করা হার্ডওয়্যার একটি সকেট থেকে পড়ো স্ক্যানার কোনো নির্দিষ্ট কার্ণেল সংস্করণের জন্য মডিউল অনুসন্ধান করো সেকেন্ড হিসাবে সময়সীমা নির্ধারণ করুন সাউন্ড কার্ড টেপ ড্রাইভ aep1000 পরিসেবাটি সক্রিয় করা হয়েছে bcm5820 পরিসেবা সক্রিয় করা হয়েছে %s-র সংযোগ বিচ্ছিন্ন করা হয়েছে %s-র সংযোগ বিচ্ছিন্ন করা হয়েছে (%s-র সাথে সংযোগ করা হয়েছিল) ভিডিও অ্যাডাপ্টার ভিডিও ক্যাপচার কার্ড 