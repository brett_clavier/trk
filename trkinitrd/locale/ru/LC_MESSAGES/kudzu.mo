��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  ]  �  b   -     �     �     �     �  #   �     	     1	     K	     d	  �   �	  K   
  *   T
     
     �
     �
  &   �
  
   �
     �
               &  Z   5  W   �  c   �     L  &   k  K   �  Q   �     0  K   =  9   �     �     �  '   �  %        <  ;   N     �  -   �        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: ru
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-26 11:55+0300
Last-Translator: Andrew Martynov <andrewm@inventa.ru>
Language-Team: Russian <fedora-trans-ru@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.9.1
 
ОШИБКА: для выполнения kudzu необходимо иметь права root.
 устройство %s привод CD-ROM контроллер IDE контроллер IEEE1394 контроллер PCMCIA/Cardbus контроллер RAID контроллер SCSI контроллер USB %s как псевдоним %s использовать только 'безопасные' проверки, не затрагивающие аппаратуру файл для чтении информации о устройствах дисковод гибких дисков жесткий диск версия ядра клавиатура создана ссылка %s на %s модем монитор мышь сетевая плата принтер искать только устройства, для которых указан 'class' искать устройства только на указанной шине ('bus') только поиск, вывести информацию на стандартный вывод выполнено loadkeys %s выполнен mouseconfig для %s читать информацию о устройствах из файла читать информацию о устройствах через сокет сканер поиск модулей для конкретной версии ядра установить задержку в секундах звуковая плата стример настроена служба aep1000 включена служба bcm5820 удалено %s ссылка %s удалена (ссылалась на %s) видео адаптер устройство видеозахвата 