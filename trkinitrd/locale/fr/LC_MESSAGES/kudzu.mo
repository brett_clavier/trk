��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  U  �  I   %     o     �     �     �  #   �     �     �     �     	  F   (	  ;   o	     �	  
   �	     �	     �	     �	     �	     �	     
     	
  
   
  (   "
  $   K
  C   p
     �
     �
  1   �
  7        R  9   Z  (   �  	   �     �  "   �  "   �  
     "   )     L     ^        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: fr
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-05-10 18:54+0100
Last-Translator: Philippe Villiers <philippe.villiers@wanadoo.fr>
Language-Team: French <fr@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.0
 
ERREUR - Vous devez être connecté en tant que root pour lancer kudzu.
 périphérique %s lecteur de CD-ROM contrôleur IDE contrôleur IEEE1394 contrôleur PCMCIA/de bus de cartes contrôleur RAID contrôleur SCSI contrôleur USB %s associé à l'alias %s procéder uniquement à des détections sans risques pour le matériel fichier contenant les informations à lire sur le matériel lecteur de disquettes disque dur version de noyau clavier lié %s à %s modem moniteur souris carte réseau imprimante ne détecter que la 'classe' spécifiée ne détecter que le 'bus' spécifié détecter uniquement, informations d'impression en sortie classique loadkeys %s exécuté mouseconfig exécuté pour %s rechercher le matériel détecté dans un fichier rechercher le matériel détecté à partir d'un socket scanner chercher les modules d'une version de noyau particulière définir un délai d'attente en secondes carte son lecteur de bande le service aep1000 a été activé le service bcm5820 a été activé %s délié %s délié (lié auparavant à %s) adaptateur vidéo carte de saisie vidéo 