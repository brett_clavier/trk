��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  d  �  :   4     o     ~     �     �     �     �     �     �      	  =   	  2   P	     �	     �	     �	     �	     �	     �	     �	     �	     �	  
   �	  *   �	  )   $
  1   N
     �
     �
  $   �
  #   �
     �
  <         >     _     l     z     �     �  "   �     �     �        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: pt_BR
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-04-19 16:08-0300
Last-Translator: Pedro Macedo <webmaster@margo.bijoux.nom.br>
Language-Team:  <fedora-trans-pt_BR@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.9.1
 
ERRO - Você deve estar como root para executar o kudzu.
 dispositivo %s drive de CD-ROM controlador IDE controlador IEEE1394 controlador PCMCIA/Cardbus controlador RAID controlador SCSI controlador USB apelidei %s de %s fazer apenas procuras 'seguras' que não perturbam o hardware arquivo no qual ler informações sobre o hardware drive de disquete disco rígido versão do kernel teclado linkei %s a %s modem monitor mouse placa de rede impressora procurar apenas pela 'classe' especificada procurar apenas o barramento especificado apenas procurar, escrever informações no stdout executei loadkeys %s executei mouseconfig em %s ler hardware detectado de um arquivo ler hardware detectado de um socket scanner procurar pelos módulos de uma versão específica do kernel definir tempo limite em segundos placa de som drive de fita liguei o serviço aep1000 liguei o serviço bcm5820 deslinkei %s deslinkei %s (estava linkado a %s) placa de vídeo placa de captura de vídeo 