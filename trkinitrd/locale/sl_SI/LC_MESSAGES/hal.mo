��    K      t  e   �      `     a     x     �     �  
   �     �     �     �     �     �     �     �  
   �     �     �               (     =     Q     `     t     �     �     �     �     �     �     �            
        !     -     9     P     ]     i     w     �     �     �     �     �     �     �     �     �     �     �     �     �     �      	     	     	     (	     >	     R	  
   _	     j	     z	     �	     �	     �	  
   �	     �	  '   �	     �	  4   
     F
     M
     Q
     _
  �  k
          4     A     J  
   ]     h     o     w     ~     �     �     �     �     �     �     �     �               *     :     N     g     |     �     �     �     �     �     �  
   �                    +     G     U     b     q  
        �     �     �     �     �     �     �     �     �     �     �     �     �               "     5     L     ^  	   m     w     �     �     �     �  
   �     �  0   �       2   ,     _     f     j     ~        ;   <               &          :             5   (   4           1       3      0                   8   -              +               G          *      	   )       !   6       /   "   ?       @   B   %               D   I   A         C          F   J      2       '   H   K   #   7   .           >   
      9      =                           E              $      ,    %s External Hard Drive %s Hard Drive %s Media %s Removable Media %s%s Drive /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL /DVD±RW /DVD±RW DL <b>Bandwidth:</b> <b>Bus Type:</b> <b>Capabilities:</b> <b>Device Type:</b> <b>Device:</b> <b>OEM Product:</b> <b>OEM Vendor:</b> <b>Power Usage:</b> <b>Product:</b> <b>Revision:</b> <b>Status:</b> <b>USB Version:</b> <b>Vendor:</b> Add an entry to fstab Advanced Audio CD Blank CD-R Blank CD-RW Blank DVD+R Blank DVD+R Dual-Layer Blank DVD+RW Blank DVD-R Blank DVD-RAM Blank DVD-RW Bus Type CD-R CD-ROM CD-RW DVD+R DVD+R Dual-Layer DVD+RW DVD-R DVD-RAM DVD-RW Device Device Manager Device Name Device Type Device Vendor Drive External %s%s Drive External Floppy Drive External Hard Drive Floppy Drive Hard Drive Manufacturer ID Max 	Power Drain OEM Manufacturer ID OEM Product ID PCI Product ID Product Revision Remove all generated entries from fstab Remove an entry from fstab Report detailed information about operation progress Status USB USB Bandwidth USB Version Project-Id-Version: sl_SI
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-07-20 10:21+0200
PO-Revision-Date: 2005-08-21 18:18+0200
Last-Translator: Janez Krek <janez.krek@euroteh.si>
Language-Team: Slovenščina <sl@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.3.1
Plural-Forms:  nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
 %s zunanji trdi disk %s trdi disk %s medij %s izmenljiv medij %s%s enota /DVD+R /DVD+RW /DVD-R /DVD-RAM /DVD-ROM /DVD-RW /DVD±R /DVD±R DL (dvoslojni) /DVD±RW /DVD±RW DL (dvoslojni) <b>Pasovna širina:</b> <b>Tip vodila:</b> <b>Zmožnosti:</b> <b>Tip naprave:</b> <b>Naprava:</b> <b>OEM izdelek:</b> <b>OEM proizvajalec:</b> <b>Poraba moči:</b> <b>Izdelek:</b> <b>Verzija:</b> <b>Status:</b> <b>USB verzija:</b> <b>Proizvajalec:</b> Dodaj zapis v datoteko fstab Napredno Zvočni CD Prazen CD-R Prazen CD-RW Prazen DVD+R Prazen DVD+R DL (dvoslojni) Prazen DVD+RW Prazen DVD-R Prazen DVD-RAM Prazen DVD-RW Tip vodila CD-R CD-ROM CD-RW DVD+R DVD+R DL (dvoslojni) DVD+RW DVD-R DVD-RAM DVD-RW Naprava Upravljalnik naprav Ime naprave Tip naprave Proizvajalec naprave Enota Zunanja %s%s enota Zunanja disketna enota Zunanji trdi disk Disketna enota Trdi disk ID proizvajalca Maksimalna 	 moč ID OEM proizvajalca ID OEM izdelka PCI ID izdelka Verzija izdelka Odstrani vse generiranje zapise v datoteki fstab Odstrani zapis v datoteki fstab Poročaj podrobne informacije o napredku operacije Status USB USB pasovna širina USB verzija 