��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  Y  �  M   �     ,  &   8     _     r     �     �     �     �  ,   �  Q   	  C   b	  $   �	     �	     �	     �	     
     %
     2
     ;
     H
  
   b
  8   m
  :   �
  =   �
  2     ,   R  4     !   �  A   �  3        L     d  "   |  "   �     �  5   �       "   )               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2004-04-29 06:35+0300
Last-Translator: Ossama M. Khayat <okhayat@yahoo.com>
Language-Team: Arabic <support@arabeyes.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.0.2
 
خطأ - يجب أن تكون root كي تقوم بتشغيل كِدْزو.
 جهاز %s قارئ الأقراص المدمجة مُتحكّم IDE مُتحكّم IEEE1394 مُتحكّم PCMCIA/Cardbus مُتحكّم RAID مُتحكّم SCSI مُتحكّم USB سُمّيَ %s بالاسم البديل %s قم فقط بالجَسّ الـ'آمن' والذي لا يُزعج العتاد اسم الملفّ لقراءة معلومات العتاد منه قارئ الأقراص المرنة القرص الصلب نسخة النّواة لوحة مفاتيح تم ربط %s بـ%s المودم شاشة الماوس بطاقة الشّبكة طابعة الجَسّ فقط عن 'الصّنف' المُحدّد الجَسّ فقط عن 'النّاقل' المُحدّد جُسّ فقط، واطبع المعلومات إلى stdout تمّ تشغيل مفاتيح التّحميل %s تمّ تشغيل mouseconfig من أجل %s اقرأ العتاد المَجسوس من ملفّ الماسحة الضّوئيّة ابحث عن الوحدات لنُسخة نواة مُعيّنة حدّد وقت الانتهاء بالثّواني بطاقة الصّوت قارئ الأشرطة تمّ تشغيل خدمة aep1000 تمّ تشغيل خدمة bcm5820 تمّ فَصْل %s تمّ فَصْل %s (كان مُرتبطاً بـ%s) مُحوّل الفيديو بطاقة التقاط فيديو 