��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  B  �  ;   �  
                  +     >     W     f     u     �  A   �  -   �     	  	   	     '	  
   7	     B	     T	     Z	     b	     g	     w	  "   	  !   �	  -   �	     �	     
  $   
     D
  2   K
  +   ~
     �
     �
     �
     �
     �
  (        9     G               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu 0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2003-09-17 10:21+CEST
Last-Translator: auto
Language-Team: Croatian <lokalizacija@linux.hr>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: TransDict server
 
GREŠKA - morate biti root korisnik za pokretanje kudzua.
 %s uređaj CD-ROM uređaj IDE kontroler IEEE1394 kontroler PCMCIA/Cardbus kontroler RAID kontroler SCSI kontroler USB kontroler %s je ime nadimak kao %s napravi samo 'sigurna' ispitivanja koje neče utjecati na hardver datoteka iz koje se čitaju podaci o hardveru disketni uređaj hard disk kernel inačica tipkovnica %s je vezan na %s modem monitor miš mrežna kartica printer ispituje samo za određenu 'klasu' ispituje samo određenu sabirnicu samo ispituje, ispisuje informacije na stdout pokreni loadkeys %s pokreni museconfig za %s čita pronađeni hardver iz datoteke skener traži module za određenu inačicu jezgre Linux-a postavlja prekoračenje vremena u sekundama zvučna kartica tračni uređaj uključen servis aep1000 uključen servis bcm5820 %s više nije vezan %s više nije vezan (bio je vezan na %s) video kartica kartica za hvatanje videa 