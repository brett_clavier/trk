��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  J  �  1   �  	                  (     :     R     `     n     {  E   �  +   �      	  
   	     	     1	     =	     Q	     W	     _	     h	  
   z	  0   �	  #   �	  .   �	     	
     
  %   =
     c
  6   k
  !   �
     �
     �
     �
     �
       -   $     R     a               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: cy
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-03-01 17:45+0000
Last-Translator: Owain Green <owaing@oceanfree.net>
Language-Team: Cymraeg <cy@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.2
 
GWALL - Rhaid eich bod yn wraidd i redeg kudzu.
 dyfais %s gyriant CDd-ROM rheolydd IDE rheolydd IEEE1394 rheolydd PCMCIA/Cardbus rheolydd RAID rheolydd SCSI rheolydd USB ffugenwyd %s yn %s gwneud archwiliadau 'diogel' yn unig na fyddant yn tarfu ar galedwedd ffeil i ddarllen gwybodaeth caledwedd ohoni gyriant disg meddal disg galed fersiwn cnewyllyn bysellfwrdd cysylltwyd %s â %s modem monitor llygoden cerdyn rhwydwaith argraffydd achwilio ar gyfer y 'dosbarth' penodedig yn unig archwilio'r 'bws' penodedig yn unig archwilio'n unig, argraffu gwybodaeth i stdout rhedwyd loadkeys %s rhedwyd mouseconfig ar gyfer %s dallen caledwedd archwiliedig o ffeil sganiwr chwilio am fodiwlau ar gyfer fersiwn cnewyllyn penodol gosodwch y goramser mewn eiliadau cerdyn sain gyriant tâp cynnwyd gwasanaeth aep1000 cynnwyd gwasanaeth bcm5820 datgysylltwyd %s datgysylltwyd %s (oedd wedi'i gysylltu â %s) addasydd fideo cerdyn cipio fideo 