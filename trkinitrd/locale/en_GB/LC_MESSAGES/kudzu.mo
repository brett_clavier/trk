��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  =  �  (   �  	   �     �               %     ?     O     _     n  =     )   �     �  	   �     �     	     	     &	     ,	     4	     :	     G	  $   O	     t	  '   �	     �	     �	      �	     
  2   
     >
  
   Y
  
   d
     o
     �
     �
     �
     �
     �
               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-09-30 00:39-0400
Last-Translator: alan@redhat.com
Language-Team: en_GB
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s perform only 'safe' probes that will not disturb the hardware file from which to read the hardware info floppy drive hard disc kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set the timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card 