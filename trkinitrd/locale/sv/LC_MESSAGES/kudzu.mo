��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  5  �  7   �     �  
   �               '     A     P     _     m  C   �  .   �     �  	   		     	      	     ,	     @	  
   F	     Q	     U	     c	  %   l	     �	  0   �	     �	     �	     
     5
  ,   A
     n
     �
     �
     �
     �
     �
  )   �
  
                        $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-03-07 16:52+0100
Last-Translator: Christian Rose <menthos@menthos.com>
Language-Team: Swedish <sv@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
FEL - Du måste vara root för att kunna köra kudzu.
 %s-enhet cdromenhet IDE-styrenhet IEEE1394-styrenhet PCMCIA-/Cardbus-styrenhet RAID-styrenhet SCSI-styrenhet USB-styrenhet skapade alias för %s: %s utför endast "säkra" tester som inte kommer att störa hårdvaran fil som hårdvaruinformation ska läsas ifrån diskettstation hårddisk kärnversion tangentbord länkade %s till %s modem bildskärm mus nätverkskort skrivare testa endast för den angivna klassen testa endast den angivna bussen testa endast, skriv information till standard ut körde "loadkeys %s" körde "mouseconfig" för %s läs testad hårdvara från fil bildläsare sök efter moduler för en viss kärnversion ställ in time-out i sekunder ljudkort bandstation aktiverade aep1000-tjänst aktiverade bcm5820-tjänst tog bort länken %s tog bort länken %s (var länkad till %s) grafikkort videofångarkort 