��    *      l  ;   �      �  (   �  	   �     �     �     �          &     6     F     U  0   f  1   �     �     �  	   �                     (     .     6     <     I  $   Q     v  '   �     �     �      �  "        (  2   0     c  
   z  
   �     �     �     �     �     �     �  O    E   `  	   �     �     �     �     �     �     	     	     ,	  0   K	  Q   |	  2   �	     
  
   
     
     -
     6
     K
     Q
     Y
     ^
     l
  '   t
     �
  :   �
     �
  %     !   ;  (   ]     �  5   �  )   �  
   �     �          $     A  "   Y     |  
   �        "          
          $                    	       *   &              '         %                #                               )   (                             !                                      
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do configuration that doesn't require user input do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: de
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2005-03-03 18:15-0500
PO-Revision-Date: 2005-03-23 22:58+0100
Last-Translator: Ronny Buchmann <ronny-vlug@vlugnet.org>
Language-Team: Deutsch <de@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0.2
 
Fehler - Sie müssen als Root angemeldet sein, um Kudzu zu starten.
 %s Gerät CD-ROM-Laufwerk IDE-Controller IEEE1394 Controller PCMCIA/Cardbus-Controller RAID-Controller SCSI-Controller USB-Controller für %s wurde Alias %s gesetzt Konfiguration ohne Benutzereingaben durchführen sicheren Erkennungsmodus verwenden, der Hardware-Funktionen nicht beeinträchtigt Datei für das Einlesen der Hardware-Informationen Disketten-Laufwerk Festplatte Kernelversion Tastatur %s mit %s verknüpft Modem Monitor Maus Netzwerkkarte Drucker nur nach der angegebene 'Klasse' suchen nur angegebenen Bus durchsuchen nur suchen, gebe Informationen auf die Standardausgabe aus loadkeys %s wurde ausgeführt mouseconfig wurde für %s ausgeführt erkannte Hardware aus Datei lesen erkannte Hardware von einem Socket lesen Scanner Nach Modulen für eine bestimmte Kernelversion suchen Zeitüberschreitung in Sekunden festlegen Soundkarte Band-Laufwerk aep1000 Dienst eingeschaltet bcm5820 Dienst eingeschaltet %s ist nicht verknüpft %s gelöst (war mit %s verknüpft) Grafikadapter Videokarte 