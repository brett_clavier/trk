��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  T  �  7   �               *     7     I     a     o     }     �  <   �  4   �  	   	     	     	  
   .	     9	     J	     P	     X	     _	  	   p	     z	     �	  (   �	     �	     �	  &   
     ;
  -   B
     p
     �
     �
     �
     �
     �
      �
          #               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu 9.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-03-31 22:20+0200
Last-Translator: Roman Maurer <roman@lugos.si>
Language-Team: Slovenian <translation-team-sl@lists.sourceforge.net>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 
NAPAKA - Za poganjanje orodja kudzu morate biti root.
 naprava: %s pogon CD-ROM krmilnik IDE krmilnik IEEE1394 krmilnik PCMCIA/Cardbus krmilnik RAID krmilnik SCSI krmilnik USB %s ima vzdevek %s izvajaj le ,varne` poskuse, ki ne bodo motile strojne opreme datoteka, s katere naj se preberejo podatki o opremi disketnik disk različica jedra tipkovnica povezana %s v %s modem monitor miška omrežna kartica tiskalnik zaznavaj le določeni ,razred` zaznavaj le določeno ,vodilo` le zaznavanje, izpiši podatke na stdout poženi loadkeys %s zagnal se je mouseconfig za %s preberi preskušano opremo iz datoteke skener poišči module za določeno različico jedra nastavi zakasnitev v sekundah zvočna kartica tračna enota vključena storitev aep1000 vključena storitev bcm5820 odvezana %s odvezana %s (bila povezana v %s) grafična kartica zajemalnik videa 