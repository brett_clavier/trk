��    )      d  ;   �      �  (   �  	   �     �     �     �     �          &     6     E  1   V     �     �  	   �     �     �     �     �     �     �     �       $        5  '   T     |     �      �  "   �     �  2   �     "  
   9  
   D     O     i     �     �     �     �  M  �  5     	   S     ]     m     {     �     �     �     �     �  6   �     %	     A	     Q	     X	     e	     l	     �	  	   �	     �	     �	  	   �	     �	     �	  ,   �	     
     1
  '   K
  *   s
  	   �
  '   �
     �
     �
     �
     �
          &  $   6     [     k        !           
          #                    	       )   %             &         $                "                               (   '                                                                    
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file read probed hardware from a socket scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: zh_CN
POT-Creation-Date: 2005-01-03 16:33-0500
PO-Revision-Date: 2005-02-25 15:23+1000
Last-Translator: Sarah Wang <sarahs@redhat.com>
Language-Team: zh_CN <i18n@redhat.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Report-Msgid-Bugs-To: 
X-Generator: KBabel 1.0.2
 
错误 - 您必须是根用户才能运行 kudzu。
 %s 设备 光盘驱动器 IDE 控制器 IEEE 1394 控制器 PCMCIA/Cardbus 控制器 RAID 控制器 SCSI 控制器 USB 控制器 将 %s 的别名定为 %s 只进行那些不会打扰硬件的“安全”探测 读取硬件信息的文件 软盘驱动器 硬盘 内核版本 键盘 将 %s 链接至 %s 调制解调器 显示器 鼠标 网卡 打印机 只探测指定的“类别” 只探测指定的“总线” 只探测，将信息用 stdout 打印出来 运行 loadkeys %s  为 %s 运行 mouseconfig 从文件中读取探测硬件的信息 从套接字中读取探测硬件的信息 扫描仪 搜索某一特定内核版本的模块 设立超时秒数 声卡 磁带驱动器 开启  aep100 服务 开启 bcm5820 服务 无链接的 %s 无链接的 %s（曾链接至 %s） 视频适配器 视频捕捉卡 