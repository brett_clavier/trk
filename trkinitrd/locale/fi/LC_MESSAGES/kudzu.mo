��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  <  �  5   �     �        
             (     >     J  
   V      a  F   �  (   �     �  
   �     		     	     &	     <	     D	     M	     S	     `	  $   i	  !   �	  3   �	     �	  &   �	  "   $
     G
  )   S
     }
     �
     �
  $   �
  $   �
     �
  2        I     V               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-06-25 08:50+0300
Last-Translator: Lauri Nurmi <lanurmi@iki.fi>
Language-Team: Finnish <>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: KBabel 1.0
 
VIRHE - Vain pääkäyttäjä voi suorittaa kudzun.
 %s-laite CD-ROM-asema IDE-ohjain IEEE1394-ohjain PCMCIA/Cardbus-ohjain RAID-ohjain SCSI-ohjain USB-ohjain annettiin moduulille %s alias %s tee vain "turvallisia" etsintöjä, jotka eivät häiritse laitteistoa tiedosto, josta laitteistotiedot luetaan levykeasema kiintolevy ytimen versio näppäimistö linkitettiin %s -> %s modeemi näyttö hiiri verkkokortti tulostin etsi vain annetun "luokan" laitteita etsi vain annetusta "väylästä" tee vain etsintä, tulosta tiedot vakiotulosteeseen suoritettiin loadkeys %s suoritettiin mouseconfig laitteelle %s lue etsitty laitteisto tiedostosta kuvanlukija etsi moduuleita tietylle ytimen versiolle aseta aikakatkaisu sekunteina äänikortti nauha-asema otettiin käyttöön aep1000-palvelu otettiin käyttöön bcm5820-palvelu irrotettiin linkitys %s irrotettiin linkitys %s (linkityksen kohde oli %s) videosovitin videonsieppauskortti 