��    (      \  5   �      p  (   q  	   �     �     �     �     �     �     �            1   .     `     �  	   �     �     �     �     �     �     �     �     �  $   �       '   ,     T     d      {     �  2   �     �  
   �  
   �               8     D     c     q  1  �  B   �     �  
                  ,     D     R     `     m  L   ~  .   �     �     	     	  
   '	     2	     D	     L	     S	  	   X	     b	  !   k	  "   �	  8   �	     �	     �	  1   
     L
  .   S
      �
     �
     �
      �
      �
            	   )  
   3               $                       !                                                                	             #             &         %          '   (             
      "       
ERROR - You must be root to run kudzu.
 %s device CD-ROM drive IDE controller IEEE1394 controller PCMCIA/Cardbus controller RAID controller SCSI controller USB controller aliased %s as %s do only 'safe' probes that won't disturb hardware file to read hardware info from floppy drive hard disk kernel version keyboard linked %s to %s modem monitor mouse network card printer probe only for the specified 'class' probe only the specified 'bus' probe only, print information to stdout ran loadkeys %s ran mouseconfig for %s read probed hardware from a file scanner search for modules for a particular kernel version set timeout in seconds sound card tape drive turned on aep1000 service turned on bcm5820 service unlinked %s unlinked %s (was linked to %s) video adapter video capture card Project-Id-Version: kudzu 1.10
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2004-12-17 16:52-0500
PO-Revision-Date: 2004-12-18 14:09+0000
Last-Translator: Richard Allen <ra@ra.is>
Language-Team: is <kde-isl@molar.is>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8-bit
 
VILLA - Þú verður að vera rótarnotandi til að keyra kudzu.
 %s tæki geisladrif IDE stýring IEEE1394 stýring PCMCIA/Cardbus stýring RAID stýring SCSI stýring USB stýring nefndi %s sem %s framkvæma einungis 'örugga' vélbúnaðarleit sem truflar ekki vélbúnað skrá sem inniheldur vélbúnaðarupplýsingar disklingadrif harður diskur útgáfa kjarna lyklaborð tengdi %s við %s módald skjár mús netspjald prentari leita einungis að tilteknum hóp leita einungis á tiltekinni braut leita einungis, prenta það sem finnst á staðalúttak keyrði loadkeys %s keyrði mouseconfig fyrir %s lesa niðurstöður vélbúnaðarleitar úr skrá skanni leita að reklum fyrir tiltekna kjarnaútgáfu setja tímatakmörk í sekúndum hljóðkort segulbandsstöð set aep1000 þjónustuna í gang set bcm5820 þjónustuna í gang aftengdi %s aftengdi %s (var tengt %s) skjákort myndskanni 