#!/usr/bin/perl

use lib qw(/usr/lib/libDrakX);
use devices;
use MDK::Common;
use Data::Dumper;

my $devfs_dir = "/etc/devfs/conf.d";
my $udev_dir = "/etc/udev/rules.d";

my %block_devices;

my $sys_block = "/sys/block";
foreach my $device (all($sys_block)) {
    my ($bus_type, $host, $target) = readlink("$sys_block/$device/device") =~ m!/(\w+)\d+/(\d+)\.(\d+)$! or next;
    my $bus = $host % 2;
    $host -= $bus;
    my $media;
    if ($bus_type eq "ide") {
	$media = cat_("/proc/ide/$device/media");
	chomp $media;
	$media = $media eq "cdrom" ? "cd" : "disk";
    } elsif ($bus_type eq "scsi") {
	$media = $device =~ /^sd/ ? "disk" : $device =~ /^sr/ ? "cdrom" : $device =~ /^sg/ ? "generic" : undef;
    }
    $block_devices{"ide/host$host/bus$bus/target$target/lun0/$media"} = $device;
}

sub from_devfs {
    my ($devfs) = @_;
    devices::from_devfs($devfs) || $block_devices{$devfs};
}

my @devices;

foreach (all($devfs_dir)) {
    my ($symlink) = /(.*)\.conf$/;
    $symlink && ! -f "$udev_dir/$symlink.rules" or next;
    my ($devfs1, $devfs2, $of1, $devfs3, $of2) = cat_("$devfs_dir/$symlink.conf") =~ /^REGISTER\s+\^(\S+)\$\s+CFUNCTION GLOBAL mksymlink (\S+) (\S+)\nUNREGISTER\s+\^(\S+)\$\s+CFUNCTION GLOBAL unlink (\S+)\n$/;
    $devfs1 eq $devfs2 && $devfs1 eq $devfs3 && $of1 eq $symlink && $of2 eq $symlink or next;
    my $device = from_devfs($devfs1);
    $device && $device ne $symlink or next;
    push @devices, { if => $device, of => $symlink };
}

# print Dumper(\%block_devices, \@devices);

foreach my $dev (@devices) {
    $dev->{if} && $dev->{of} or next;
    print "$udev_dir/$dev->{of}.rules\n" .  qq(KERNEL="$dev->{if}", SYMLINK="$dev->{of}\n");
    output_p("$udev_dir/$dev->{of}.rules\n", qq(KERNEL="$dev->{if}", SYMLINK="$dev->{of}"\n));
}
