#!/bin/sh
#
# rc.halt       This file is executed by init when it goes into runlevel
#               0 (halt) or runlevel 6 (reboot). It kills all processes,
#               unmounts file systems and then either halts or reboots.
#
# Author:       Miquel van Smoorenburg, <miquels@drinkel.nl.mugnet.org>
#               Modified for RHS Linux by Damien Neil
#

# Set the path.
PATH=/sbin:/bin:/usr/bin:/usr/sbin
# Set TRK CD as a variable
if [ -s /etc/trkcd ]; then
TRKCD=`cat /etc/trkcd`
elif [ -s /etc/trkhd ]; then
TRKCD=`cat /etc/trkhd`
else 
TRKCD=`cat /etc/trknfs`
fi;
TRKMOUNTDIR=`cat /etc/trkmountdir`
# See how we were called.
case "$0" in
  *halt)
  	message=
	command="halt"
	;;
  *reboot)
	message="Please stand by while rebooting the system..."
	command="reboot"
	;;
  *)
	echo "$0: call me as \"rc.halt\" or \"rc.reboot\" please!"
	exit 1
	;;
esac
if [ -e /etc/mountallfstab ]; then umountallfs; fi;
# Kill all processes.
[ "${BASH+bash}" = bash ] && enable kill

echo "Sending all processes the TERM signal..."
kill -15 -1
sleep 5
echo "Sending all processes the KILL signal.."
kill -9 -1

# Write to wtmp file before unmounting /var
halt -w
# Stopping local fileserver
if [ -s /var/run/smbd.pid ]; then fileserver stop; fi
# Turn off swap, then unmount file systems.
echo "Turning off swap"
swapoff -a
echo "Unmounting filesystems"
rm -f /usr
rm -f /lib/modules
rm -f /lib/security
rm -f /var/lib/samba
rm -f /var/lock/subsys/trklocal
if [ -e /zim ]; then 
cd /zim/lib/
for i in  *; do rm -f /lib/$i; done
cd /zim/bin/
for i in  *; do rm -f /bin/$i; done
fi;
cd /
sleep 2
if [ -e /zim ]; then
umount -f /zim;
fi;
umount -f /linkedfs
umount -f /linkedfs-ro
losetup -d /dev/loop0 2>/dev/null
# Reattempt to turn of swap if it didn't succeed the first time
swapoff -a
if [ $TRKMOUNTDIR = /trk -a ! -e /var/run/runfromhd ]; then 
umount -f /trk
 if [ ! -e /var/run/noeject ]; then
 echo "Ejecting TRK CD/disk"
 eject /dev/$TRKCD 2>/dev/null;
 sleep 1
 fi;
fi;
echo "Remounting root read-only"
mount -n -o remount,ro /
echo -e $"\\033[01;36m"
echo -e $"If you like what TRK had to offer, you can send a Paypal donation to donations@trinityhome.org. 
More info on http://trinityhome.org/trk"
echo -en $"\\033[0;39m"

if [ $command == halt ]; then
	echo
	echo
	echo
	echo
	COUNT=10
	escape=$'\x1b'
	export HOME=/root
	until [ $COUNT = -1 ]; do
		tput cup $[`stty size|awk '{print $1}'`-4] 0
		echo -e "\\033[01;39m                     $COUNT seconds to poweroff. 
	Press 'escape' to cancel, 'r' for reboot, 'p' for poweroff now, 's' for a shell \\033[0;39m"
		read -n1 -t1 key 
		if [ "r$key" == "r$escape" ]; then echo "Escape pressed, cancel poweroff"; exit 0; 
		elif [ "r$key" == "rr" ]; then reboot -i -d
		elif [ "r$key" == "rp" ]; then halt -i -d -p
		elif [ "r$key" == "rs" ]; then echo "Returning to a bash shell"; bash
		fi;
		COUNT=$[$COUNT-1]
	done;
fi;

# Now halt or reboot.
echo "$message"
[ -f /etc/fastboot ] && echo "On the next boot fsck will be skipped."
eval $command -i -d -p

