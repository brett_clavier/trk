# /etc/profile.d/lang.sh - set i18n stuff

if [ -n "$GDM_LANG" ]; then
    LC_SOURCED=1
    LANG="$GDM_LANG"
else
    # only source one of the possible files, and in that order;
    # if system wide and user config are mixed and different, umpredictable
    # things will happen...
    for langfile in $HOME/.i18n /etc/sysconfig/i18n ; do
	[ -f $langfile -a "$LC_SOURCED" != 1 ] && . $langfile && LC_SOURCED=1 && export LC_SOURCED
    done    
fi

if [ "$LC_SOURCED" = 1 ]; then
    if [ -n "$LANG" ] ; then
       [ "$LANG" = "C" ] && LANG="en_US"
       export LANG
    else
      unset LANG
    fi
    [ -n "$LANG" ] && export LANG || unset LANG
    [ -n "$LC_ADDRESS" ] && export LC_ADDRESS || unset LC_ADDRESS
    [ -n "$LC_CTYPE" ] && export LC_CTYPE || unset LC_CTYPE
    [ -n "$LC_COLLATE" ] && export LC_COLLATE || unset LC_COLLATE
    [ -n "$LC_IDENTIFICATION" ] && export LC_IDENTIFICATION || unset LC_IDENTIFICATION
    [ -n "$LC_MEASUREMENT" ] && export LC_MEASUREMENT || unset LC_MEASUREMENT
    [ -n "$LC_MESSAGES" ] && export LC_MESSAGES || unset LC_MESSAGES
    [ -n "$LC_MONETARY" ] && export LC_MONETARY || unset LC_MONETARY
    [ -n "$LC_NAME" ] && export LC_NAME || unset LC_NAME
    [ -n "$LC_NUMERIC" ] && export LC_NUMERIC || unset LC_NUMERIC
    [ -n "$LC_PAPER" ] && export LC_PAPER || unset LC_PAPER
    [ -n "$LC_TELEPHONE" ] && export LC_TELEPHONE || unset LC_TELEPHONE
    [ -n "$LC_TIME" ] && export LC_TIME || unset LC_TIME
    if [ -n "$LC_ALL" ]; then
       if [ "$LC_ALL" != "$LANG" ]; then
         export LC_ALL
       else
         unset LC_ALL
       fi
    else
       unset LC_ALL
    fi
    [ -n "$LANGUAGE" ] && export LANGUAGE || unset LANGUAGE
    if [ -n "$LINGUAS" ]; then
       if [ "$LINGUAS" != "$LANG" -a "$LINGUAS" != "$LANGUAGE" ]; then
          export LINGUAS
       else
          unset LINGUAS
       fi
    else 
       unset LINGUAS
    fi
    
    if [ -n "$RPM_INSTALL_LANG" ]; then
       export RPM_INSTALL_LANG
    else
       unset RPM_INSTALL_LANG
    fi

    # XIM and keyboard stuff
    [ -n "$ENC" ] && export ENC || unset ENC
    [ -n "$XIM" ] && export XIM || unset XIM
    [ -n "$XIM_PROGRAM" ] && export XIM_PROGRAM || unset XIM_PROGRAM
    [ -n "$XMODIFIERS" ] && export XMODIFIERS || unset XMODIFIERS
    [ -n "$_XKB_CHARSET" ] && export _XKB_CHARSET || unset _XKB_CHARSET
    [ -n "$GTK_IM_MODULE" ] && export GTK_IM_MODULE || unset GTK_IM_MODULE
    [ -n "$QT_IM_MODULE" ] && export QT_IM_MODULE || unset QT_IM_MODULE

    [ -f /etc/init.d/mandrake_consmap ] && . /etc/init.d/mandrake_consmap

    unset SYSFONTACM SYSFONT

    # handling of special cases where localization is done
    # only on console or only on X11.
    # when launching an xdm-like program we are not yet in X11 (so
    # $DISPLAY is not defined) but obviously we will, so we check
    # also $DESKTOP which is defined before calling xdm-like program
    if [ -n "$DISPLAY" ]; then
	[ "$X11_NOT_LOCALIZED" = "yes" ] && LANGUAGE=C
    elif [ -z "$DESKTOP" ]; then
	[ "$CONSOLE_NOT_LOCALIZED" = "yes" ] && LANGUAGE=C
    fi
    
    if [ -n "$CHARSET" ]; then
	case $CHARSET in
	    8859-1|8859-2|8859-5|8859-15|koi*)
                if [ "$TERM" = "linux" -a "`/sbin/consoletype`" = "vt" ]; then
                       echo -n -e '\033(K' 2>/dev/null > /proc/$$/fd/0
                fi
                ;;
        esac
    elif [ -n "$SYSFONTACM" ]; then
	case $SYSFONTACM in
	    iso01*|iso02*|iso05*|iso15*|koi*|latin2-ucw*)
		if [ "$TERM" = "linux" -a "`/sbin/consoletype`" = "vt" ]; then
			echo -n -e '\033(K' 2>/dev/null > /proc/$$/fd/0
		fi
		;;
	esac
    fi
    if [ -n "$LANG" ]; then
      case $LANG in
    	*.utf8*|*.UTF-8*)
    	if [ "$TERM" = "linux" -a "`/sbin/consoletype`" = "vt" ]; then
		[ -x /bin/unicode_start ] && /sbin/consoletype fg && unicode_start $SYSFONT $SYSFONTACM
        fi
	;;
      esac
    fi
    [ -n "$LANGUAGE" ] && export LANGUAGE

fi
unset langfile
