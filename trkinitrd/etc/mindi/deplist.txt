# $Id: deplist.txt 2622 2010-04-12 02:47:23Z bruno $
#
# NB:
# 1. Directories are allowed. Their contents will be included by Mindi.
# 2. If you put 'libncurses' (w/o quotation marks) on an uncommented line then
#    Mindi will find /lib/libncurses* and include all matching files.
# 3. You recommend that you give an absolute path name
# 4. If you want to add something, just add it on its own line. K.I.S.S.
# 5. You must _not_ put a semicolon & comment after an entry. e.g. 'foo; #bar'.

#------------------------- STUFF ADDED BY THE USER ----------------------------
#---vvvv     vvvv     vvvv  list your stuff here!  vvvv     vvvv     vvvv


#---^^^^     ^^^^     ^^^^  list your stuff here!  ^^^^     ^^^^     ^^^^
#------------------------------------------------------------------------------
